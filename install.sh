#!/bin/bash

sudo su <<HERE

chown -R $USER .
chgrp -R $USER .
chmod -R 777 .
exit
HERE


composer clear-cache
composer install
php bin/console d:s:u -f
php bin/console d:f:l -n
php bin/console c:c

php bin/console c:c --env=prod
php bin/console a:d --env=prod

sudo su << THERE

chown -R $USER .
chgrp -R $USER .
chmod -R 777 .
exit
THERE
