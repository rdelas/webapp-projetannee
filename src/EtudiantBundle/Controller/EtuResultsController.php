<?php

namespace EtudiantBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class EtuResultsController extends Controller
{
    /**
     * @Route("/results", name="student-results")
     */
    public function indexAction(Request $request)
    {
        $page = ($request->get('page') !=  null) ? $request->get('page') : 1;
        $element =  ($request->get('elements') !=  null) ? $request->get('elements') : 10;
        $filter = ($request->get('filter') != null) ? $request->get('filter') : null;
        $student = $this->get('security.token_storage')->getToken()->getUser();

        $submissions = $this->get('student.results')->findSubmissionsPagination($student, $page, $element, $filter);

        //Reception pagination
        if($request->get('paginate') != null ) {
            return $this->render('EtudiantBundle:Results:student-results-table.html.twig', array(
                'totalResults' => $submissions["totalResults"],
                'submissions' => $submissions["submissions"],
                'totalPage' => $submissions["totalPage"],
                'beginResult' => $submissions["beginResult"],
                'endResult' => $submissions["endResult"],
                'runResults' => $submissions["runResults"],
                'statut' => $submissions["submission"],
                'page' => $page,
                'elements' => $element,
            ));
        }

        return $this->render('EtudiantBundle:Results:student-results.html.twig', array(
            'totalResults' => $submissions["totalResults"],
            'submissions' => $submissions["submissions"],
            'totalPage' => $submissions["totalPage"],
            'beginResult' => $submissions["beginResult"],
            'endResult' => $submissions["endResult"],
            'runResults' => $submissions["runResults"],
            'statut' => $submissions["submission"],
            'page' => $page,
            'elements' => $element,
        ));


    }

    /**
     * @Route("/results/build", name="student-results-build-modal")
     */
    public function teacherTestsBuildModalAction(Request $request) {
        $submission = $this->get('student.results')->getSubmission($request->get('id'));
        return $this->render('EtudiantBundle:Results:Modal/student-results-build-modal-body.html.twig', array(
            'sub' => $submission,
        ));
    }

    /**
     * @Route("/results/run", name="student-results-run-modal")
     */
    public function teacherTestsRunModalAction(Request $request) {
        $submission = $this->get('student.results')->getSubmission($request->get('id'));
        return $this->render('EtudiantBundle:Results:Modal/student-results-run-modal-body.html.twig', array(
            'sub' => $submission,
        ));
    }
}
