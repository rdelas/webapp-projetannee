<?php

namespace EtudiantBundle\Controller;

use SharedBundle\Entity\Submission;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use EtudiantBundle\Form\Repository\RepoInfoType;
use EtudiantBundle\Form\Repository\SubmitCodeType;
use SharedBundle\Entity\RunResult;
use Symfony\Component\HttpFoundation\Response;

class EtuRepositoriesController extends Controller
{
    /**
     * @Route("/repositories", name="student-repositories")
     */
    public function indexAction(Request $request)
    {
        $page = ($request->get('page') !=  null) ? $request->get('page') : 1;
        $element =  ($request->get('elements') !=  null) ? $request->get('elements') : 10;
        $filter = ($request->get('filter') != null) ? $request->get('filter') : null;
        $student =  $this->get('security.token_storage')->getToken()->getUser();

        $repositories = $this->get('student.repositories')->findRepositoriesPagination($student, $page, $element, $filter);


        //Reception pagination
        if($request->get('paginate') != null ) {
            return $this->render('EtudiantBundle:Repositories:student-repositories-table.html.twig', array(
                'totalResults' => $repositories["totalResults"],
                'repositories' => $repositories["repositories"],
                'totalPage' => $repositories["totalPage"],
                'beginResult' => $repositories["beginResult"],
                'endResult' => $repositories["endResult"],
                'page' => $page,
                'elements' => $element,
                'userSubmission' => $repositories["userSubmission"],
                'userGrantedSubmission' => $repositories["userGrantedSubmission"],
                'judgeStatus' => $this->get('rest.manager')->statusJudge()
            ));
        }

        //Other
        return $this->render('EtudiantBundle:Repositories:student-repositories.html.twig', array(
            'totalResults' => $repositories["totalResults"],
            'repositories' => $repositories["repositories"],
            'totalPage' => $repositories["totalPage"],
            'beginResult' => $repositories["beginResult"],
            'endResult' => $repositories["endResult"],
            'page' => $page,
            'elements' => $element,
            'userSubmission' => $repositories["userSubmission"],
            'userGrantedSubmission' => $repositories["userGrantedSubmission"],
            'judgeStatus' => $this->get('rest.manager')->statusJudge()
        ));
    }

    /**
     * @Route("/repositories/submission", name="student-repositories-submission-modal")
     */
    public function teacherTestsBuildModalAction(Request $request) {
        $repo = $this->get('student.repositories')->getRepository($request->get('id'));
        $student =  $this->get('security.token_storage')->getToken()->getUser();

        $submission = new Submission();
        $formRepoSubmission = $this->get('form.factory')->createNamedBuilder("repo_submission_form".$repo->getId(), SubmitCodeType::class, $submission)->getForm();
        $formRepoSubmission->handleRequest($request);
        if($formRepoSubmission->isSubmitted()) {
            $this->get('student.repositories')->submitCode($submission, $repo, $student);
            $response = $this->redirectToRoute('student-results', array(), 301);
            return $response;
        }

        return $this->render('EtudiantBundle:Repositories:Modal/student-repositories-submissions-modal-body.html.twig', array(
            'repository' => $repo,
            'formRepoSubmission' =>  $formRepoSubmission->createView(),
        ));
    }

    /**
     * @Route("/repositories/description", name="student-repositories-description-modal")
     */
    public function studentRepositoriesDescModalAction(Request $request) {
        $repo = $this->get('student.repositories')->getRepository($request->get('id'));
        return $this->render('EtudiantBundle:Repositories:Modal/student-repositories-description-modal-body.html.twig', array(
            'repository' => $repo,
        ));
    }
}
