<?php

namespace EtudiantBundle\Services;

use Doctrine\ORM\EntityManager;

class StudentResultsService {

    protected $em;
    protected $submissionRepository;
    protected $runResultRepository;

    /**
     * StudentResultsService constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em) {
        $this->em = $em;
        $this->submissionRepository = $em->getRepository('SharedBundle:Submission');
        $this->runResultRepository = $em->getRepository('SharedBundle:RunResult');
    }

    /**
     * @param $student
     * @param $page
     * @param $elements
     * @param $filter
     * @return array
     */
    public function findSubmissionsPagination($student, $page, $elements, $filter) {
        $results = array();
        $totalResults = $this->submissionRepository->countAllSubmissionOfStudent($student);
        $totalPage = ceil( $totalResults / $elements );
        $submissions = $this->submissionRepository->findAllSubmissionOfStudentWithLimit($student, $elements, (($page - 1) * $elements), $filter);

        $results["totalResults"] = $totalResults;
        $results["totalPage"] = $totalPage;
        $results["submissions"] = $submissions;
        $results["beginResult"] = (($page - 1) * $elements) + 1;
        $results["endResult"] = ($page == $totalPage) ? $totalResults : $page * $elements;

        $results['runResults'] = [];
        $results['submission'] = [];
        foreach($submissions as $key => $sub) {
            $rrs = $sub->getRunResults();
            $isTimeout = false;
            $isWrongAnswer = false;
            $isExecutionError = false;
            $isOtherError = false;
            foreach($rrs as $rr) {
                $res = $rr->getResult();
                if($res == 400) {
                    $isExecutionError = true;
                } else if($res == 408) {
                    $isTimeout = true;
                } else if($res == 401) {
                    $isWrongAnswer = true;
                } else if($res == 404) {
                    $isOtherError = true;
                }
            }
           $results['runResults'][$key] = $rrs;
            if($isExecutionError == 1) {
                $results['submission'][$key] = 400;
            } else if ($isTimeout == 1) {
                $results['submission'][$key] = 408;
            } else if ($isWrongAnswer ==  1) {
                $results['submission'][$key] = 401;
            } else if ($isOtherError == 1) {
                $results['submission'][$key] = 404;
            } else {
                $results['submission'][$key] = 200;
            }
        }
        return $results;
    }

    /**
     * @param $id
     * @return null|object
     */
    public function getSubmission($id) {
        return $this->submissionRepository->find($id);
    }
}