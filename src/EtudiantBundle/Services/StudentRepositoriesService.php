<?php
namespace EtudiantBundle\Services;

use AppBundle\Service\HTTPService;
use Doctrine\ORM\EntityManager;
use SharedBundle\Entity\Repository;
use SharedBundle\Entity\RunResult;
use SharedBundle\Entity\Submission;
use SharedBundle\Entity\TestCase;
use UserBundle\Entity\User;

class StudentRepositoriesService {

    protected $em;
    protected $http;
    protected $repoRepository;
    protected $testCaseRepository;

    public function __construct( EntityManager $em, HTTPService $http) {
        $this->em = $em;
        $this->http = $http;
        $this->repoRepository = $em->getRepository('SharedBundle:Repository');
        $this->testCaseRepository = $em->getRepository('SharedBundle:TestCase');
    }

    /**
     * @param $student
     * @param $page
     * @param $elements
     * @param $filter
     * @return array
     */
    public function findRepositoriesPagination($student, $page, $elements, $filter) {
        $results = array();
        $totalResults = $this->repoRepository->countAllRepoOfStudent($student);
        $totalPage = ceil( $totalResults / $elements );
        $repositories = $this->repoRepository->findAllRepoOfStudentWithLimit($student, $elements, (($page - 1) * $elements), $filter);
        $results["totalResults"] = $totalResults;
        $results["totalPage"] = $totalPage;
        $results["repositories"] = $repositories;
        $results["beginResult"] = (($page - 1) * $elements) + 1;
        $results["endResult"] = ($page == $totalPage) ? $totalResults : $page * $elements;
        $results['userSubmission'] = array();
        $results['userGrantedSubmission'] = array();

        foreach($repositories as $key => $repo) {
            $results['userSubmission'][$key] = $this->repoRepository->CountSubmissionOfStudent($student, $repo);
            $results['userGrantedSubmission'][$key] = $this->repoRepository->CountGrantedSubmissionOfStudent($student, $repo);
        }
        return $results;
    }

    /**
     * @param RunResult $runResult
     * @param Repository $repo
     * @param User $student
     * @return array
     * @throws \Exception
     */
    public function submitCode(Submission $submission, Repository $repo, User $student) {
        $submission->setDateCommit(new \DateTime());
        $submission->setUser($student);
        $submission->setRepository($repo);
        $this->em->persist($submission);
        $this->em->flush();
        return $this->http->runStudentTest($submission, $repo);
    }

    /**
     * @param $id
     * @return null|object
     */
    public function getRepository($id) {
        return $this->repoRepository->find($id);
    }
}
