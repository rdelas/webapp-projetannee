<?php
namespace AdminBundle\Services;

use Doctrine\ORM\EntityManager;
use UserBundle\Entity\User;

class AdminTeacherService {

    protected $userRepository;
    protected $repoRepository;
    protected $runResultRepository;
    protected $roles;
    protected $em;
    protected $colors = array('blue','info','warning', 'violet', 'grey', 'green', 'yellow', 'dark');

    public function __construct( EntityManager $em) {
        $this->userRepository = $em->getRepository('UserBundle:User');
        $this->repoRepository = $em->getRepository('SharedBundle:Repository');
        $this->runResultRepository = $em->getRepository('SharedBundle:RunResult');
        $this->roles = "%ROLE_PROF%";
        $this->em=$em;
    }

    public function saveTeacher(User $etudiant)
    {
        $this->em->merge($etudiant);
        $this->em->flush();
    }
    public function findTeachersPagination($page, $elements, $filter) {
        $results = array();
        $totalResults = $this->userRepository->countAllUsers($this->roles);
        $totalPage = ceil( $totalResults / $elements );
        $users = $this->userRepository->findAllUsersWithLimit($this->roles, $elements, (($page - 1) * $elements), $filter);
        $results["totalResults"] = $totalResults;
        $results["totalPage"] = $totalPage;
        $results["users"] = $users;
        $results["beginResult"] = (($page - 1) * $elements) + 1;
        $results["endResult"] = ($page == $totalPage) ? $totalResults : $page * $elements;
        return $results;
    }

    public function getTeacherCard() {
        $results = array();
        $results['total_users'] = $this->userRepository->countAllUsers($this->roles);
        $results['active_users'] = $this->userRepository->countActiveUsers($this->roles);
        $results['inactive_users'] = $this->userRepository->countInactiveUsers($this->roles);
        return $results;
    }

    public function getRepoCard() {
        $results = array();
        $results['total_repo'] = $this->repoRepository->countAllRepo();
        $results['language_repo'] = $this->repoRepository->countRepoByLanguage();
        $results['colors'] = $this->colors;
        return $results;
    }

    public function getStudentCard() {
        $results = array();
        $results['total_students'] = $this->userRepository->countAllUsers("%ROLE_ETU%");
        $results['active_students'] = $this->userRepository->countActiveUsers("%ROLE_ETU%");
        $results['inactive_students'] = $this->userRepository->countInactiveUsers("%ROLE_ETU");
        return $results;
    }

    public function getSubmissionCard() {
        $results = array();
        $results['total_submissions'] = $this->runResultRepository->countAllRunResult();
        $results['result_submissions'] = $this->runResultRepository->CountRunResultByReturnCode($this->roles);
        $results['colors'] = $this->colors;
        return $results;
    }
}
