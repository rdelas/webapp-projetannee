<?php
/**
 * Created by PhpStorm.
 * User: remi
 * Date: 05/04/2016
 * Time: 14:34
 */
namespace AdminBundle\Services;

use Doctrine\ORM\EntityManager;
use SharedBundle\Entity\Config;

class AdminConfigService {

    protected $em;
    protected $configRepository;

    public function __construct( EntityManager $em) {
        $this->em = $em;
        $this->configRepository = $em->getRepository('SharedBundle:Config');
    }

    public function addConfig(Config $config) {
        $config->setEnabled(false);
        $this->em->persist($config);
        $this->em->flush();
    }

    public function editConfig(Config $config) {
        if($config->getEnabled()) {
            $this->configRepository->resetStatus();
        }
        $this->em->persist($config);
        $this->em->flush();
    }

    public function findConfigsPagination($page, $elements, $filter) {
        $results = array();
        $totalResults = $this->configRepository->countAllConfig();
        $totalPage = ceil( $totalResults / $elements );
        $configs = $this->configRepository->findAllConfigWithLimit($elements, (($page - 1) * $elements), $filter);
        $results["totalResults"] = $totalResults;
        $results["totalPage"] = $totalPage;
        $results["configs"] = $configs;
        $results["beginResult"] = (($page - 1) * $elements) + 1;
        $results["endResult"] = ($page == $totalPage) ? $totalResults : $page * $elements;
        return $results;
    }
}
