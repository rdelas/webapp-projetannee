<?php
/**
 * Created by PhpStorm.
 * User: remi
 * Date: 05/04/2016
 * Time: 15:00
 */

namespace AdminBundle\Form\Config;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class EditConfigType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('label', TextType::class)
            ->add('addr', TextType::class)
            ->add('port', IntegerType::class)
            ->add('uri', TextType::class, array('required' => false))
            ->add('enabled', CheckboxType::class, array('required' => false))
            ->add('save', SubmitType::class, array('label' => 'Ajouter une config'))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SharedBundle\Entity\Config'
        ));
    }

    public function getName()
    {
        return 'adminBundle_config_add';
    }
}