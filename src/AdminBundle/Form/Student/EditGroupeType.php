<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 12/04/2016
 * Time: 16:29
 */

namespace AdminBundle\Form\Student;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use UserBundle\Entity\User;
use UserBundle\Repository\UserRepository;

class EditGroupeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('libelle', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Renommer'))
            ->add('disabled', CheckboxType::class, array('required' => false))
            ->add('creator', EntityType::class, array(
                'class' => 'UserBundle:User',
                'by_reference' => true,
                'choice_label' => function (User $usr) {
                    return $usr->getFullname();
                },
                'required' => true,
                'multiple' => false,
                'query_builder' => function (UserRepository $ugr) {
                    return $ugr->createQueryBuilder('u')->where('u.roles LIKE :role')->setParameter('role', '%ROLE_PROF%');
                }));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(

            'groupe' => null,
            'data_class' => 'SharedBundle\Entity\UserGroup'
        ));
    }


    public function getName()
    {
        return 'professeurBundle__groupe_edit';
    }
}