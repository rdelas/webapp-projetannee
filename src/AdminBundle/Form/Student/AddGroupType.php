<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 10/04/2016
 * Time: 16:41
 */

namespace AdminBundle\Form\Student;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use UserBundle\Entity\User;
use UserBundle\Repository\UserRepository;

class AddGroupType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('libelle', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Ajouter le groupe'))
            ->add('creator', EntityType::class, array(
                'class' => 'UserBundle:User',
                'by_reference' => true,
                'choice_label' => function (User $usr) {
                    return $usr->getFullname();
                },
                'required' => true,
                'multiple' => false,
                'query_builder' => function (UserRepository $ugr) {
                    return $ugr->createQueryBuilder('u')->where('u.roles LIKE :role')->setParameter('role', '%ROLE_PROF%');
                }));


    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array());
    }

    public function getName()
    {
        return 'professeurBundle_group_add';
    }

}