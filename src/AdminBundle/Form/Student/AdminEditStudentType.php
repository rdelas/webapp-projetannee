<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 10/04/2016
 * Time: 16:41
 */

namespace AdminBundle\Form\Student;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdminEditStudentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email', EmailType::class)
            ->add('firstname', TextType::class)
            ->add('lastname', TextType::class)
            ->add('id', TextType::class, array('attr' => array('class' => 'hidden')))
            ->add('username', TextType::class, array('attr' => array('class' => 'hidden')))
            ->add('Enregistrer', SubmitType::class, array('attr' => array('class' => 'btSaveEtuFromGroup btn btn-info')))
            ->add('enabled', CheckboxType::class, array('attr' => array('class' => 'hidden'), 'required' => false))
            ->add('Retirer', CheckboxType::class, array('attr' => array('class' => 'hidden'), 'mapped' => false, 'required' => false))
            ->add('groupe', EntityType::class, array('class' => 'SharedBundle:UserGroup', 'attr' => array('class' => 'hidden'), 'choice_label' => 'libelle', 'mapped' => false, 'data' => $builder->getOption('groupe')));

    }

    public
    function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'groupe' => null
        ));
    }

    public
    function getName()
    {
        return 'professeurBundle_edit_sutdent';
    }

}