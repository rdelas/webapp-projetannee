<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 02/04/2016
 * Time: 14:13
 */

namespace AdminBundle\Form\Student;

use SharedBundle\Entity\UserGroup;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class ImportListStudentType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('textlisttuser', TextareaType::class, array('mapped' => false))
            ->add('userGroups', EntityType::class, array(
                'class' => UserGroup::class,
                'choice_label' => function (UserGroup $urg) {
                    return $urg->getLibelleAndCreator();
                },
                'required' => false,
                'multiple' => true,
                'choices' => $builder->getOption('listgroups')
            ))->add('save', SubmitType::class, array('label' => 'Importer la liste au format csv'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(

            'listgroups' => null,
            'data_class' => 'UserBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'rofesseurBundle_liststudent_import';
    }
}