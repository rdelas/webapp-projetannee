<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AdminBundle\Form\Teacher\AddTeacherType;
use AdminBundle\Form\Teacher\EditTeacherType;
use AdminBundle\Form\Teacher\PasswordTeacherType;

use UserBundle\Entity\User;

class AdminTeacherController extends Controller
{
    /**
     * @Route("/teacher", name="admin-teacher")
     */
    public function professeurAdminAction(Request $request)
    {
        $page = ($request->get('page') != null) ? $request->get('page') : 1;
        $element = ($request->get('elements') != null) ? $request->get('elements') : 10;
        $filter = ($request->get('filter') != null) ? $request->get('filter') : null;

        //Récupération des datas
        $listProfs = $this->get('admin.teacher')->findTeachersPagination($page, $element, $filter);

        //Formulaire d'edition
        $formsEditTeacher = array();
        for ($i = 0; $i < count($listProfs['users']); $i++) {
            $prof = $listProfs['users'][$i];
            $formEditTeacher = $this->get('form.factory')->createNamedBuilder("teacher_edit" . $prof->getId(), EditTeacherType::class, $prof)->getForm();

            $formEditTeacher->handleRequest($request);
            if ($formEditTeacher->isSubmitted() && $formEditTeacher->isValid()) {
                $this->get('user.manager')->updateMyUser($prof);

                $this->get("mail.controller")->sendSimpleMail($prof, "Notification de mise à jour de profil",  "Vous recevez cet email car un administrateur a mis à jour votre profil");

                return $this->render('AdminBundle:Teacher:admin-teacher-tline.html.twig', array(
                    'prof' => $prof,
                    'formEditTeacher' => $formEditTeacher->createView(),
                ));
            }
            array_push($formsEditTeacher, $formEditTeacher->createView());
        }


        $userForm = new User();
        $formAddTeacher = $this->createForm(AddTeacherType::class, $userForm);
        $formAddTeacher->handleRequest($request);
        $teacherFormEdit = $this->get('user.manager')->findUserByCanonicalEmail($formAddTeacher->getData()->getEmail(),"ROLE_PROF");
        if($teacherFormEdit=="ALLREADYTAKENEMAIL")
        {

            return new Response("<div class='alert alert-danger''>Cet email est déja utilisé pour un compte administrateur ou étudiant </div>");
        }
        if ($teacherFormEdit) {
            $teacherEdit = $this->createForm(AddTeacherType::class, $teacherFormEdit);
            $teacherEdit->handleRequest($request);
            if ($teacherEdit->isSubmitted() && $teacherEdit->isValid()) {
                $teacher = $teacherEdit->getData();
                $this->get('admin.teacher')->saveTeacher($teacher);
                $this->get("mail.controller")->sendSimpleMail($teacher, "Notification de mise à jour de profil", "Vous recevez cet email car un administrateur a mis à jour votre profil");


                return new Response("<div class='alert alert-warning''>Un professeur existait avec cet email , cependant  ses informations ont étés mises à jour</div>");
            }
        } else {
            if ($formAddTeacher->isSubmitted() && $formAddTeacher->isValid()) {
                $this->get('user.manager')->registerUser($userForm, array("ROLE_PROF"), true);
                return new Response("<div class='alert alert-success'>Le professeur " . $userForm->getFullname() . " a été ajouté</div>");
            }
        }


        /*
        //Formulaire d'ajout
        $user = new User();
        $formAddTeacher = $this->createForm(AddTeacherType::class, $user);
        $formAddTeacher->handleRequest($request);
        if ($formAddTeacher->isSubmitted() && $formAddTeacher->isValid()) {
            $this->get('user.manager')->registerUser($user, array('ROLE_PROF'), false);
            return new Response();
        }*/

        //Reception pagination

        if ($request->get('paginate') != null) {
            return $this->render('AdminBundle:Teacher:admin-teacher-table.html.twig', array(
                'profs' => $listProfs['users'],
                'page' => $page,
                'formsEditTeacher' => $formsEditTeacher,
                'elements' => $element,
                'totalResults' => $listProfs["totalResults"],
                'totalPage' => $listProfs["totalPage"],
                'beginResult' => $listProfs["beginResult"],
                'endResult' => $listProfs["endResult"],
            ));
        }

        $teacherCard = $this->get('admin.teacher')->getTeacherCard();
        $repoCard = $this->get('admin.teacher')->getRepoCard();
        $studentCard = $this->get('admin.teacher')->getStudentCard();
        $submissionCard = $this->get('admin.teacher')->getSubmissionCard();
        return $this->render('AdminBundle:Teacher:admin-teacher.html.twig', array(
            'teacherCard' => $teacherCard,
            'repoCard' => $repoCard,
            'studentCard' => $studentCard,
            'submissionCard' => $submissionCard,
            'formAddTeacher' => $formAddTeacher->createView(),
            'formsEditTeacher' => $formsEditTeacher,
            'page' => $page,
            'elements' => $element,
            'totalResults' => $listProfs["totalResults"],
            'totalPage' => $listProfs["totalPage"],
            'users' => $listProfs["users"],
            'beginResult' => $listProfs["beginResult"],
            'endResult' => $listProfs["endResult"],
        ));
    }

    /**
     * @Route("/professeur/requestEditPasswordModal", name="admin-teacher-request-password-modal")
     */
    public function requestEditPasswordModal(Request $request)
    {
        $id = $request->get('id');
        $user = $this->get('user.manager')->findUserById($id);
        $form = $this->createForm(PasswordTeacherType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->get('user.manager')->updateMyUser($user);
            $request->getSession()->getFlashBag()->add('success', 'Le mot de passe a bien été changé !');
            return $this->render('AdminBundle:Teacher:admin-teacher-password-modal.html.twig', array(
                'form' => $form->createView(),
                'id' => $id,
            ));
        }
        return $this->render('AdminBundle:Teacher:admin-teacher-password-modal.html.twig', array(
            'form' => $form->createView(),
            'id' => $id,
        ));
    }
}
