<?php
/**
 * Created by PhpStorm.
 * User: remi
 * Date: 05/04/2016
 * Time: 14:28
 */

namespace AdminBundle\Controller;

use SharedBundle\Entity\Config;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use AdminBundle\Form\Config\AddConfigType;
use AdminBundle\Form\Config\EditConfigType;

class AdminConfigController extends Controller
{
    /**
     * @Route("/config", name="admin-config")
     */
    public function adminAction(Request $request)
    {
        $page = ($request->get('page') !=  null) ? $request->get('page') : 1;
        $element =  ($request->get('elements') !=  null) ? $request->get('elements') : 10;
        $filter = ($request->get('filter') != null) ? $request->get('filter') : null ;

        $configs = $this->get('admin.config')->findConfigsPagination($page,$element,$filter);

        //Formulaire d'edition
        $formsEditConfig = array();
        for ($i = 0; $i < count($configs['configs']); $i++) {
            $conf = $configs['configs'][$i];
            $formEditConfig = $this->get('form.factory')->createNamedBuilder("admin_edit_config".$conf->getId(), EditConfigType::class, $conf)->getForm();
            $formEditConfig->handleRequest($request);
            if($formEditConfig->isSubmitted() && $formEditConfig->isValid()) {
                $this->get('admin.config')->editConfig($conf);
                return new Response();
            }
            array_push($formsEditConfig, $formEditConfig->createView());
        }

        //Formulaire d'ajout
        $config = new Config();
        $formAddConfig = $this->createForm(AddConfigType::class, $config);
        $formAddConfig->handleRequest($request);
        if ($formAddConfig->isSubmitted() && $formAddConfig->isValid()) {
            $this->get('admin.config')->addConfig($config);
            return new Response();
        }

        //Reception pagination
        if($request->get('paginate') != null ) {
            return $this->render('AdminBundle:Config:admin-config-table.html.twig', array(
                'configs' => $configs['configs'],
                'page' => $page,
                'formsEditConfig' => $formsEditConfig,
                'elements' => $element,
                'totalResults' => $configs["totalResults"],
                'totalPage' => $configs["totalPage"],
                'beginResult' => $configs["beginResult"],
                'endResult' => $configs["endResult"],
            ));
        }

        //Réponse initial
        return $this->render('AdminBundle:Config:admin-config.html.twig', array(
            'page' => $page,
            'elements' => $element,
            'formAddConfig' => $formAddConfig->createView(),
            'formsEditConfig' => $formsEditConfig,
            'totalResults' => $configs["totalResults"],
            'totalPage' => $configs["totalPage"],
            'configs' => $configs["configs"],
            'beginResult' => $configs["beginResult"],
            'endResult' => $configs["endResult"],
        ));
    }

    /**
     * @Route("/config/test", name="admin-config-test")
     */
    public function testConnexionAction() {
        $result = $this->get('rest.manager')->statusJudge();

        return $this->render('AdminBundle:Config:admin-config-connexion-test.html.twig', array(
            'connectionStatus' => $this->get('rest.manager')->getJudgeStatus(),
            'judgeStatus' => (isset($result->{'status'})) ? $result->{'status'} == 'running' : false,
        ));
    }

    /**
     * @Route("/config/test/status", name="admin-config-test-status")
     */
    public function statusAction() {
        return new Response();
    }

    /**
     * @Route("/config/test/start", name="admin-config-test-s")
     */
    public function startAction() {
        if($this->checkJudgeConnexion()) return new Response();

        $result = $this->get('rest.manager')->startJudge();
        return $this->render('AdminBundle:Config:admin-config-test-action.html.twig', array(
            'status' => $result->{'status'},
            'message' => $result->{'exec'} ? "Démarrage du juge OK": "Le démarrage du juge à échoué",
            'exec' => $result->{'exec'},
        ));
    }


    /**
     * @Route("/config/test/start-build", name="admin-config-test-sb")
     */
    public function startBuildAction() {
        if($this->checkJudgeConnexion()) return new Response();

        $result = $this->get('rest.manager')->startBuildJudge();
        return $this->render('AdminBundle:Config:admin-config-test-action.html.twig', array(
            'status' => $result->{'status'},
            'message' => $result->{'exec'} ? "Construction et démarrage du juge OK": "La construction et démarrage du juge à échoué",
            'exec' => $result->{'exec'},
        ));
    }


    /**
     * @Route("/config/test/restart", name="admin-config-test-r")
     */
    public function restartAction() {
        if($this->checkJudgeConnexion()) return new Response();

        $result = $this->get('rest.manager')->restartJudge();
        return $this->render('AdminBundle:Config:admin-config-test-action.html.twig', array(
            'status' => $result->{'status'},
            'message' => $result->{'exec'} ? "Redémarrage du juge OK": "Le redémarrage du juge à échoué",
            'exec' => $result->{'exec'},
        ));
    }


    /**
     * @Route("/config/test/restart-build", name="admin-config-test-rb")
     */
    public function restartBuildAction() {
        if($this->checkJudgeConnexion()) return new Response();

        $result = $this->get('rest.manager')->restartBuildJudge();
        return $this->render('AdminBundle:Config:admin-config-test-action.html.twig', array(
            'status' => $result->{'status'},
            'message' => $result->{'exec'} ? "Construction et redémarrage du juge OK": "La construction et redémarrage du juge à échoué",
            'exec' => $result->{'exec'},
        ));
    }


    /**
     * @Route("/config/test/stop", name="admin-config-test-stop")
     */
    public function stopAction() {
        if($this->checkJudgeConnexion()) return new Response();

        $result = $this->get('rest.manager')->stopJudge();
        return $this->render('AdminBundle:Config:admin-config-test-action.html.twig', array(
            'status' => $result->{'status'},
            'message' => $result->{'exec'} ? "Arrêt du juge OK": "L'arrêt du juge à échoué",
            'exec' => $result->{'exec'},
        ));
    }

    /**
     * @return bool
     */
    private function checkJudgeConnexion(){
        $res = $this->get('rest.manager')->getJudgeStatus();

        return !isset($res) || !$res['status'];
    }
}