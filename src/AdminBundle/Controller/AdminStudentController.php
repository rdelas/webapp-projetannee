<?php
/**
 * Created by PhpStorm.
 * User: remi
 * Date: 18/02/2016
 * Time: 19:39
 */

namespace AdminBundle\Controller;

use AdminBundle\Form\Student\AddGroupType;
use AdminBundle\Form\Student\AddSudentType;
use AdminBundle\Form\Student\AdminEditStudentType;
use AdminBundle\Form\Student\EditGroupeType;
use AdminBundle\Form\Student\ImportListStudentType;
use AdminBundle\Form\Student\ListStudentsGroupPaginationType;
use SharedBundle\Entity\UserGroup;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\User;
use stdClass;
use UserBundle\Form\Groupe\GroupeMailType;

class AdminStudentController extends Controller
{

    /**
     * @Route("/students", name="admin-students")
     */
    public function etudiantAdminAction(Request $request)
    {
        $currUser = $this->get('security.token_storage')->getToken()->getUser();
        $listGroup = $this->findAllGroupes(true);
        $group = new UserGroup();
        $formAddGroup = $this->createForm(AddGroupType::class, $group);
        $formAddGroup->handleRequest($request);
        $flashbag = null;
        $tagclass = "success";
        if ($formAddGroup->isSubmitted() && $formAddGroup->isValid()) {
            $this->get('admin.students')->createGroup($group);
            $formAddGroup = $this->createForm(AddGroupType::class, new UserGroup());
            $listGroup = $this->findAllGroupes(true);
            $flashbag = "Le groupe " . $group->getLibelle() . ' a été ajouté avec succès';
        }
        $userForm = new User();
        $formAddEtudiant = $this->createForm(AddSudentType::class, $userForm, ['listgroups' => $listGroup]);
        $formAddEtudiant->handleRequest($request);
        $etudiantFormEdit = $this->get('user.manager')->findUserByCanonicalEmail($formAddEtudiant->getData()->getEmail(), "ROLE_ETU");
        if ($etudiantFormEdit == "ALLREADYTAKENEMAIL") {

            return new Response("<div class='alert alert-danger''>Cet email est déja utilisé pour un compte administrateur ou professeur </div>");
        }

        if ($etudiantFormEdit) {
            $etudiantEdit = $this->createForm(AddSudentType::class, $etudiantFormEdit, ['listgroups' => $listGroup]);
            $etudiantEdit->handleRequest($request);
            if ($etudiantEdit->isSubmitted() && $etudiantEdit->isValid()) {
                $etudiant = $etudiantEdit->getData();


                $this->get('admin.students')->saveEtudiant($etudiant);
                $this->get("mail.controller")->sendSimpleMail($etudiant, "Notification de mise à jour de profil", "Vous recevez cet email car un administrateur a mis à jour votre profil");

                $this->get('admin.students')->putUserInGroups($formAddEtudiant->get('userGroups')->getData(), array($etudiant));

                return new Response("<div class='alert alert-warning''>Un étudiant existait avec cet email , cependant il a été ajouté aux groupes et ses informations ont étés mises à jour</div>");
            }
        } else {
            if ($formAddEtudiant->isSubmitted() && $formAddEtudiant->isValid()) {

                $this->get('user.manager')->registerUser($userForm, array("ROLE_ETU"), true);
                $this->get('admin.students')->putUserInGroups($formAddEtudiant->get('userGroups')->getData(), array($userForm));
                return new Response("<div class='alert alert-success'>L'étudiant " . $userForm->getFullname() . " a été ajouté</div>");
            }
        }
        $formImportListStudent = $this->createForm(ImportListStudentType::class, $currUser, ['listgroups' => $listGroup]);
        $formImportListStudent->handleRequest($request);
        if ($formImportListStudent->isSubmitted() && $formImportListStudent->isValid()) {
            $response = $this->get('admin.students')->importStudentList($formImportListStudent->get('textlisttuser')->getData(), $formImportListStudent->get('userGroups')->getData(), 'ROLE_ETU');
            $formImportListStudent = $this->createForm(ImportListStudentType::class, $currUser, ['listgroups' => $listGroup]);
            $total = $response["students"];
            $added = $response["added"];
            $created = $response["created"];
            $rejected = $response["rejected"];

            if($rejected == 0){
                $flashbag = "La liste des étudiants a été importée avec succès";
                $tagclass = "success";
            } else if($added != 0 && $rejected != 0) {
                $flashbag =  "La liste des étudiants a été importée avec des erreurs";
                $tagclass = "warning";
            } else {
                $flashbag =  "La liste des étudiants n'a pas été importée";
                $tagclass = "danger";
            }

            $flashbag .=    "<br/>$total étudiants donnés : <br/>".
                "<ul><li>$added étudiants ajoutés aux groupes</li>".
                "<li>$created étudiants créés</li>".
                "<li>$rejected étudiants rejetés</li></ul>";
            
        }

        $listGroupEdit = array();
        foreach ($listGroup as $groupe) {
            $groupeEdit = $this->get('form.factory')->createNamedBuilder("groupe_edit" . $groupe->getId(), EditGroupeType::class, $groupe)->getForm();
            $groupeListStudent = $this->get('form.factory')->createNamedBuilder("groupe_list_user" . $groupe->getId(), ListStudentsGroupPaginationType::class, $groupe)->getForm();
            $groupeEditForm = $groupeEdit->createView();
            $groupeEditForm->vars['groupeListStudent'] = $groupeListStudent->createView();
            array_push($listGroupEdit, $groupeEditForm);
        }

        $formMail = $this->createForm(GroupeMailType ::class, null, ['listgroups' => $listGroup]);
        $listStudent = $this->createForm(ListStudentsGroupPaginationType::class);
        return $this->renderPage($formAddEtudiant, $listGroupEdit, $formAddGroup, $formImportListStudent, $listStudent, $formMail, $flashbag, $tagclass);

    }

    private function findAllGroupes($enabled)
    {
        return $this->get('admin.students')->findAllGroupes($enabled);
    }

    private function renderPage($formAddEtudiant, $listGroupEdit, $formAddGroup, $formImportListStudent, $listStudent, $formMail, $flashbag = null, $tagclass = null)
    {
        return $this->render('AdminBundle:Student:admin-students.html.twig', array(
            'listStudent' => $listStudent->createView(),
            'listGroupEdit' => $listGroupEdit
        , 'formAddEtudiant' => $formAddEtudiant->createView()

        , 'formAddGroup' => $formAddGroup->createView()

        , 'formImportListStudent' => $formImportListStudent->createView(),
            'formMail' => $formMail->createView(),
            'flashbag' => $flashbag,
            'tagclass' => $tagclass
        ));
    }

    /**
     * @Route("/students/get-student-group", name="admin-getStudent-groups-students")
     * @return Response
     */
    public function adminGetGroupes(Request $request)
    {
        $listGroup = $this->findAllGroupes($request->get('enabled') == 'true');
        $listGroupEdit = array();
        foreach ($listGroup as $groupe) {
            $groupeEdit = $this->get('form.factory')->createNamedBuilder("groupe_edit" . $groupe->getId(), EditGroupeType::class, $groupe)->getForm();
            $groupeListStudent = $this->get('form.factory')->createNamedBuilder("groupe_list_user" . $groupe->getId(), ListStudentsGroupPaginationType::class, $groupe)->getForm();
            $groupeEditForm = $groupeEdit->createView();
            $groupeEditForm->vars['groupeListStudent'] = $groupeListStudent->createView();
            array_push($listGroupEdit, $groupeEditForm);
        }
        return $this->render('AdminBundle:Student:admin-student-group-tbody.html.twig', array(
            'listGroupEdit' => $listGroupEdit
        ));
    }

    /**
     * @Route("/students/edit-student-group", name="admin-edit-students")
     * @return Response
     */
    public function editEtudiant(Request $request)
    {
        $formOfRequest = $this->createForm(AdminEditStudentType::class, new User());
        $formOfRequest->handleRequest($request);
        $idEdit = $formOfRequest->get('id')->getData();
        $etudiantFormEdit = $this->get('admin.students')->findUserbyId($idEdit);
        if ($etudiantFormEdit) {
            $etudiantEdit = $this->createForm(AdminEditStudentType::class, $etudiantFormEdit);
            $etudiantEdit->handleRequest($request);
            if ($etudiantEdit->isSubmitted() && $etudiantEdit->isValid()) {
                $etudiant = $etudiantEdit->getData();

                if ($etudiantEdit->get('Retirer')->getData()) {
                    if ($etudiantEdit->get('groupe')->getData()) {
                        $this->get('admin.students')->removeEtuFromGroup($etudiantEdit->get('groupe')->getData(), $etudiant);
                        $this->get("mail.controller")->sendSimpleMail($etudiant, "Notification de retrait de groupe",
                            "Vous recevez cet email car un administrateur vous a retiré du groupe. Cela signifie que vous ne pouvez plus soummettre des codes pour les projets associés ", null,
                            "Groupe : " . $etudiantEdit->get('groupe')->getData()->getLibelle());

                    }
                } else {
                    $this->get('admin.students')->saveEtudiant($etudiant);
                    $this->get("mail.controller")->sendSimpleMail($etudiant, "Notification de mise à jour de profil", "Vous recevez cet email car un administrateur a mis à jour votre profil");

                    return $this->render('AdminBundle:Student:row-admin-edit-student-table.html.twig', array(
                            'etu' => $etudiantEdit->createView(),
                            'trclass' => 'success'
                        )
                    );
                }
            }
        }
        return new Response();
    }

    /**
     * @Route("/students/admin-edit-student-group", name="admin-edit-students-group")
     * @return Response
     */
    public
    function editStudentGroupe(Request $request)
    {

        $listGroup = $this->get('admin.students')->getAllGroupes();
        foreach ($listGroup as $groupe) {
            $dislabedgroup = $groupe->isDisabled();
            $groupeEdit = $this->get('form.factory')->createNamedBuilder("groupe_edit" . $groupe->getId(), EditGroupeType::class, $groupe)->getForm();
            $groupeEdit->handleRequest($request);
            if ($groupeEdit->isSubmitted() && $groupeEdit->isValid()) {
                $dislabedSame = $groupe->isDisabled() == $dislabedgroup;
                $groupeListStudent = $this->get('form.factory')->createNamedBuilder("groupe_list_user" . $groupe->getId(), ListStudentsGroupPaginationType::class, $groupe)->getForm();
                $groupeEditForm = $groupeEdit->createView();
                $groupeEditForm->vars['groupeListStudent'] = $groupeListStudent->createView();
                // var_dump($groupeEdit);
                $this->get('admin.students')->saveGroupe($groupe);
                if ($dislabedSame) {
                    return $this->render('AdminBundle:Student:admin-student-group-tbody.html.twig', array(
                        'listGroupEdit' => array($groupeEditForm)
                    ));
                }
                return new Response();

            }
        }

        return new Response();
    }


    /**
     * @Route("/students/list-all-student-pagin", name="list-all-student-pagin")
     * @return Response
     */
    public function listStudentPagin(Request $request)
    {
        $listStudent = $this->createForm(ListStudentsGroupPaginationType::class);
        $listStudent->handleRequest($request);
        if ($listStudent->isSubmitted() && $listStudent->isValid()) {

            $nbResultPage = $listStudent->get('nbResultPage')->getData();
            $pageCourante = $listStudent->get('pageCourante')->getData();

            if ($nbResultPage <= 0) {
                return new Response();
            }

            if ($listStudent->get('actifsCb')->getData() != $listStudent->get('inactifsCb')->getData()) {
                $allStudents = $this->get('user.manager')->findActiveEtu($listStudent->get('actifsCb')->getData());
            } else {
                $allStudents = $this->get('admin.students')->getAllStudents();
            }


            $nbTotalInGroup = count($allStudents);
            $nbPages = ceil($nbTotalInGroup / $nbResultPage);
            if ($pageCourante >= $nbPages) {
                $pageCourante = $nbPages - 1;
            }


            $etudiants = array_slice($allStudents, $pageCourante * $nbResultPage, $nbResultPage);
            foreach ($etudiants as $key => $etudiant) {
                $etudiantEdit = $this->createForm(AdminEditStudentType::class, $etudiant);
                $etudiants[$key] = $etudiantEdit->createView();
            }

            //   $listStudent = $this->createForm(ListStudentsGroupPaginationType::class);

            return $this->render('AdminBundle:Student:admin-students-pagination.html.twig', array(
                'groupeListStudent' => $listStudent->createView(),
                'etudiants' => $etudiants,
                'nbResultPage' => $nbResultPage,
                'pageCourante' => $pageCourante,
                'nbPages' => $nbPages,
                'path' => 'list-all-student-pagin'
            ));
        }
        return new Response();
    }

    /**
     * @Route("/students/list-student-group", name="admin-students-group")
     * @return Response
     */
    public function listStudentGroupe(Request $request)
    {

        $listGroup = $this->get('admin.students')->getAllGroupes();
        foreach ($listGroup as $groupe) {
            $groupeListStudent = $this->get('form.factory')->createNamedBuilder("groupe_list_user" . $groupe->getId(), ListStudentsGroupPaginationType::class, $groupe)->getForm();
            $groupeListStudent->handleRequest($request);
            if ($groupeListStudent->isSubmitted() && $groupeListStudent->isValid()) {
                $nbResultPage = $groupeListStudent->get('nbResultPage')->getData();
                $pageCourante = $groupeListStudent->get('pageCourante')->getData();

                if ($nbResultPage <= 0) {
                    return new Response();
                }

                if ($groupeListStudent->get('actifsCb')->getData() != $groupeListStudent->get('inactifsCb')->getData()) {
                    $users = $this->get('user.manager')->findActiveInactiveEtuInGroup($groupe, $groupeListStudent->get('actifsCb')->getData());
                } else {
                    $users = $groupe->getUsers()->getValues();
                }
                $nbTotalInGroup = count($users);


                $nbPages = ceil($nbTotalInGroup / $nbResultPage);
                if ($pageCourante >= $nbPages) {
                    $pageCourante = $nbPages - 1;
                }


                $etudiants = array_slice($users, $pageCourante * $nbResultPage, $nbResultPage);
                foreach ($etudiants as $key => $etudiant) {
                    $etudiantEdit = $this->createForm(AdminEditStudentType::class, $etudiant, ['groupe' => $groupe]);
                    $etudiants[$key] = $etudiantEdit->createView();
                }


                return $this->render('AdminBundle:Student:admin-students-pagination.html.twig', array(
                    'groupeListStudent' => $groupeListStudent->createView(),
                    'etudiants' => $etudiants,
                    'nbResultPage' => $nbResultPage,
                    'pageCourante' => $pageCourante,
                    'nbPages' => $nbPages,
                    'path' => 'admin-students-group'
                ));
            }
        }

        return new Response();
    }

}