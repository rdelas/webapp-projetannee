<?php

namespace UserBundle\Service;

use Doctrine\ORM\EntityManager;

use FOS\UserBundle\Doctrine\UserManager;

use UserBundle\Entity\User;

/**
*
* A simple mailer solution
*
*/
class MailerService {

	protected $userManager;
	protected $mailer;
	protected $templating;

    /**
     * MailerService constructor.
     * @param UserManager $em
     * @param $mm
     * @param $tem
     */
    public function __construct(UserManager $em, $mm, $tem)
    {
        $this->userManager = $em;
        $this->mailer = $mm;
        $this->templating = $tem;
    }

    /**
     * @param $list
     * @param $professeur
     * @param $projet
     */
    public function notifyStudentsFromListOfProjectAdding($list, $professeur, $projet) {
    	foreach(explode($list->getDelimiter(), $list->getList()) as $emailAddress) {
            $this->sendNotificationMailTo(
                $emailAddress,
                ($this->userManager->findUserByEmail($emailAddress) != null) ? true : false,
                $professeur,
                $projet
            );
    	}
    }

    /**
     * @param $email
     * @param $isRegistered
     * @param $professeur
     * @param $projet
     * @return mixed
     */
    private function sendNotificationMailTo($email, $isRegistered, $professeurName, $projectName) {
    	$message = \Swift_Message::newInstance()
    		->setSubject('Notification d\'ajout dans un projet')
    		->setFrom('no-reply@oj.com')
	        ->setTo($email)
	        ->setBody(
	        	$this->templating->render('SharedBundle:Emails:project_notification_email.html.twig', 
	        		array('isRegistered' => $isRegistered, 'professeur' => $professeurName, 'projet' => $projectName)),
	        		'text/html; charset=utf-8'
	        );
	    
	    return $this->mailer->send($message);
    }
}

?>