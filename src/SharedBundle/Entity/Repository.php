<?php

namespace SharedBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\Tests\StringableObject;

/**
 * Repository
 *
 * @ORM\Table(name="repository")
 * @ORM\Entity(repositoryClass="SharedBundle\Repository\RepositoryRepository")
 */
class Repository
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=64)
     */
    private $libelle;

    /**
     * @var string
     *
     * @ORM\Column(name="sacha", type="string", length=2048)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_date", type="datetime")
     */
    private $createDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="begin_date", type="datetime")
     */
    private $beginDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="datetime")
     */
    private $endDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="limited", type="boolean")
     */
    private $limited;

    /**
     * @var array
     * @ORM\OneToMany(targetEntity="\SharedBundle\Entity\GrantedSubmission", mappedBy="repository")
     */
    protected $grantedSubmissions;


    /**
     * @var int
     *
     * @ORM\Column(name="max_commit_per_person", type="integer")
     */
    private $maxCommitPerPerson;

    /**
     * @ORM\ManyToOne(targetEntity="\UserBundle\Entity\User", inversedBy="repositories")
     * @ORM\JoinColumn(name="creator_id", referencedColumnName="id")
     */
    private $creator;

    /**
     * @ORM\ManyToOne(targetEntity="\SharedBundle\Entity\Language", inversedBy="repositories")
     * @ORM\JoinColumn(name="language_id", referencedColumnName="id")
     */
    private $language;

    /**
     * @var array
     * @ORM\OneToMany(targetEntity="\SharedBundle\Entity\TestCase", mappedBy="repository")
     */
    private $testCases;

    /**
     * @var array
     * @ORM\OneToMany(targetEntity="\SharedBundle\Entity\Submission", mappedBy="repository")
     */
    private $submissions;


    /**
     * @var array
     * @ORM\ManyToMany(targetEntity="\SharedBundle\Entity\UserGroup", inversedBy="repositories")
     * @ORM\JoinTable(name="user_group_in_repository")
     */
    protected $userGroups;

    /**
     *
     * @var float
     *
     * @ORM\Column(name="timeout", type="float")
     */
    private $timeout;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean")
     */
    private $enabled;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->testCases = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     *
     * @return Repository
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Repository
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     *
     * @return Repository
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return Repository
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set maxCommitPerPerson
     *
     * @param integer $maxCommitPerPerson
     *
     * @return Repository
     */
    public function setMaxCommitPerPerson($maxCommitPerPerson)
    {
        $this->maxCommitPerPerson = $maxCommitPerPerson;

        return $this;
    }

    /**
     * Get maxCommitPerPerson
     *
     * @return integer
     */
    public function getMaxCommitPerPerson()
    {
        return $this->maxCommitPerPerson;
    }

    /**
     * Set creator
     *
     * @param \UserBundle\Entity\User $creator
     *
     * @return Repository
     */
    public function setCreator(\UserBundle\Entity\User $creator = null)
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * Get creator
     *
     * @return \UserBundle\Entity\User
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * Set language
     *
     * @param \SharedBundle\Entity\Language $language
     *
     * @return Repository
     */
    public function setLanguage(\SharedBundle\Entity\Language $language = null)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return \SharedBundle\Entity\Language
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Add testCase
     *
     * @param \SharedBundle\Entity\TestCase $testCase
     *
     * @return Repository
     */
    public function addTestCase(\SharedBundle\Entity\TestCase $testCase)
    {
        $this->testCases[] = $testCase;

        return $this;
    }

    /**
     * Remove testCase
     *
     * @param \SharedBundle\Entity\TestCase $testCase
     */
    public function removeTestCase(\SharedBundle\Entity\TestCase $testCase)
    {
        $this->testCases->removeElement($testCase);
    }

    /**
     * Get testCases
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTestCases()
    {
        return $this->testCases;
    }

    /**
     * Add userGroup
     *
     * @param \SharedBundle\Entity\UserGroup $userGroup
     *
     * @return Repository
     */
    public function addUserGroup(\SharedBundle\Entity\UserGroup $userGroup)
    {
        $this->userGroups[] = $userGroup;
        $userGroup->addRepository($this);

        return $this;
    }

    /**
     * Remove userGroup
     *
     * @param \SharedBundle\Entity\UserGroup $userGroup
     */
    public function removeUserGroup(\SharedBundle\Entity\UserGroup $userGroup)
    {
        $this->userGroups->removeElement($userGroup);
    }

    /**
     * Get userGroups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserGroups()
    {
        return $this->userGroups;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return Repository
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set timeout
     *
     * @param float $timeout
     *
     * @return Repository
     */
    public function setTimeout($timeout)
    {
        $this->timeout = $timeout;

        return $this;
    }

    /**
     * Get timeout
     *
     * @return float
     */
    public function getTimeout()
    {
        return $this->timeout;
    }

    /**
     * Set beginDate
     *
     * @param \DateTime $beginDate
     *
     * @return Repository
     */
    public function setBeginDate($beginDate)
    {
        $this->beginDate = $beginDate;

        return $this;
    }

    /**
     * Get beginDate
     *
     * @return \DateTime
     */
    public function getBeginDate()
    {
        return $this->beginDate;
    }

    /**
     * Set label
     *
     * @param string $label
     *
     * @return Repository
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Add submission
     *
     * @param \SharedBundle\Entity\Submission $submission
     *
     * @return Repository
     */
    public function addSubmission(\SharedBundle\Entity\Submission $submission)
    {
        $this->submissions[] = $submission;

        return $this;
    }

    /**
     * Remove submission
     *
     * @param \SharedBundle\Entity\Submission $submission
     */
    public function removeSubmission(\SharedBundle\Entity\Submission $submission)
    {
        $this->submissions->removeElement($submission);
    }

    /**
     * Get submissions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubmissions()
    {
        return $this->submissions;
    }

    /**
     * Set limited
     *
     * @param boolean $limited
     *
     * @return Repository
     */
    public function setLimited($limited)
    {
        $this->limited = $limited;

        return $this;
    }

    /**
     * Get limited
     *
     * @return boolean
     */
    public function getLimited()
    {
        return $this->limited;
    }
}
