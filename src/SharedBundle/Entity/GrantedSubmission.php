<?php

namespace SharedBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \UserBundle\Entity\User;

/**
 * GrantedSubmission
 *
 * @ORM\Table(name="granted_submission")
 * @ORM\Entity(repositoryClass="SharedBundle\Repository\GrantedSubmissionRepository")
 */
class GrantedSubmission
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="\UserBundle\Entity\User", inversedBy="grantedSubmissions")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var Repository
     *
     * @ORM\ManyToOne(targetEntity="\SharedBundle\Entity\Repository", inversedBy="grantedSubmissions")
     * @ORM\JoinColumn(name="repository_id", referencedColumnName="id")
     */
    private $repository;

    /**
     * @var int
     *
     * @ORM\Column(name="nb", type="integer")
     */
    private $nb;


    /**
     * Set user
     *
     * @param \UserBundle\Entity\User $user
     *
     * @return GrantedSubmission
     */
    public function setUser(\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set repository
     *
     * @param \SharedBundle\Entity\Repository $repository
     *
     * @return GrantedSubmission
     */
    public function setRepository(\SharedBundle\Entity\Repository $repository = null)
    {
        $this->repository = $repository;

        return $this;
    }

    /**
     * Get repository
     *
     * @return \SharedBundle\Entity\Repository
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nb
     *
     * @param integer $nb
     *
     * @return GrantedSubmission
     */
    public function setNb($nb)
    {
        $this->nb = $nb;

        return $this;
    }

    /**
     * Get nb
     *
     * @return int
     */
    public function getNb()
    {
        return $this->nb;
    }
}
