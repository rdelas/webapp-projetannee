<?php

namespace SharedBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\ManyToOne as ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn as JoinColumn;
use Doctrine\ORM\Mapping\JoinTable as JoinTable;

/**
 * TestCase
 *
 * @ORM\Table(name="test_case")
 * @ORM\Entity(repositoryClass="SharedBundle\Repository\TestCaseRepository")
 */
class TestCase
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=64)
     */
    private $libelle;

    /**
     * @var blob
     *
     * @ORM\Column(name="input", type="blob")
     */
    private $input;

    /**
     * @var blob
     *
     * @ORM\Column(name="output", type="blob")
     */
    private $output;

    /**
     *
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean")
     */
    private $enabled;

    /**
     * @ManyToOne(targetEntity="\SharedBundle\Entity\Repository", inversedBy="testCases")
     * @JoinColumn(name="repo_id", referencedColumnName="id")
     */
    private $repository;

    /**
     * @ORM\OneToMany(targetEntity="\SharedBundle\Entity\RunResult", mappedBy="testCase")
     */
    private $runResults;

    /**
     * @var boolean
     *
     * @ORM\Column(name="final", type="boolean")
     */
    private $final;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->runResults = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     *
     * @return TestCase
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set input
     *
     * @param string $input
     *
     * @return TestCase
     */
    public function setInput($input)
    {
        $this->input = $input;

        return $this;
    }

    /**
     * Get input
     *
     * @return string
     */
    public function getInput()
    {
        return isset($this->input) ? (gettype($this->input) == "resource") ? stream_get_contents($this->input) : $this->input : null;
    }

    /**
     * Set output
     *
     * @param string $output
     *
     * @return TestCase
     */
    public function setOutput($output)
    {
        $this->output = $output;

        return $this;
    }

    /**
     * Get output
     *
     * @return string
     */
    public function getOutput()
    {
        return isset($this->output) ? (gettype($this->input) == "resource") ? stream_get_contents($this->output) : $this->output : null;
    }

    /**
     * Set repository
     *
     * @param \SharedBundle\Entity\Repository $repository
     *
     * @return TestCase
     */
    public function setRepository(\SharedBundle\Entity\Repository $repository = null)
    {
        $this->repository = $repository;

        return $this;
    }

    /**
     * Get repository
     *
     * @return \SharedBundle\Entity\Repository
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * Add runResult
     *
     * @param \SharedBundle\Entity\RunResult $runResult
     *
     * @return TestCase
     */
    public function addRunResult(\SharedBundle\Entity\RunResult $runResult)
    {
        $this->runResults[] = $runResult;

        return $this;
    }

    /**
     * Remove runResult
     *
     * @param \SharedBundle\Entity\RunResult $runResult
     */
    public function removeRunResult(\SharedBundle\Entity\RunResult $runResult)
    {
        $this->runResults->removeElement($runResult);
    }

    /**
     * Get runResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRunResults()
    {
        return $this->runResults;
    }

    /**
     * Set final
     *
     * @param boolean $final
     *
     * @return TestCase
     */
    public function setFinal($final)
    {
        $this->final = $final;

        return $this;
    }

    /**
     * Get final
     *
     * @return boolean
     */
    public function getFinal()
    {
        return $this->final;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return TestCase
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }
}
