<?php

namespace SharedBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * RunResult
 *
 * @ORM\Table(name="run_result")
 * @ORM\Entity(repositoryClass="SharedBundle\Repository\RunResultRepository")

 */
class RunResult
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(name="result", type="string", length=255, nullable=true)
     */
    private $result;

    /**
     * @var blob
     *
     * @ORM\Column(name="log", type="blob", nullable=true)
     */
    private $log;


    /**
     * @ORM\ManyToOne(targetEntity="\SharedBundle\Entity\TestCase", inversedBy="runResults")
     * @ORM\JoinColumn(name="test_case_id", referencedColumnName="id")
     */
    private $testCase;

    /**
     * @ORM\ManyToOne(targetEntity="\SharedBundle\Entity\Submission", inversedBy="runResults")
     * @ORM\JoinColumn(name="submission_id", referencedColumnName="id")
     */
    private $submission;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set result
     *
     * @param string $result
     *
     * @return RunResult
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * Get result
     *
     * @return string
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Set log
     *
     * @param string $log
     *
     * @return RunResult
     */
    public function setLog($log)
    {
        $this->log = $log;

        return $this;
    }

    /**
     * Get log
     *
     * @return string
     */
    public function getLog()
    {
        return isset($this->log) ? (gettype($this->log) == "resource") ? stream_get_contents($this->log) : $this->log : null;
    }

    /**
     * Set testCase
     *
     * @param \SharedBundle\Entity\TestCase $testCase
     *
     * @return RunResult
     */
    public function setTestCase(\SharedBundle\Entity\TestCase $testCase = null)
    {
        $this->testCase = $testCase;

        return $this;
    }

    /**
     * Get testCase
     *
     * @return \SharedBundle\Entity\TestCase
     */
    public function getTestCase()
    {
        return $this->testCase;
    }

    /**
     * Set submission
     *
     * @param \SharedBundle\Entity\Submission $submission
     *
     * @return RunResult
     */
    public function setSubmission(\SharedBundle\Entity\Submission $submission = null)
    {
        $this->submission = $submission;

        return $this;
    }

    /**
     * Get submission
     *
     * @return \SharedBundle\Entity\Submission
     */
    public function getSubmission()
    {
        return $this->submission;
    }
}
