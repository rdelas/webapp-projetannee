<?php

namespace SharedBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Language
 *
 * @ORM\Table(name="language")
 * @ORM\Entity(repositoryClass="SharedBundle\Repository\LanguageRepository")
 */
class Language
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=30)
     */
    private $libelle;

    /**
     * @ORM\OneToMany(targetEntity="\SharedBundle\Entity\Repository", mappedBy="language")
     */
    protected $repositories;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->repositories = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     *
     * @return Language
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Add repository
     *
     * @param \SharedBundle\Entity\Repository $repository
     *
     * @return Language
     */
    public function addRepository(\SharedBundle\Entity\Repository $repository)
    {
        $this->repositories[] = $repository;

        return $this;
    }

    /**
     * Remove repository
     *
     * @param \SharedBundle\Entity\Repository $repository
     */
    public function removeRepository(\SharedBundle\Entity\Repository $repository)
    {
        $this->repositories->removeElement($repository);
    }

    /**
     * Get repositories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRepositories()
    {
        return $this->repositories;
    }
}
