<?php

namespace SharedBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use UserBundle\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Submission
 *
 * @ORM\Table(name="submission")
 * @ORM\Entity(repositoryClass="SharedBundle\Repository\SubmissionRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Submission
{
    private $temp;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @Assert\File(maxSize="6000000")
     */
    private $file;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pathToCode;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="\UserBundle\Entity\User", inversedBy="submissions")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var Repository
     *
     * @ORM\ManyToOne(targetEntity="\SharedBundle\Entity\Repository", inversedBy="submissions")
     * @ORM\JoinColumn(name="repository_id", referencedColumnName="id")
     */
    private $repository;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateCommit", type="datetime")
     */
    private $dateCommit;

    /**
     * @var string
     *
     * @ORM\Column(name="result", type="string", length=255, nullable=true)
     */
    private $result;

    /**
     * @var string
     *
     * @ORM\Column(name="log", type="string", length=1024, nullable=true)
     */
    private $log;

    /**
     * @var string
     *
     * @ORM\Column(name="buildresult", type="string", length=255, nullable=true)
     */
    private $buildResult;

    /**
     * @var blob
     *
     * @ORM\Column(name="buildlog", type="blob", nullable=true)
     */
    private $buildLog;

    /**
     * @ORM\OneToMany(targetEntity="\SharedBundle\Entity\RunResult", mappedBy="submission")
     */
    protected $runResults;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function getAbsolutePath()
    {
        return null === $this->pathToCode
            ? null
            : $this->getUploadRootDir().'/'.$this->pathToCode;
    }

    public function getWebPath()
    {
        return null === $this->pathToCode
            ? null
            : $this->getUploadDir().'/'.$this->pathToCode;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/code';
    }

    public function getFixturesPath()
    {

        return $this->getAbsolutePath() . '.web/uploads/code/fixtures/';
    }


    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
        // check if we have an old image path
        if (isset($this->pathToCode)) {
            // store the old name to delete after the update
            $this->temp = $this->pathToCode;
            $this->pathToCode = null;
        } else {
            $this->pathToCode = 'initial';
        }
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->getFile()) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->pathToCode = $filename.'.'.$this->getFile()->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->getFile()) {
            return;
        }

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->getFile()->move($this->getUploadRootDir(), $this->pathToCode);

        // check if we have an old image
        if (isset($this->temp)) {
            // delete the old image
            unlink($this->getUploadRootDir().'/'.$this->temp);
            // clear the temp image path
            $this->temp = null;
        }
        $this->file = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        $file = $this->getAbsolutePath();
        if ($file) {
            unlink($file);
        }
    }

    /**
     * Set pathToCode
     *
     * @param string $pathToCode
     *
     * @return RunResult
     */
    public function setPathToCode($pathToCode)
    {
        $this->pathToCode = $pathToCode;

        return $this;
    }

    /**
     * Get pathToCode
     *
     * @return string
     */
    public function getPathToCode()
    {
        return $this->pathToCode;
    }

    /**
     * Set user
     *
     * @param \UserBundle\Entity\User $user
     *
     * @return Submission
     */
    public function setUser(\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set repository
     *
     * @param \SharedBundle\Entity\Repository $repository
     *
     * @return Submission
     */
    public function setRepository(\SharedBundle\Entity\Repository $repository = null)
    {
        $this->repository = $repository;

        return $this;
    }

    /**
     * Get repository
     *
     * @return \SharedBundle\Entity\Repository
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * Set dateCommit
     *
     * @param \DateTime $dateCommit
     *
     * @return Submission
     */
    public function setDateCommit($dateCommit)
    {
        $this->dateCommit = $dateCommit;

        return $this;
    }

    /**
     * Get dateCommit
     *
     * @return \DateTime
     */
    public function getDateCommit()
    {
        return $this->dateCommit;
    }

    /**
     * Set result
     *
     * @param string $result
     *
     * @return Submission
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * Get result
     *
     * @return string
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Set log
     *
     * @param string $log
     *
     * @return Submission
     */
    public function setLog($log)
    {
        $this->log = $log;

        return $this;
    }

    /**
     * Get log
     *
     * @return string
     */
    public function getLog()
    {
        return $this->log;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->runResults = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add runResult
     *
     * @param \SharedBundle\Entity\Submission $runResult
     *
     * @return Submission
     */
    public function addRunResult(\SharedBundle\Entity\Submission $runResult)
    {
        $this->runResults[] = $runResult;

        return $this;
    }

    /**
     * Remove runResult
     *
     * @param \SharedBundle\Entity\Submission $runResult
     */
    public function removeRunResult(\SharedBundle\Entity\Submission $runResult)
    {
        $this->runResults->removeElement($runResult);
    }

    /**
     * Get runResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRunResults()
    {
        return $this->runResults;
    }

    /**
     * Set buildResult
     *
     * @param string $buildResult
     *
     * @return Submission
     */
    public function setBuildResult($buildResult)
    {
        $this->buildResult = $buildResult;

        return $this;
    }

    /**
     * Get buildResult
     *
     * @return string
     */
    public function getBuildResult()
    {
        return $this->buildResult;
    }

    /**
     * Set buildLog
     *
     * @param string $buildLog
     *
     * @return Submission
     */
    public function setBuildLog($buildLog)
    {
        $this->buildLog = $buildLog;

        return $this;
    }

    /**
     * Get buildLog
     *
     * @return string
     */
    public function getBuildLog()
    {
        return isset($this->buildLog) ? (gettype($this->buildLog) == "resource") ? stream_get_contents($this->buildLog) : $this->buildLog : null;
    }
}
