<?php

namespace SharedBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable as JoinTable;
use Doctrine\ORM\Mapping\JoinColumn as JoinColumn;
/**
 * UserGroup
 *
 * @ORM\Table(name="user_group")
 * @ORM\Entity(repositoryClass="SharedBundle\Repository\UserGroupRepository")
 */
class UserGroup
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var disabled
     * @ORM\Column(name="disabled", type="boolean", options={"default":false})
     */
    protected $disabled;


    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=100)
     */
    private $libelle;

    /**
     * @var array
     * @ORM\ManyToMany(targetEntity="\UserBundle\Entity\User", inversedBy="userGroups",cascade={"persist"})
     * @JoinTable(name="user_in_group",
     *      joinColumns={@JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="group_id", referencedColumnName="id")}
     *      )
     */
    private $users;

    /**
     * @var array
     * @ORM\ManyToMany(targetEntity="\SharedBundle\Entity\Repository", mappedBy="userGroups")
     */
    private $repositories;

    /**
     * @ORM\ManyToOne(targetEntity="\UserBundle\Entity\User", inversedBy="createdGroups")
     * @ORM\JoinColumn(name="creator_id", referencedColumnName="id")
     */
    private $creator;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     *
     * @return UserGroup
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }
    /**
     * Enable Disable group
     *
     * @param boolean enabled
     *
     * @return UserGroup
     */
    public function setEnabled($enabled)
    {
        $this->disabled=!$enabled;
        return $this;
    }
    /**
     * is group enabled
     *
     * @return boolean
     */
    public function isEnabled()
    {
        return !$this->disabled;
    }
    /**
     * is group disabled
     *
     * @return boolean
     */
    public function isDisabled()
    {
        return $this->disabled;
    }

    public function setDisabled($disabled)
    {
        $this->disabled=$disabled;
        return $this;
    }
    /**
     * is group enabled
     *
     * @return boolean
     */




    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Get libelle and creator
     *
     * @return string
     */
    public function getLibelleAndCreator()
    {
        return $this->libelle.' ('.$this->creator->getFullname().')';
    }
    /**
     * Add user
     *
     * @param \UserBundle\Entity\User $user
     *
     * @return UserGroup
     */
    public function addUser(\UserBundle\Entity\User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \UserBundle\Entity\User $user
     */
    public function removeUser(\UserBundle\Entity\User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Add repository
     *
     * @param \SharedBundle\Entity\Repository $repository
     *
     * @return UserGroup
     */
    public function addRepository(\SharedBundle\Entity\Repository $repository)
    {
        $this->repositories[] = $repository;

        return $this;
    }

    /**
     * Remove repository
     *
     * @param \SharedBundle\Entity\Repository $repository
     */
    public function removeRepository(\SharedBundle\Entity\Repository $repository)
    {
        $this->repositories->removeElement($repository);
    }

    /**
     * Get repositories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRepositories()
    {
        return $this->repositories;
    }

    /**
     * Set creator
     *
     * @param \UserBundle\Entity\User $creator
     *
     * @return UserGroup
     */
    public function setCreator(\UserBundle\Entity\User $creator = null)
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * Get creator
     *
     * @return \UserBundle\Entity\User
     */
    public function getCreator()
    {
        return $this->creator;
    }
}
