<?php

namespace UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use SharedBundle\Entity\Language;
use SharedBundle\Entity\UserGroup;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUserGroupData extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $userManager = $this->container->get('fos_user.user_manager');

        $noms = ["Finley", "Morton", "Harvey", "Cooley", "Lamb", "Thornton", "Duffy", "Bond", "Mcknight", "Mcneil", "Weiss", "Woods", "Franklin"];
        $prenoms = ["Len", "Grady", "Nolan", "Clark", "Adrian", "Holmes", "Abraham", "Rigel", "Garrett", "Warren", "Quinn", "Elijah", "Christian"];

        for($i = 0; $i < 3; $i++) {
            $group = new UserGroup();
            $group->setLibelle("groupe " . $i);
            $group->setCreator($this->getReference("prof"));
            $group->setEnabled(true);
            $manager->persist($group);
            $this->addReference("group_" . $i, $group);

            for($j = 0; $j < 5; $j++) {
                // Create our user and set details
                $user = $userManager->createUser();
                $user->setFirstName($prenoms[$j + $i]);
                $user->setLastName($noms[$j + $i]);
                $user->setUsername('etu' . $i . $j);
                $user->setEmail('etu' . $i . $j . '@etu.com');
                $user->setPlainPassword('etu');
                $user->setNbConnexion(4);
                $user->setRegisterDate(new \DateTime());
                $user->setEnabled(true);
                $user->setRoles(array('ROLE_ETU'));

                // Lien au groupe
                $user->addUserGroup($group);

                // Update the user
                $userManager->updateUser($user, true);
            }
        }

        $manager->flush();
    }

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 2;
    }
}
?>