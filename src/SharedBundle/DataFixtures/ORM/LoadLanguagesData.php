<?php

namespace UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use SharedBundle\Entity\Language;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

class LoadLanguagesData extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $codes = array("c", "cpp", "make", "maven", "java","python");
        foreach($codes as $code) {

            $l = new Language();
            $l->setLibelle($code);

            $manager->persist($l);
            $this->addReference($code, $l);
        }

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 0;
    }
}
?>