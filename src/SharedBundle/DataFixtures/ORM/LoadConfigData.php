<?php
/**
 * Created by PhpStorm.
 * User: remi
 * Date: 05/04/2016
 * Time: 14:13
 */

namespace SharedBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use SharedBundle\Entity\Config;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

class LoadConfigData extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {

        $config = new Config();
        $config->setEnabled(true);
        $config->setLabel("Tomcat-Linux");
        $config->setPort(8080);
        $config->setAddr("localhost");
        $config->setUri("CoreAppJudge");
        $manager->persist($config);

        $config = new Config();
        $config->setEnabled(false);
        $config->setLabel("localhost");
        $config->setPort(8086);
        $config->setAddr("localhost");
        $config->setUri("");
        $manager->persist($config);

        $config = new Config();
        $config->setEnabled(false);
        $config->setLabel("localhost tomcat");
        $config->setPort(8086);
        $config->setAddr("localhost");
        $config->setUri("CoreAppJudge");
        $manager->persist($config);

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 6;
    }
}