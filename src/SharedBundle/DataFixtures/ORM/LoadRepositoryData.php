<?php

namespace UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use SharedBundle\Entity\GrantedSubmission;
use SharedBundle\Entity\Repository;
use SharedBundle\Entity\TestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class LoadRepositoryData extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{
    const LOREM = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut convallis ac massa at ultrices. Sed id arcu eu est ultricies lobortis. Integer pulvinar consequat ultricies. Donec bibendum pretium justo id finibus. Vivamus venenatis tincidunt arcu, at molestie neque condimentum vel. Cras in tincidunt massa, eu sagittis nisi. Phasellus a maximus mauris. Proin condimentum erat lobortis condimentum pharetra. Mauris ullamcorper tempus orci, id facilisis diam posuere tempor. Aliquam nibh purus, aliquam sit amet augue non, lobortis cursus risus. Nunc sit amet ante euismod, tincidunt ex nec, accumsan quam. Etiam mollis est dolor, eget laoreet lorem suscipit nec. Maecenas pulvinar nunc elit, in facilisis ligula commodo at. Praesent quis scelerisque felis, at tincidunt libero. Phasellus semper odio consectetur ipsum condimentum luctus. Nulla et vestibulum neque.<br/><br/>Cras malesuada vel lectus nec semper. Sed cursus ex tortor, vitae mattis odio varius et. Cras mattis nulla in bibendum sollicitudin. Ut ipsum justo, lobortis id dapibus non, fermentum ultrices sem. Proin condimentum, nunc a egestas posuere, justo est tincidunt nibh, quis aliquam quam risus ac lectus. Nunc vulputate blandit nisl quis facilisis. Maecenas sit amet feugiat orci, sit amet ornare nisi. Morbi ac justo at sapien efficitur aliquam nec sed nunc. Nullam tristique ligula in augue ornare aliquam. Aliquam pulvinar nulla eu sapien fermentum, in commodo tortor tempus. Nam placerat, augue non placerat vehicula, sapien dui ultrices magna, vel porta felis leo at dui. ";

    public function load(ObjectManager $manager)
    {
        $d = new \DateTime( '2016-05-24' );
        $d->modify( 'next month');

        /*
            Projet 42 Python
         */
        $repository = new Repository();
        $repository->setLibelle("Projet 42 python");
        $repository->setDescription("Repeter les chiffre jusqu'a ce que le nombre 42 soit donné.<br/>" . self::LOREM);
        $repository->setCreateDate(new \DateTime());
        $repository->setBeginDate(new \DateTime());
        $repository->setEndDate($d);
        $repository->setMaxCommitPerPerson(5);
        $repository->setLimited(true);
        $repository->setCreator($this->getReference("prof"));
        $repository->setLanguage($this->getReference("python"));
        $repository->setTimeout(1.000);
        $repository->addUserGroup($this->getReference("group_0"));
        $repository->addUserGroup($this->getReference("group_1"));
        $repository->addUserGroup($this->getReference("group_2"));
        $repository->setEnabled(true);

        $manager->persist($repository);
        $this->setReference("repo_1", $repository);

        $gs = new GrantedSubmission();
        $gs->setUser($this->getReference('etu'));
        $gs->setNb(2);
        $gs->setRepository($repository);

        $manager->persist($gs);

        $input = "12\n232\n42\n345\n34534\n232\n34534\n234\n234\n42\n2342\n4322\n123\n";
        $output = "12\n232\n";
        $testCase = new TestCase();
        $testCase->setLibelle("Testcase 1");
        $testCase->setInput($input);
        $testCase->setOutput($output);
        $testCase->setFinal(false);
        $testCase->setEnabled(true);
        $testCase->setRepository($repository);

        $manager->persist($testCase);
        $this->setReference("test_case_1", $testCase);

        $input = "12\n45\n31233\n45\n12\n232\n42\n345\n34534\n232\n34534\n234\n234\n42\n2342\n4322\n123\n";
        $output = "12\n45\n31233\n45\n12\n232\n";
        $testCase = new TestCase();
        $testCase->setLibelle("Testcase 2");
        $testCase->setInput($input);
        $testCase->setOutput($output);
        $testCase->setFinal(false);
        $testCase->setEnabled(true);
        $testCase->setRepository($repository);

        $input = "12\n45\n31233\n232\n42\n345\n34534\n232\n34534\n234\n234\n42\n2342\n4322\n123\n";
        $output = "12\n45\n31233\n232\n";
        $testCase = new TestCase();
        $testCase->setLibelle("Testcase 2");
        $testCase->setInput($input);
        $testCase->setOutput($output);
        $testCase->setFinal(false);
        $testCase->setEnabled(true);
        $testCase->setRepository($repository);

        $manager->persist($testCase);

        /*
            Projet Hello world C
         */
        $repository = new Repository();
        $repository->setLibelle("Projet Hello world c");
        $repository->setDescription("Afficher le text \"Hello World\" à l'écran<br/>" . self::LOREM);
        $repository->setCreateDate(new \DateTime());
        $repository->setBeginDate(new \DateTime());
        $repository->setEndDate($d);
        $repository->setMaxCommitPerPerson(5);
        $repository->setLimited(false);
        $repository->setCreator($this->getReference("prof_1"));
        $repository->setLanguage($this->getReference("c"));
        $repository->setTimeout(1.000);
        $repository->setEnabled(true);

        $manager->persist($repository);
        $this->setReference("repo_2", $repository);

        $testCase = new TestCase();
        $testCase->setLibelle("Test hello world");
        $testCase->setInput("\n");
        $testCase->setOutput("Hello World\n");
        $testCase->setFinal(false);
        $testCase->setEnabled(true);
        $testCase->setRepository($repository);

        $manager->persist($testCase);
        $this->setReference("test_case_2", $testCase);


        /*
            Projet 42 Cpp
         */
        $repository = new Repository();
        $repository->setLibelle("Projet 42 cpp");
        $repository->setDescription("Repeter les chiffre jusqu'a ce que le nombre 42 soit donné.\n" . self::LOREM);
        $repository->setCreateDate(new \DateTime());
        $repository->setBeginDate(new \DateTime());
        $repository->setEndDate($d);
        $repository->setMaxCommitPerPerson(5);
        $repository->setCreator($this->getReference("prof"));
        $repository->setLanguage($this->getReference("cpp"));
        $repository->setTimeout(1.000);
        $repository->setLimited(false);
        $repository->addUserGroup($this->getReference("group_0"));
        $repository->addUserGroup($this->getReference("group_1"));
        $repository->addUserGroup($this->getReference("group_2"));
        $repository->setEnabled(true);

        $manager->persist($repository);
        $this->setReference("repo_3", $repository);

        $testCase = new TestCase();
        $testCase->setLibelle("Test suite simple");
        $testCase->setInput("12\n232\n42\n345\n34534\n232\n34534\n234\n234\n42\n2342\n4322\n123\n");
        $testCase->setOutput("12\n232\n");
        $testCase->setFinal(false);
        $testCase->setEnabled(true);
        $testCase->setRepository($repository);

        $manager->persist($testCase);
        $this->setReference("test_case_3", $testCase);

        /*
            Projet 42 Java
        */
        $repository = new Repository();
        $repository->setLibelle("Projet 42 java");
        $repository->setDescription("Repeter les chiffre jusqu'a ce que le nombre 42 soit donné.<br/>" . self::LOREM);
        $repository->setCreateDate(new \DateTime());
        $repository->setBeginDate(new \DateTime());
        $repository->setEndDate($d);
        $repository->setMaxCommitPerPerson(5);
        $repository->setCreator($this->getReference("prof"));
        $repository->setLanguage($this->getReference("java"));
        $repository->setTimeout(1.000);
        $repository->setLimited(false);
        $repository->addUserGroup($this->getReference("group_0"));
        $repository->addUserGroup($this->getReference("group_1"));
        $repository->addUserGroup($this->getReference("group_2"));
        $repository->setEnabled(true);

        $manager->persist($repository);
        $this->setReference("repo_4", $repository);

        $testCase = new TestCase();
        $testCase->setLibelle("Test suite simple");
        $testCase->setInput("12\n232\n42\n345\n34534\n232\n34534\n234\n234\n42\n2342\n4322\n123\n");
        $testCase->setOutput("12\n232\n");
        $testCase->setFinal(false);
        $testCase->setEnabled(true);
        $testCase->setRepository($repository);

        $manager->persist($testCase);
        $this->setReference("test_case_4", $testCase);

        /*
            Projet 42 Make
        */
        $repository = new Repository();
        $repository->setLibelle("Projet 42 make");
        $repository->setDescription("Repeter les chiffre jusqu'a ce que le nombre 42 soit donné.<br/>" . self::LOREM);
        $repository->setCreateDate(new \DateTime());
        $repository->setBeginDate(new \DateTime());
        $repository->setEndDate($d);
        $repository->setMaxCommitPerPerson(5);
        $repository->setCreator($this->getReference("prof"));
        $repository->setLanguage($this->getReference("make"));
        $repository->setTimeout(1.000);
        $repository->setLimited(true);
        $repository->addUserGroup($this->getReference("group_0"));
        $repository->addUserGroup($this->getReference("group_1"));
        $repository->addUserGroup($this->getReference("group_2"));
        $repository->setEnabled(true);

        $manager->persist($repository);
        $this->setReference("repo_5", $repository);

        $gs = new GrantedSubmission();
        $gs->setUser($this->getReference('etu'));
        $gs->setNb(3);
        $gs->setRepository($repository);

        $testCase = new TestCase();
        $testCase->setLibelle("Test suite simple");
        $testCase->setInput("12\n232\n42\n345\n34534\n232\n34534\n234\n234\n42\n2342\n4322\n123\n");
        $testCase->setOutput("12\n232\n");
        $testCase->setFinal(false);
        $testCase->setEnabled(true);
        $testCase->setRepository($repository);

        $manager->persist($testCase);
        $this->setReference("test_case_5", $testCase);

        /*
            Projet Maven
        */
        $repository = new Repository();
        $repository->setLibelle("Projet maven");
        $repository->setDescription("Description du projet Maven<br/>" . self::LOREM);
        $repository->setCreateDate(new \DateTime());
        $repository->setBeginDate(new \DateTime());
        $repository->setEndDate($d);
        $repository->setMaxCommitPerPerson(5);
        $repository->setCreator($this->getReference("prof"));
        $repository->setLanguage($this->getReference("maven"));
        $repository->setTimeout(1.000);
        $repository->setLimited(false);
        $repository->addUserGroup($this->getReference("group_0"));
        $repository->addUserGroup($this->getReference("group_1"));
        $repository->addUserGroup($this->getReference("group_2"));
        $repository->setEnabled(true);

        $manager->persist($repository);
        $this->setReference("repo_5", $repository);

        $manager->flush();
    }
    /**
     * @return int
     */
    public function getOrder()
    {
        return 3;
    }
}
?>