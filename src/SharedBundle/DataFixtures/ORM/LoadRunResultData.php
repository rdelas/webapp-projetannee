<?php

namespace UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use SharedBundle\Entity\RunResult;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class LoadRunResultData extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $group = $this->getReference("group_0");


      /* foreach($group->getUsers() as $user) {
            /*$runResult = new RunResult();
            $runResult->setDateCommit(new \DateTime());
            $runResult->setLog("------------\n -- Log ---\n------------\n ");
            $runResult->setScore("13/30");
            $runResult->setTestCase($this->getReference("test_case_1"));
            $runResult->setUser($user);

            $file = new File(__DIR__ . "../../)
            $runResult->setFile($file);
            $runResult->setResult("WRONG ANSWER");
            $manager->persist($runResult);

            $manager->persist($runResult);
        }*/

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 4;
    }
}
?>