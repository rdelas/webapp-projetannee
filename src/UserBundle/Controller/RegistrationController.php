<?php

namespace UserBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use FOS\UserBundle\Controller\RegistrationController as FOSRegistrationController;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\User\UserInterface;
use UserBundle\Form\Profil\PasswordConfirmType;

class RegistrationController extends FOSRegistrationController
{

    /**
     * Tell the user his account is now confirmed
     */
    public function confirmedAction()
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }
        $formPassword = $this->createForm(PasswordConfirmType  ::class, $user);
        return $this->render('UserBundle:Profil:user-confirm-password.html.twig',array(
            'formPassword'=> $formPassword->createView()
        ));
    }
}
