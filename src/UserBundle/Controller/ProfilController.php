<?php
namespace UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Form\Profil\PasswordConfirmType;
use UserBundle\Form\Profil\UserForgotSomethingType;

class ProfilController extends Controller
{

    /**
     * @Route("/confirmPassword", name="user-confirm-password")
     */
    public function userConfirmPassword(Request $request)
    {

        $currUser = $this->get('security.token_storage')->getToken()->getUser();
        $formPassword = $this->createForm(PasswordConfirmType ::class, $currUser);
        $formPassword->handleRequest($request);
        if ($formPassword->isSubmitted() && $formPassword->isValid()) {
            $this->get('user.manager')->updateMyUser($currUser);

            $this->get("mail.controller")->sendSimpleMail($currUser, "Notification de validation du mot de passe" , "Votre mot de passe viens d'être validé avec succès sur Student Code Judge. Si vous n'êtes pas à l'origine de cette modification, utilisez la fonction de mot de passe oublié pour en définir un sécurisé", null, "Votre mot de passe a été modifié");
            $response = $this->forward('AppBundle:Default:index');
            return $response;
        }
        return $this->render('UserBundle:Profil:user-confirm-password.html.twig', array(
            'formPassword' => $formPassword->createView()
        ));

    }

    /**
     * @Route("/changePassword", name="user-change-password")
     */
    public function userChangePassword(Request $request)
    {

        $currUser = $this->get('security.token_storage')->getToken()->getUser();
        $formPassword = $this->createForm(PasswordConfirmType ::class, $currUser);
        $formPassword->handleRequest($request);
        if ($formPassword->isSubmitted() && $formPassword->isValid()) {
            $this->get('user.manager')->updateMyUser($currUser);

            $this->get("mail.controller")->sendSimpleMail($currUser, "Notification de changement de mot de passe" , "Votre mot de passe viens d'être modifié sur Student Code Judge. Si vous n'êtes pas à l'origine de cette modification, utilisez la fonction de mot de passe oublié pour en définir un sécurisé", null, "Votre mot de passe a été modifié");

            return $this->render('UserBundle:Profil:confirmSomethingToUser.html.twig', array(
                'formPassword' => $formPassword->createView(),
                'message' => 'Votre mot de passe a été modifié !',
            ));
        }
        return $this->render('UserBundle:Profil:user-change-password.html.twig', array(
            'formPassword' => $formPassword->createView(),
            'message' => 'Formulaire de modification du mot de passe',
            'message2' => 'Pensez à définir un mot de passe sécurisé'
        ));

    }

    /**
     * @Route("/resetting", name="user-forgot-something")
     */
    public function userForgotSomething(Request $request)
    {

        $formForgot = $this->createForm(UserForgotSomethingType ::class);
        $formForgot->handleRequest($request);
        if ($formForgot->isSubmitted() && $formForgot->isValid()) {
            $userResetAsked = $this->get('user.manager')->findUserByCanonicalEmail($formForgot->get("Email")->getData());

            if ($userResetAsked && $userResetAsked->isEnabled()) {

                if ($formForgot->get('login')->isClicked()) {
                    $this->get('mail.controller')->sendEmailLogin($userResetAsked);
                    return $this->render('UserBundle:Profil:user-forgot-something.html.twig', array(
                        'formForgot' => $formForgot->createView()
                    , 'message' => "Vérifiez votre boite e-mail, " . $userResetAsked->getFullname()
                    , 'message2' => "Nous avons envoyé votre Identifiant à " . $userResetAsked->getEmailCanonical() . ". Renvoyer?"
                    ));
                } else {
                    $this->get('mail.controller')->sendEmailResetPassword($userResetAsked);
                    return $this->render('UserBundle:Profil:user-forgot-something.html.twig', array(
                        'formForgot' => $formForgot->createView()
                    , 'message' => "Pas de panique, " . $userResetAsked->getFullname()
                    , 'message2' => "Nous avons envoyé un e-mail de réintitailisation à " . $userResetAsked->getEmailCanonical() . ". Renvoyer?"
                    ));
                }
            }
            return $this->render('UserBundle:Profil:user-forgot-something.html.twig', array(
                'formForgot' => $formForgot->createView()
            , 'message' => "Désolé, Cette adresse ne correspond à aucun de nos comptes"
            , 'message2' => 'Entrez votre adresse, pour recevoir un mail et retrouver vos informations'
            ));
            //   $response = $this->forward('AppBundle:Default:index');
            // return $response;
        }
        return $this->render('UserBundle:Profil:user-forgot-something.html.twig', array(
            'formForgot' => $formForgot->createView()
        , 'message' => 'Vous avez perdu votre identifiant ou votre mot de passe?'
        , 'message2' => 'Entrez votre adresse, pour recevoir un mail et le retrouver'
        ));

    }
}
