<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 22/05/2016
 * Time: 19:25
 */

namespace UserBundle\Controller;


use FOS\UserBundle\Doctrine\UserManager;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Util\TokenGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use UserBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UserBundle\Form\Groupe\GroupeMailType;


class UserMailController extends Controller
{

    protected $container;
    protected $userManager;
    protected $tokenGenerator;

    public function giveToken($user)
    {
        $user->setConfirmationToken($this->tokenGenerator->generateToken());
        $this->userManager->updateUser($user);
    }

    public function __construct(Container $container = null, UserManager $userManager = null, TokenGenerator $tokenGenerator = null)
    {
        $this->container = $container;
        $this->userManager = $userManager;
        $this->tokenGenerator = $tokenGenerator;
    }

    /**
     * @Route("/mailGroupe", name="mail-groupe")
     * @return Response
     */

    public function routeMailForGroup(Request $request)
    {
        $um = $this->get('user.manager');

        $currUser = $this->get('security.token_storage')->getToken()->getUser();
        if ($currUser->isAdmin()) {
            $listGroup = $this->get('admin.students')->findAllGroupes(true);
        } else {
            $listGroup = $this->get('teacher.students')->findAllGroupesOfTeacher($currUser);
        }
        $formMail = $this->createForm(GroupeMailType ::class, null, ['listgroups' => $listGroup]);
        $formMail->handleRequest($request);
        if ($formMail->isSubmitted() && $formMail->isValid()) {


            $userGroups = $formMail->get('userGroups')->getData();
            $formMail->get('usersList')->getData();
            $subjectMail = $formMail->get('objectmail')->getData();
            $textMail = $formMail->get('textmail')->getData();
            $studentEmailsArray = explode(';', $formMail->get('usersList')->getData());
            $actif = "ALL";
            if ($formMail->get('actifsCb')->getData() != $formMail->get('inactifsCb')->getData()) {
                $actif = $formMail->get('actifsCb')->getData();
            }

            foreach ($studentEmailsArray as $email) {
                if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $userToMail = $um->findUserByCanonicalEmail($email);
                    if ($userToMail) {
                        if ($userToMail->isEnabled() == $actif || !is_bool($actif)) {
                            $this->sendSimpleMail($userToMail,
                                "Student Code Judge : " . $subjectMail, $textMail, $currUser);
                        }
                    }
                }
            }

            foreach ($userGroups as $groupe) {
                if (is_bool($actif)) {


                    $usersToMail = $this->get('user.manager')->findActiveInactiveEtuInGroup($groupe, $actif);
                } else {

                    $usersToMail = $groupe->getUsers()->getValues();
                }
                foreach ($usersToMail as $userToMail) {
                    $this->sendSimpleMail($userToMail,
                        "Student Code Judge : " . $subjectMail, $textMail, $currUser);
                }
            }
            return $this->render('UserBundle:Groupe:groupe-mail.html.twig', array(
                'formMail' => $formMail->createView(),
                'message' => 'Le mail a été envoyé aux utilisateurs et groupes sélectionnés'

            ));
        }


        return $this->render('UserBundle:Groupe:groupe-mail.html.twig', array(
            'formMail' => $formMail->createView(),

        ));
    }

    public function sendSimpleMail(UserInterface $user, $objet, $message = "", User $sender = null, $objet2 = null)
    {
        $protocole = "http://";
        if (isset($_SERVER['HTTPS'])) {
            if ($_SERVER['HTTPS'] == "on") {
                $protocole = "https://";
            }
        }
        $url = $this->generateUrl('fos_user_security_login');

        $message = \Swift_Message::newInstance()
            ->setSubject($objet)
            ->setFrom("no.reply.soj@gmail.com")
            ->setTo($user->getEmail())
            ->setContentType('text/html')
            ->setBody(
                $this->renderView(
                    "UserBundle:Groupe:SimpleMailBody.html.twig", array(
                    'user' => $user,
                    'objet2' => $objet2,
                    'sender' => $sender,
                    'message' => $message,
                    'confirmationUrl' => $protocole . $this->getParameter("mailer_host") . $url))
            );
        $sent = $this->get('mailer')->send($message);

        return ['user' => $user,
            'url' => $url,
            'success' => $sent ? 'Yes' : 'No'];
    }


    public function sendEmailAcountActivation(User $user)
    {
        $this->giveToken($user);
        $protocole = "http://";
        if (isset($_SERVER['HTTPS'])) {
            if ($_SERVER['HTTPS'] == "on") {
                $protocole = "https://";
            }
        }
        $url = $this->generateUrl('fos_user_registration_confirm', array('type' => 'confirm', 'token' => $user->getConfirmationToken()), true);

        $message = \Swift_Message::newInstance()
            ->setSubject('Activez votre compte : Vous avez été inscrit sur Student Code Judge')
            ->setFrom("no.reply.soj@gmail.com")
            ->setTo($user->getEmail())
            ->setContentType('text/html')
            ->setBody(
                $this->renderView(
                    "UserBundle:Registration:ActiveAcountEmail.html.twig", array(
                    'user' => $user,
                    'confirmationUrl' => $protocole . $this->getParameter("mailer_host") . $url))
            );
        $sent = $this->get('mailer')->send($message);

        return ['user' => $user,
            'url' => $url,
            'success' => $sent ? 'Yes' : 'No'];

    }

    public function sendEmailLogin(User $user)
    {
        $protocole = "http://";
        if (isset($_SERVER['HTTPS'])) {
            if ($_SERVER['HTTPS'] == "on") {
                $protocole = "https://";
            }
        }
        $url = $this->generateUrl('fos_user_security_login');

        $message = \Swift_Message::newInstance()
            ->setSubject('Student Code Judge : Vous avez demandé votre identifiant')
            ->setFrom("no.reply.soj@gmail.com")
            ->setTo($user->getEmail())
            ->setContentType('text/html')
            ->setBody(
                $this->renderView(
                    "UserBundle:Registration:AskLoginEmail.html.twig", array(
                    'user' => $user,
                    'confirmationUrl' => $protocole . $this->getParameter("mailer_host") . $url))
            );
        $sent = $this->get('mailer')->send($message);

        return ['user' => $user,
            'url' => $url,
            'success' => $sent ? 'Yes' : 'No'];

    }


    public function sendEmailResetPassword(User $user)
    {

        $this->giveToken($user);

        $protocole = "http://";
        if (isset($_SERVER['HTTPS'])) {
            if ($_SERVER['HTTPS'] == "on") {
                $protocole = "https://";
            }
        }

        $url = $this->generateUrl('fos_user_registration_confirm', array('type' => 'reset', 'token' => $user->getConfirmationToken()), true);

        $message = \Swift_Message::newInstance()
            ->setSubject('Mot de passe oublié : Vous avez demandé une réinitialisation sur Student Code Judge')
            ->setFrom("no.reply.soj@gmail.com")
            ->setTo($user->getEmail())
            ->setContentType('text/html')
            ->setBody(
                $this->renderView(
                    "UserBundle:Registration:ResetPasswordEmail.html.twig", array(
                    'user' => $user,
                    'confirmationUrl' => $protocole . $this->getParameter("mailer_host") . $url))
            );
        $sent = $this->get('mailer')->send($message);

        return ['user' => $user,
            'url' => $url,
            'success' => $sent ? 'Yes' : 'No'];

    }

}