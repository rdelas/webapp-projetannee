<?php

namespace UserBundle\Service;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\Security\Core\SecurityContext;
use FOS\UserBundle\Doctrine\UserManager;
use UserBundle\Entity\User;

/**
 *
 * A simple user managing solution with two methods : addUser and removeUser
 *
 */
class UserService {

    protected $context;
    protected $userManager;

    public function __construct(SecurityContext $context, UserManager $um) {
        $this->context = $context;
        $this->userRepository = $em->getRepository('UserBundle:User');
    }

    /**
     * Register user in datatbase
     *
     * @param MyUser $myUser
     * @param $roles
     * @return bool
     */
    public function registerUser(User $user, $roles) {

        $new_user = $this->userManager->createUser();
        $new_user->setUserName($user->getUserName());
        $new_user->setEmail($user->getEmail());
        $new_user->setFirstname($user->getFirstname());
        $new_user->setLastname($user->getLastname());
        $new_user->setRegisterDate(new \DateTime());
        $new_user->setNbConnexion(0);
        $new_user->setEnabled(true);
        $new_user->setPlainPassword($user->getPassword());
        $new_user->setRoles($roles);
        $this->userManager->updateUser($new_user);
        return true;
    }

    public function updateMyUser(User $user) {
        $this->userManager->updateUser($user);
    }

    public function findUserById($id) {
        $result = $this->userManager->findUserBy(array('id' => $id));
        return $result;
    }

    public function findUsersPagination($roles, $page, $elements) {
        $results = array();
        $totalResults = $this->userRepository->countAllUsers($roles);
        $totalPage = ceil( $totalResults / $elements );
        $users = $this->userRepository->findAllUsersWithLimit($roles, $elements, (($page - 1) * $elements));
        $results["totalResults"] = $totalResults;
        $results["totalPage"] = $totalPage;
        $results["users"] = $users;
        $results["beginResult"] = (($page - 1) * $elements) + 1;
        $results["endResult"] = ($page == $totalPage) ? $totalResults : $page * $elements;
        return $results;
    }
}