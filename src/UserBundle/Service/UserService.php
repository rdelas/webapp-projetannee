<?php

namespace UserBundle\Service;

use Doctrine\ORM\EntityManager;

use FOS\UserBundle\Doctrine\UserManager;

use FOS\UserBundle\Util\TokenGenerator;
use UserBundle\Controller\UserMailController;
use UserBundle\Entity\User;

/**
 *
 * A simple user managing solution with two methods : addUser and removeUser
 *
 */
class UserService
{

    protected $userManager;
    protected $userRepository;
    protected $em;
    protected $mailController;
    protected $tokenGenerator;

    public function __construct(UserManager $um, EntityManager $em, UserMailController $mailController, TokenGenerator $tokenGenerator)
    {
        $this->userManager = $um;
        $this->userRepository = $em->getRepository('UserBundle:User');
        $this->em = $em;

        $this->mailController = $mailController;
        $this->tokenGenerator = $tokenGenerator;
    }

    /**
     * Register user in datatbase
     *
     * @param MyUser $myUser
     * @param $roles
     * @return bool
     */

    public function registerUser(User $user, $roles, $generatedUser)
    {
        if ($generatedUser) {
            if(!$user->getPassword()) {
                $user->setPassword(random_bytes(10));
            }
            $user->setUsername($user->getLastname() . random_bytes(20));
        }
        $user->setRoles($roles);
        $user->setRegisterDate(new \DateTime());
        $user->setNbConnexion(0);
        $this->userManager->updateUser($user);

        if ($generatedUser) {
            $user->setUsername($user->getLastname() . "-" . $user->getId());
            $this->userManager->updateUser($user);
        }
        $this->mailController->sendEmailAcountActivation($user);

        return true;
    }

    public
    function updateMyUser(User $user)
    {
        $this->userManager->updateUser($user);
    }
    public function giveToken($user)
    {
        $user->setConfirmationToken($this->tokenGenerator->generateToken());
        $this->userManager->updateUser($user);
    }

    public
    function findUserById($id)
    {
        $result = $this->userManager->findUserBy(array('id' => $id));
        return $result;
    }

    public
    function findUserByUserName($username)
    {
        $result = $this->userManager->findUserBy(array('username' => $username));
        return $result;
    }

    public
    function findUserByCanonicalUserName($usernameCanonical)
    {
        $result = $this->userManager->findUserBy(array('usernameCanonical' => strtolower($usernameCanonical)));
        return $result;
    }

    public
    function findUserByCanonicalEmail($anyEmail, $role = false)
    {
        $result = $this->userManager->findUserBy(array('emailCanonical' => strtolower($anyEmail)));
        if ($result && $role) {
            return in_array($role, $result->getRoles()) ? $result : "ALLREADYTAKENEMAIL";
        }
        return $result;
    }

    public function findActiveInactiveEtuInGroup($groupe, $actif=true) {
        return $this->userRepository->findActiveInactiveEtuInGroup($groupe,$actif);
    }
    public function findActiveEtu($actif=true)
    {
        return $this->userRepository->findActiveEtu($actif);
    }


}

?>