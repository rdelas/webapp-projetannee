<?php
namespace UserBundle\Form\Profil;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserForgotSomethingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('Email', EmailType::class , array('mapped'=>false,'attr'=>array("class"=>"form-control")))
        ->add('login',SubmitType::class,array("label"=>"Recevoir mon Identifiant par Email",'attr'=>array("class"=>"btn btn-primary")))
        ->add('password',SubmitType::class ,array("label"=>"Réinitialiser mon mot de passe",'attr'=>array("class"=>"btn btn-primary")));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array());
    }

    public function getName()
    {
        return 'user_confirm_password';
    }

}