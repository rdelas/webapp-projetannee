<?php

namespace UserBundle\Form\Groupe;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 25/05/2016
 * Time: 20:10
 */
class GroupeMailType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('objectmail', TextType ::class, array('label' => "Objet de l'e-mail :", 'mapped' => false))
            ->add('textmail', TextareaType::class, array('label' => "Corps du message :", 'mapped' => false))
            ->add('usersList', TextareaType::class, array('label' => "Envoyer aux étudiants :", 'mapped' => false, 'required' => false))
            ->add("actifsCb", CheckboxType::class, array('mapped' => false, 'required' => false, 'data' => $options['actifsCb'], 'label' => ' Actifs ', "label_attr" => array('class' => 'label label-sm label-success'), "attr" => array('class' => 'checkbox-pagination-side-label')))
            ->add("inactifsCb", CheckboxType::class, array('mapped' => false, 'required' => false, 'data' => $options['inactifsCb'], 'label' => 'Inactifs', "label_attr" => array('class' => 'label label-sm label-danger'), "attr" => array('class' => 'checkbox-pagination-side-label')))
            ->add('userGroups', EntityType::class, array(
                'class' => 'SharedBundle:UserGroup',
                'choice_label' => 'libelle',
                'required' => false,
                'multiple' => true,
                'choices' => $builder->getOption('listgroups')
            , 'label' => "Envoyer aux groupes :"
            ), array())
            ->add('Envoyer', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array('listgroups' => null, 'actifsCb' => true,
            'inactifsCb' => true,
        ));
    }

    public function getName()
    {
        return 'userBundle_mailGroupe';
    }



}