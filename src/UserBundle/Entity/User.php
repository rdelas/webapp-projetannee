<?php

namespace UserBundle\Entity;

use DateTime;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=30)
     */
    protected $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=30)
     */
    protected $lastname;

    /**
     * @var datetime
     *
     * @ORM\Column(name="registerDate", type="datetime")
     */
    protected $registerDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbConnexion", type="integer")
     */
    protected $nbConnexion;

    /**
     * @var array
     * @ORM\OneToMany(targetEntity="\SharedBundle\Entity\Submission", mappedBy="user")
     */
    protected $submissions;

    /**
     * @var array
     * @ORM\OneToMany(targetEntity="\SharedBundle\Entity\GrantedSubmission", mappedBy="user")
     */
    protected $grantedSubmissions;

    /**
     * @var array
     * @ORM\OneToMany(targetEntity="\SharedBundle\Entity\Repository", mappedBy="creator")
     */
    protected $repositories;

    /**
     * @var array
     * @ORM\OneToMany(targetEntity="\SharedBundle\Entity\UserGroup", mappedBy="creator")
     */
    protected $createdGroups;

    /**
     * @var array
     * @ORM\ManyToMany(targetEntity="\SharedBundle\Entity\UserGroup", mappedBy="users")
     */
    protected $userGroups;


    public function __construct()
    {
        parent::__construct();
        // your own logic
    }


    /**
     * Add runResult
     *
     * @param \SharedBundle\Entity\RunResult $runResult
     *
     * @return User
     */
    public function addRunResult(\SharedBundle\Entity\RunResult $runResult)
    {
        $this->runResults[] = $runResult;

        return $this;
    }

    /**
     * Remove runResult
     *
     * @param \SharedBundle\Entity\RunResult $runResult
     */
    public function removeRunResult(\SharedBundle\Entity\RunResult $runResult)
    {
        $this->runResults->removeElement($runResult);
    }

    /**
     * Get runResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRunResults()
    {
        return $this->runResults;
    }

    /**
     * Add repository
     *
     * @param \SharedBundle\Entity\Repository $repository
     *
     * @return User
     */
    public function addRepository(\SharedBundle\Entity\Repository $repository)
    {
        $this->repositories[] = $repository;

        return $this;
    }

    /**
     * Set ID : Turnaround for a bug, does nothing
     */
    public function setId($nothing)
    {
        //Fake method
    }

    /**
     * Remove repository
     *
     * @param \SharedBundle\Entity\Repository $repository
     */
    public function removeRepository(\SharedBundle\Entity\Repository $repository)
    {
        $this->repositories->removeElement($repository);
    }

    /**
     * Get repositories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRepositories()
    {
        return $this->repositories;
    }

    /**
     * Add userGroup
     *
     * @param \SharedBundle\Entity\UserGroup $userGroup
     *
     * @return User
     */
    public function addUserGroup(\SharedBundle\Entity\UserGroup $userGroup)
    {
        $this->userGroups[] = $userGroup;
        $userGroup->addUser($this);

        return $this;
    }

    /**
     * Remove userGroup
     *
     * @param \SharedBundle\Entity\UserGroup $userGroup
     */
    public function removeUserGroup(\SharedBundle\Entity\UserGroup $userGroup)
    {
        $this->userGroups->removeElement($userGroup);
    }

    /**
     * Get userGroups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserGroups()
    {
        return $this->userGroups;
    }

    /**
     * Add createdGroup
     *
     * @param \SharedBundle\Entity\UserGroup $createdGroup
     *
     * @return User
     */
    public function addCreatedGroup(\SharedBundle\Entity\UserGroup $createdGroup)
    {
        $this->createdGroups[] = $createdGroup;

        return $this;
    }

    /**
     * Remove createdGroup
     *
     * @param \SharedBundle\Entity\UserGroup $createdGroup
     */
    public function removeCreatedGroup(\SharedBundle\Entity\UserGroup $createdGroup)
    {
        $this->createdGroups->removeElement($createdGroup);
    }

    /**
     * Get createdGroups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCreatedGroups()
    {
        return $this->createdGroups;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function setUserGroups($userGroupArray)
    {
        $this->userGroups=$userGroupArray;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Get fullname
     *
     * @return the full name of the user
     */
    public function getFullname()
    {
        return $this->firstname . ' ' . $this->lastname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set nbConnexion
     *
     * @param integer $nbConnexion
     *
     * @return User
     */
    public function setNbConnexion($nbConnexion)
    {
        $this->nbConnexion = $nbConnexion;

        return $this;
    }

    /**
     * Get nbConnexion
     *
     * @return integer
     */
    public function getNbConnexion()
    {
        return $this->nbConnexion;
    }

    /**
     * Set registerDate
     *
     * @param \DateTime $registerDate
     *
     * @return User
     */
    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    /**
     * Get registerDate
     *
     * @return \DateTime
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }
}
