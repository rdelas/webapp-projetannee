<?php

namespace UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUserData extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $userManager = $this->container->get('fos_user.user_manager');

        // Create our user and set details
        $user = $userManager->createUser();
        $user->setFirstName("Caesar");
        $user->setLastName("Parsons");
        $user->setUsername('admin');
        $user->setEmail('admin@admin.com');
        $user->setNbConnexion(4);
        $user->setRegisterDate(new \DateTime());
        $user->setPlainPassword('admin');
        $user->setEnabled(true);
        $user->setRoles(array('ROLE_ADMIN'));

        // Update the user
        $userManager->updateUser($user, true);

        // Create our user and set details
        $user = $userManager->createUser();
        $user->setFirstName("Kato");
        $user->setLastName("Burke");
        $user->setUsername("prof");
        $user->setEmail("prof@prof.com");
        $user->setNbConnexion(4);
        $user->setRegisterDate(new \DateTime());
        $user->setPlainPassword('prof');
        $user->setEnabled(true);
        $user->setRoles(array('ROLE_PROF'));

        // Update the user
        $userManager->updateUser($user, true);
        $this->setReference("prof", $user);

        // Create our user and set details
        $user = $userManager->createUser();
        $user->setFirstName("Honorato");
        $user->setLastName("Mays");
        $user->setUsername("etu");
        $user->setNbConnexion(4);
        $user->setRegisterDate(new \DateTime());
        $user->setEmail("etu@etu.com");
        $user->setPlainPassword('etu');
        $user->setEnabled(true);
        $user->setRoles(array('ROLE_ETU'));

        // Update the user
        $userManager->updateUser($user, true);
        $this->setReference("etu", $user);

        for($j = 0; $j < 5; $j++) {

            // Create our user and set details
            $user = $userManager->createUser();
            $user->setFirstName("Kato$j");
            $user->setLastName("Burke$j");
            $user->setUsername("prof$j");
            $user->setEmail("prof$j@prof.com");
            $user->setNbConnexion(4);
            $user->setRegisterDate(new \DateTime());
            $user->setPlainPassword('prof');
            $user->setEnabled(true);
            $user->setRoles(array('ROLE_PROF'));

            // Update the user
            $userManager->updateUser($user, true);
            $this->setReference("prof_$j", $user);

            for($i = 0; $i < 5; $i++) {

                // Create our user and set details
                $user = $userManager->createUser();
                $user->setFirstName("Honorato-$j-$j$i");
                $user->setLastName("Mays-$j-$j$i");
                $user->setUsername("etu_$j-$j$i");
                $user->setNbConnexion(4);
                $user->setRegisterDate(new \DateTime());
                $user->setEmail("etu_$j$i@etu.com");
                $user->setPlainPassword('etu');
                $user->setEnabled(true);
                $user->setRoles(array('ROLE_ETU'));

                // Update the user
                $userManager->updateUser($user, true);
                $this->setReference("etu_$j-$j$i", $user);
            }
        }
    }

    /**
	 * @var ContainerInterface
	 */
	private $container;

	/**
	 * {@inheritDoc}
	 */
	public function setContainer(ContainerInterface $container = null)
	{
	    $this->container = $container;
	}

    /**
     * @return int
     */
    public function getOrder()
    {
        return 1;
    }
}
?>
