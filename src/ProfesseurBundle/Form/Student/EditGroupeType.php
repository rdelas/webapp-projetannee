<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 12/04/2016
 * Time: 16:29
 */

namespace ProfesseurBundle\Form\Student;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EditGroupeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('libelle', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Renommer'))
            ->add('disabled', CheckboxType::class, array('required' => false));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(

            'groupe' => null,
        ));
    }


    public function getName()
    {
        return 'professeurBundle__groupe_edit';
    }
}