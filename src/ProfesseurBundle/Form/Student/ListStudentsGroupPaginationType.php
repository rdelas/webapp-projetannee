<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 10/04/2016
 * Time: 16:41
 */

namespace ProfesseurBundle\Form\Student;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ListStudentsGroupPaginationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nbResultPage', ChoiceType::class, array('mapped' => false, 'data' => $options['nbResultPage'], 'choices' => array(
                '5' => 5, '10' => 10, '15' => 15, '20' => 20, '25' => 25, '30' => 30, '35' => 35, '40' => 40, '45' => 45, '50' => 50,
                '55' => 55, '60' => 60, '75' => 75, '80' => 80, '85' => 85, '90' => 90, '95' => 95, '100' => 100
            )))
            ->add("actifsCb", CheckboxType::class, array('mapped' => false, 'required' => false, 'data' => $options['actifsCb'], 'label' => ' Actifs ', "label_attr" => array('class' => 'label label-sm label-success'), "attr" => array('class' => 'checkbox-pagination-isactif checkbox-pagination-side-label')))
            ->add("inactifsCb", CheckboxType::class, array('mapped' => false, 'required' =>false, 'data' =>  $options['inactifsCb'], 'label' => 'Inactifs', "label_attr" => array('class' => 'label label-sm label-danger'), "attr" => array('class' => 'checkbox-pagination-isactif checkbox-pagination-side-label')))
            ->add('pageCourante', IntegerType ::class, array('mapped' => false, 'data' => $options['pageCourante']));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'actifsCb' => true,
            'inactifsCb' => true,
            'pageCourante' => 0,
            'nbResultPage' => 15
        ));
    }

    public function getName()
    {
        return 'professeurBundle_pagination';
    }

}