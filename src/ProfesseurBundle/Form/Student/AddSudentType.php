<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 02/04/2016
 * Time: 14:13
 */

namespace ProfesseurBundle\Form\Student;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use Symfony\Component\OptionsResolver\OptionsResolver;


class AddSudentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class)
            ->add('firstname', TextType::class)
            ->add('lastname', TextType::class)
            ->add('userGroups', EntityType::class, array(
                'class' => 'SharedBundle:UserGroup',
                'choice_label' => 'libelle',
                'mapped'=>false,
                'required' => true,
                'multiple' => true,
                'choices' => $builder->getOption('listgroups')
            ))->add('save', SubmitType::class, array('label' => 'Ajouter un étudiant'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(

            'listgroups' => null,
        ));
    }

    public function getName()
    {
        return 'professeurBundle_student_add';
    }
}