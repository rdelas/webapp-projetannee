<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 10/04/2016
 * Time: 16:41
 */

namespace ProfesseurBundle\Form\Student;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class AddGroupType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('libelle', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Ajouter le groupe'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array());
    }

    public function getName()
    {
        return 'professeurBundle_group_add';
    }

}