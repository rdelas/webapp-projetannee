<?php
/**
 * Created by PhpStorm.
 * User: remi
 * Date: 11/04/2016
 * Time: 14:29
 */
namespace ProfesseurBundle\Form\Repositories;

use SharedBundle\Repository\UserGroupRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use UserBundle\Entity\User;


class EditAffectRepositoryType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @return mixed
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('userGroups', EntityType::class, array(
                'class' => 'SharedBundle:UserGroup',
                'choice_label' => 'libelle',
                'required' => false,
                'multiple' => true,
                'query_builder' => function (UserGroupRepository $ugr) use ($builder) {
                    return $ugr->createQueryBuilder('u')
                        ->where('identity(u.creator) = :id')
                        ->setParameter('id', $builder->getOption('id'));
                }))
            ->add('save', SubmitType::class, array('label' => 'Editer les affectations'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(

            'id' => null,
            'data_class' => 'SharedBundle\Entity\Repository'
        ));
    }

    public function getName()
    {
        return 'teacherBundle_repositories_testCase_add';
    }
}
