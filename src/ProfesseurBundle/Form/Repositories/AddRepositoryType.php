<?php
/**
 * Created by PhpStorm.
 * User: remi
 * Date: 04/04/2016
 * Time: 21:10
 */

namespace ProfesseurBundle\Form\Repositories;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class AddRepositoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('libelle', TextType::class)
            ->add('beginDate', DateType::class, array(
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'required' => true,
                'attr' => [
                    'class' => 'form-control input-inline dateForJuiAddBegin',
                    'data-provide' => 'datepicker',
                ]
            ))
            ->add('endDate', DateType::class, array(
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'required' => true,
                'attr' => [
                    'class' => 'form-control input-inline dateForJuiAddEnd',
                    'data-provide' => 'datepicker',
                ]
            ))
            ->add('language', EntityType::class, array(
                'class' => 'SharedBundle:Language',
                'choice_label' => 'libelle',
                'required' => true,
            ))
            ->add('description', CKEditorType::class, array(
                //Configuration à faire !
                //voir la doc
                'required' => 'required',
                'config' => array(
                    'toolbar' => array(
                        array(
                            'name' => 'document',
                            'items' => array('Source', '-', 'DocProps', 'Preview', '-', 'Templates'),
                        ),
                        array(
                            'name' => 'basicstyles',
                            'items' => array('Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'),
                        ),
                    ),
                    'uiColor' => '#D9EDF7',
                    //...
                ),
            ))
            ->add('limited', CheckboxType::class, array(
                'required' => false,
            ))
            ->add('maxCommitPerPerson', IntegerType::class, array(
                'required' => true,
                'attr' => array(
                    'min' => 1,
                    'step' => 1,
                    'readonly' => true
                ),
                'data' => 1
            ))
            ->add('timeout', IntegerType::class, array(
                'required' => true,
                'attr' => array(
                    'min' => 1,
                    'step' => 0.001
                )
            ))
            ->add('save', SubmitType::class, array('label' => 'Ajouter un dépôt'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SharedBundle\Entity\Repository'
        ));
    }

    public function getName()
    {
        return 'professeurBundle_repo_add';
    }
}
