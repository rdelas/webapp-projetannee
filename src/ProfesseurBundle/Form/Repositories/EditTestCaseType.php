<?php
/**
 * Created by PhpStorm.
 * User: remi
 * Date: 06/04/2016
 * Time: 22:13
 */
namespace ProfesseurBundle\Form\Repositories;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class EditTestCaseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('libelle', TextType::class)
            ->add('input', TextareaType::class)
            ->add('output', TextareaType::class)
            ->add('final', CheckboxType::class, array('required' => false))
            ->add('enabled', CheckboxType::class, array('required' => false))
            ->add('save', SubmitType::class, array('label' => 'Editer un Test'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SharedBundle\Entity\TestCase'
        ));
    }

    public function getName()
    {
        return 'teacherBundle_repositories_testCase_add';
    }
}
