<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 10/04/2016
 * Time: 16:41
 */

namespace ProfesseurBundle\Form\Submissions;


use Proxies\__CG__\SharedBundle\Entity\UserGroup;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class AddGrantedSubmissionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', EntityType::class,
                array(
                    'class' => 'UserBundle:User',
                    'choice_label' => function($user) {
                        return $user->getFirstName() . ' ' . $user->getLastName();
                    },
                    'choices' => $builder->getOption('users')
                )
            )
            ->add('repository', EntityType::class,
                array(
                    'class' => 'SharedBundle:Repository',
                    'choice_label' => 'libelle',
                    'choices' => $builder->getOption('repositories')
                )
            )
            ->add('nb', IntegerType::class, array('attr' => array('value' => 0)))
            ->add('submit', SubmitType::class, array('label' => 'Enregistrer', 'attr' => array('class' => 'btn btn-info')));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'users' => null,
            'repositories' => null,
            'data_class' => 'SharedBundle\Entity\GrantedSubmission'
        ));
    }

    public function getName()
    {
        return 'professeurBundle_add_grantedsubmission';
    }

}