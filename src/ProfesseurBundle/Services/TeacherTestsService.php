<?php
/**
 * Created by PhpStorm.
 * User: remi
 * Date: 23/05/2016
 * Time: 13:52
 */

namespace ProfesseurBundle\Services;


use AppBundle\Service\HTTPService;
use Doctrine\ORM\EntityManager;
use SharedBundle\Entity\Repository;
use SharedBundle\Entity\Submission;
use UserBundle\Entity\User;

class TeacherTestsService
{

    protected $em;
    protected $submissionRepository;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->submissionRepository = $em->getRepository('SharedBundle:Submission');
    }

    /**
     * @param $id
     * @return null|object
     */
    public function getSubmission($id) {
        return $this->submissionRepository->find($id);
    }
}
