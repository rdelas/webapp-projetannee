<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 06/04/2016
 * Time: 22:05
 */

namespace ProfesseurBundle\Services;


use Doctrine\ORM\EntityManager;
use SharedBundle\Entity\UserGroup;
use UserBundle\Controller\UserMailController;
use UserBundle\Entity\User;

class TeacherStudentService
{
    protected $em;
    protected $usergroupRepository;
    protected $userRepository;
    protected $mailController;

    public function __construct(EntityManager $em, UserMailController $mailController)
    {
        $this->em = $em;
        $this->usergroupRepository = $em->getRepository('SharedBundle:UserGroup');
        $this->userRepository = $em->getRepository('UserBundle:User');
        $this->mailController = $mailController;
    }


    public function findAllGroupesOfTeacher($user)
    {

        return $this->usergroupRepository->findAllGroupesOfTeacher($user, true);

    }

    public function findGroupeOfTeacherById($idgroupe, $user)
    {

        return $this->usergroupRepository->findGroupeOfTeacherById($idgroupe, $user, true);

    }

    public function findUserbyId($id)
    {
        return $this->userRepository->find($id);
    }

    public function saveEtudiant(User $etudiant)
    {
        $this->em->merge($etudiant);
        $this->em->flush();
    }

    public function removeEtuFromGroup(UserGroup $group, User $etudiant)
    {
        if ($this->usergroupRepository->isUserInGroup($group, $etudiant)) {
            $group->removeUser($etudiant);
            $this->em->merge($group);
            $this->em->flush();

        }
    }

    public function createGroup(User $user, UserGroup $group)
    {

        $group->setCreator($user);
        $group->setEnabled(true);
        $this->em->persist($group);
        $this->em->flush();
    }

    public function saveGroupe($groupe)
    {
        $this->em->merge($groupe);
        $this->em->flush();

    }

    public function findOneBy($array)
    {
        return $this->userRepository->findOneBy($array);
    }


    public function importStudentList($listeCSV, $groupes)
    {


        set_time_limit ( 0 );
        /////////////////CREATION DES UTILISATEURS/////////////////////
        $roles = array('ROLE_ETU');
        $date = new \DateTime();
        $studToImportArray = array();

        $Data = str_getcsv($listeCSV, PHP_EOL); //parse the rows
        $created = 0;
        $rejected = 0;
        foreach ($Data as &$Row) {
            $Row = str_getcsv($Row, ";");

            if (count($Row) === 3) {
                if (filter_var($Row[2], FILTER_VALIDATE_EMAIL)) {
                    $newStudent = $this->userRepository->findOneBy(array('email' => $Row[2]));


                    if (!$newStudent) {

                        $newStudent = new User();
                        $newStudent->setPassword(random_bytes(10));
                        $newStudent->setPlainPassword(random_bytes(10));
                        $newStudent->setEnabled(false);
                        $newStudent->setUsername($Row[0] . random_bytes(20));
                        $newStudent->setRoles($roles);
                        $newStudent->setRegisterDate($date);
                        $newStudent->setNbConnexion(0);
                        $newStudent->setLastname($Row[0]);
                        $newStudent->setFirstname($Row[1]);
                        $newStudent->setEmail($Row[2]);
                        $this->em->persist($newStudent);
                        $this->em->flush();
                        $newStudent->setUsername($newStudent->getLastname() . "-" . $newStudent->getId());
                        $created++;
                    }
                    if (!$newStudent->isEnabled() && $newStudent->hasRole("ROLE_ETU")) {
                        $this->mailController->sendEmailAcountActivation($newStudent);
                    }

                    if($newStudent->hasRole("ROLE_ETU")){
                        array_push($studToImportArray, $newStudent);
                    } else {
                        $rejected++;
                    }

                }
            }

        }

        ///////////////AJOUT DES UTILISATEURS AUX GROUPES///////////////

        $this->putUserInGroups($groupes, $studToImportArray);

        $response = array('students' => sizeof($Data),
            'added' => sizeof($studToImportArray),
            'created' => $created,
            'rejected'=> $rejected);
        
        return $response;
    }


    public function putUserInGroups($groupes, $studToImportArray)
    {
        foreach ($studToImportArray as $etudiant) {
            $groupesNames = "";
            foreach ($groupes as $group) {
                if (!$this->usergroupRepository->isUserInGroup($group, $etudiant) && $etudiant->hasRole("ROLE_ETU")) {
                    $groupesNames .= '; ' . $group->getLibelle();
                    $group->addUser($etudiant);
                }
                $this->em->merge($group);
                $this->em->flush($group);
            }
            $this->mailController->sendSimpleMail($etudiant, "Notification d'ajout aux groupes" . $groupesNames, $groupesNames . '. Cela signifie que vous devez rendre les projets qui y seront associés', null, "Vous recevez cet email car vous avez été ajouté aux groupes suivants");

        }
    }
}















