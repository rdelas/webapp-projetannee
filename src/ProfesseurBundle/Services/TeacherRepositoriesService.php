<?php
namespace ProfesseurBundle\Services;

use AppBundle\Service\HTTPService;
use Doctrine\ORM\EntityManager;
use SharedBundle\Entity\Repository;
use SharedBundle\Entity\RunResult;
use SharedBundle\Entity\Submission;
use SharedBundle\Entity\TestCase;
use SharedBundle\Entity\UserGroup;
use UserBundle\Entity\User;

class TeacherRepositoriesService
{

    protected $em;
    protected $http;
    protected $repoRepository;
    protected $testCaseRepository;
    protected $submissionRepository;

    public function __construct(EntityManager $em, HTTPService $http)
    {
        $this->em = $em;
        $this->http = $http;
        $this->repoRepository = $em->getRepository('SharedBundle:Repository');
        $this->testCaseRepository = $em->getRepository('SharedBundle:TestCase');
        $this->submissionRepository = $em->getRepository('SharedBundle:Submission');
    }

    /**
     * @param Repository $repository
     * @param User $user
     */
    public function createRepository(Repository $repository, User $user)
    {
        $repository->setCreator($user);
        $repository->setCreateDate(new \DateTime());
        $repository->setEnabled(true);
        $this->em->persist($repository);
        $this->em->flush();
    }

    public function editRepository(Repository $repository) {
        $this->em->merge($repository);
        $this->em->flush();
    }

    /**
     * @param $id
     * @return null|object
     */
    public function getRepo($id)
    {
        return $this->repoRepository->find($id);
    }

    /**
     * @param User $user
     * @param $page
     * @param $elements
     * @param $filter
     * @return array
     */
    public function findRepositoriesPagination(User $user, $page, $elements, $filter)
    {
        $results = array();
        $totalResults = $this->repoRepository->CountAllRepoOfTeacher($user);
        $totalPage = ceil($totalResults / $elements);
        $repositories = $this->repoRepository->findAllRepoOfTeacherWithLimit($user, $elements, (($page - 1) * $elements), $filter);
        $results["totalResults"] = $totalResults;
        $results["totalPage"] = $totalPage;
        $results["repositories"] = $repositories;
        $results["beginResult"] = (($page - 1) * $elements) + 1;
        $results["endResult"] = ($page == $totalPage) ? $totalResults : $page * $elements;
        return $results;
    }

    /**
     * @param Repository $repo
     * @param $page
     * @param $elements
     * @param $filter
     * @return array
     */
    public function findTestCasesPagination(Repository $repo, $page, $elements)
    {
        $results = array();
        $totalResults = $this->testCaseRepository->CountAllTestCaseOfRepo($repo);
        $totalPage = ceil($totalResults / $elements);
        $testCases = $this->testCaseRepository->findAllTestCaseOfRepoWithLimit($repo, $elements, (($page - 1) * $elements));
        $results["totalResults"] = $totalResults;
        $results["totalPage"] = $totalPage;
        $results["testCases"] = $testCases;
        $results["beginResult"] = (($page - 1) * $elements) + 1;
        $results["endResult"] = ($page == $totalPage) ? $totalResults : $page * $elements;
        return $results;
    }

    /**
     * @param TestCase $testcase
     * @param Repository $repository
     */
    public function addTestCase(TestCase $testcase, Repository $repository)
    {
        $testcase->setRepository($repository);
        $testcase->setEnabled(true);
        $testcase->setInput($testcase->getInput() . "\n");
        $testcase->setOutput($testcase->getOutput() . "\n");
        $this->em->persist($testcase);
        $this->em->flush();
    }

    /**
     * @param TestCase $testcase
     */
    public function editTestCase(TestCase $testcase)
    {
        $testcase->setInput($testcase->getInput() . "\n");
        $testcase->setOutput($testcase->getOutput() . "\n");
        $this->em->merge($testcase);
        $this->em->flush();
    }

    /**
     * @param $repository
     */
    public function editRepoAffect(Repository $repository)
    {
        $this->em->merge($repository);
        $this->em->flush();
    }

    /**
     * @param Submission $submission
     * @param Repository $repo
     * @param User $student
     * @return array
     */
    public function submitCode(Submission $submission, Repository $repo, User $teacher) {
        $submission->setDateCommit(new \DateTime());
        $submission->setUser($teacher);
        $submission->setRepository($repo);
        $this->em->persist($submission);
        $this->em->flush();
        return $this->http->runTeacherTest($submission, $repo);
    }

    public function getAllLastSubmissionOfGroup(Repository $repository, UserGroup $group) {
        return $this->submissionRepository->getLastSubmission($repository->getId(), $group->getId());
    }
}
