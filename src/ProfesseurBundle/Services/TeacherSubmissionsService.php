<?php
namespace ProfesseurBundle\Services;

use AppBundle\Service\HTTPService;
use Doctrine\ORM\EntityManager;
use SharedBundle\Entity\Repository;
use SharedBundle\Entity\RunResult;
use SharedBundle\Entity\Submission;
use SharedBundle\Entity\TestCase;
use UserBundle\Entity\User;

class TeacherSubmissionsService
{

    protected $em;
    protected $http;
    protected $repoRepository;
    protected $submissionRepository;
    protected $userGroupRepository;
    protected $userRepo;

    public function __construct(EntityManager $em, HTTPService $http)
    {
        $this->em = $em;
        $this->http = $http;
        $this->repoRepository = $em->getRepository('SharedBundle:Repository');
        $this->submissionRepository = $em->getRepository('SharedBundle:Submission');
        $this->userGroupRepository = $em->getRepository('SharedBundle:UserGroup');
        $this->userRepo =  $em->getRepository('UserBundle:User');
    }


    /**
     * @param User $teacher
     * @return mixed
     */
    public function findAllRepositoriesOfTeacher(User $teacher) {
        return $this->repoRepository->findAllRepoOfTeacher($teacher, true);
    }

    public function findAllStudentsOfTeacher(User $teacher) {
        return $this->userRepo->findAllEtu($teacher);
    }
    /**
     * @param Repository $repo
     * @param $page
     * @param $elements
     * @param $filter
     * @return array
     */
    public function findSubmissionsPagination(User $teacher, $page, $elements, $filter)
    {
        $results = array();
        $totalResults = $this->submissionRepository->CountAllSubmissionOfTeacher($teacher);
        $totalPage = ceil($totalResults / $elements);
        $submissions = $this->submissionRepository->findAllSubmissionOfTeacherWithLimit($teacher, $elements, (($page - 1) * $elements), $filter);
        $results["totalResults"] = $totalResults;
        $results["totalPage"] = $totalPage;
        $results["submissions"] = $submissions;
        $results["beginResult"] = (($page - 1) * $elements) + 1;
        $results["endResult"] = ($page == $totalPage) ? $totalResults : $page * $elements;

        $results['runResults'] = [];
        $results['submission'] = [];
        foreach($submissions as $key => $sub) {
            $rrs = $sub->getRunResults();
            $isTimeout = false;
            $isWrongAnswer = false;
            $isExecutionError = false;
            $isOtherError = false;
            foreach($rrs as $rr) {
                $res = $rr->getResult();
                if($res == 400) {
                    $isExecutionError = true;
                } else if($res == 408) {
                    $isTimeout = true;
                } else if($res == 401) {
                    $isWrongAnswer = true;
                } else if($res == 404) {
                    $isOtherError = true;
                }
            }
            $results['runResults'][$key] = $rrs;
            if($isExecutionError == 1) {
                $results['submission'][$key] = 400;
            } else if ($isTimeout == 1) {
                $results['submission'][$key] = 408;
            } else if ($isWrongAnswer ==  1) {
                $results['submission'][$key] = 401;
            } else if ($isOtherError == 1) {
                $results['submission'][$key] = 404;
            } else {
                $results['submission'][$key] = 200;
            }
        }
        return $results;
    }

    /**
     * @param Submission $submission
     * @param Repository $repo
     * @param User $student
     * @return array
     */
    public function submitCode(Submission $submission, Repository $repo, User $teacher) {
        $results = array();
        $sub = new Submission();
        $sub->setFile($submission->getFile());
        $sub->setPathToCode($submission->getPathToCode());
        $sub->setDateCommit(new \DateTime());
        $sub->setUser($teacher);
        $sub->setRepository($repo);

        $this->em->persist($sub);
        $this->em->flush();

        $this->http->runTeacherTest($sub, $repo);

        $results['test'] = 'ok';
        return $results;
    }
    /**
     * @param $id
     * @return null|object
     */
    public function getSubmission($id) {
        return $this->submissionRepository->find($id);
    }

    /**
     * @param User $teacher
     * @return mixed
     */
    public function findAllGroupOfTeacher(User $teacher) {
        return $this->userGroupRepository->getAllGroupOfTeacher($teacher);
    }
}
