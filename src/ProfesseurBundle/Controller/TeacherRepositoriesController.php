<?php
/**
 * Created by PhpStorm.
 * User: remi
 * Date: 18/02/2016
 * Time: 19:56
 */

namespace ProfesseurBundle\Controller;

use ProfesseurBundle\Form\Repositories\EditRepositoryType;
use SharedBundle\Entity\Repository;
use SharedBundle\Entity\Submission;
use SharedBundle\Entity\TestCase;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use ProfesseurBundle\Form\Repositories\AddRepositoryType;
use ProfesseurBundle\Form\Repositories\AddTestCaseType;
use ProfesseurBundle\Form\Repositories\EditTestCaseType;
use ProfesseurBundle\Form\Repositories\EditAffectRepositoryType;
use EtudiantBundle\Form\Repository\SubmitCodeType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class TeacherRepositoriesController extends Controller
{
    /**
     * @Route("/repositories", name="teacher-repositories")
     */
    public function teacherRepositoriesAction(Request $request)
    {

        $page = ($request->get('page') != null) ? $request->get('page') : 1;
        $element = ($request->get('elements') != null) ? $request->get('elements') : 10;
        $filter = ($request->get('filter') != null) ? $request->get('filter') : null;

        $teacher = $this->get('security.token_storage')->getToken()->getUser();
        $repositories = $this->get('teacher.repositories')->findRepositoriesPagination($teacher, $page, $element, $filter);

        $repository = new Repository();
        $formAddRepo = $this->createForm(AddRepositoryType::class, $repository);
        $formAddRepo->handleRequest($request);
        if ($formAddRepo->isSubmitted() && $formAddRepo->isValid()) {
            $this->get('teacher.repositories')->createRepository($repository, $teacher);
            $repository = new Repository();
            $formAddRepo = $this->createForm(AddRepositoryType::class, $repository);
            return  $this->render('ProfesseurBundle:Repositories:teacher-repositories-add.html.twig', array(
                'formAddRepo' => $formAddRepo->createView(),
                'uri' => $this->generateUrl('teacher-repositories'),
            ));
        }

        //Reception pagination
        if ($request->get('paginate') != null) {
            return $this->render('ProfesseurBundle:Repositories:teacher-repositories-table.html.twig', array(
                'repositories' => $repositories['repositories'],
                'page' => $page,
                'elements' => $element,
                'totalResults' => $repositories["totalResults"],
                'totalPage' => $repositories["totalPage"],
                'beginResult' => $repositories["beginResult"],
                'endResult' => $repositories["endResult"],
            ));
        }


        return $this->render('ProfesseurBundle:Repositories:teacher-repositories.html.twig', array(
            'formAddRepo' => $formAddRepo->createView(),
            'totalResults' => $repositories["totalResults"],
            'totalPage' => $repositories["totalPage"],
            'repositories' => $repositories["repositories"],
            'beginResult' => $repositories["beginResult"],
            'endResult' => $repositories["endResult"],
            'page' => $page,
            'elements' => $element,
        ));
    }

    /**
     * @Route("/repositories/administration", name="teacher-repositories-administration-modal")
     */
    public function teacherRepositoriesAdministrationModalAction(Request $request) {
        $page = ($request->get('page') !=  null) ? $request->get('page') : 1;
        $element =  1;

        $repository = $this->get('teacher.repositories')->getRepo($request->get('id'));
        $teacher = $this->get('security.token_storage')->getToken()->getUser();

        $testCases = $this->get('teacher.repositories')->findTestCasesPagination($repository, $page, $element);
        $formsTestCase = array();

        for ($i = 0; $i < count($testCases['testCases']); $i++) {
            $test = $testCases['testCases'][$i];
            $formTestCase = $this->get('form.factory')->createNamedBuilder("teacher_edit_testcase" . $test->getId(), EditTestCaseType::class, $test)->getForm();
            $formTestCase->handleRequest($request);
            if ($formTestCase->isSubmitted() && $formTestCase->isValid()) {
                $this->get('teacher.repositories')->editTestCase($test);
                return $this->render('ProfesseurBundle:Repositories:Modal/Administration/teacher-repositories-testcase-edit-form.html.twig', array(
                    'formTestCase' => $formTestCase->createView(),
                ));
            }
            array_push($formsTestCase, $formTestCase->createView());
        }

        $testCase = new TestCase();
        $formAddTestCase = $this->createForm(AddTestCaseType::class, $testCase);
        $formAddTestCase->handleRequest($request);
        if($formAddTestCase->isSubmitted() && $formAddTestCase->isValid()) {
            $this->get('teacher.repositories')->addTestCase($testCase, $repository);
            return new Response();
        }

        $formEditAffect = $this->createForm( EditAffectRepositoryType::class, $repository, ['id' => $teacher->getId()]);
        $formEditAffect->handleRequest($request);
        if($formEditAffect->isSubmitted() && $formEditAffect->isValid()) {
            $this->get('teacher.repositories')->editRepoAffect($repository);
            return $this->render('ProfesseurBundle:Repositories:Modal/Administration/teacher-repositories-affect.html.twig', array(
                'formEditAffect' => $formEditAffect->createView(),
                'repository' => $repository,
            ));
        }

        if($request->get('paginate') != null ) {
            return $this->render('ProfesseurBundle:Repositories:Modal/Administration/teacher-repositories-testcase-edit.html.twig', array(
                'page' => $page,
                'testCases' => $testCases["testCases"],
                'totalResults' => $testCases["totalResults"],
                'totalPage' => $testCases["totalPage"],
                'beginResult' => $testCases["beginResult"],
                'endResult' => $testCases["endResult"],
                'formsTestCase' => $formsTestCase,
                'formEditAffect' => $formEditAffect->createView(),
                'repository' => $repository,
            ));
        }

        return $this->render('ProfesseurBundle:Repositories:Modal/Administration/teacher-repositories-administration-modal-body.html.twig', array(
            'repository' => $repository,
            'formAddTestCase' => $formAddTestCase->createView(),
            'formsTestCase' => $formsTestCase,
            'totalResults' => $testCases["totalResults"],
            'totalPage' => $testCases["totalPage"],
            'beginResult' => $testCases["beginResult"],
            'endResult' => $testCases["endResult"],
            'page' => $page,
            'formEditAffect' => $formEditAffect->createView(),
        ));
    }

    /**
     * @Route("/repositories/affect", name="teacher-repositories-affect")
     */
    public function teacherRepositoriesAffectAction(Request $request)
    {

        $repository = $this->get('teacher.repositories')->getRepo($request->get('id'));
        $teacher = $this->get('security.token_storage')->getToken()->getUser();

        $formEditAffect = $this->createForm(EditAffectRepositoryType::class, $repository, ['id' => $teacher->getId()]);
        $formEditAffect->handleRequest($request);
        if ($formEditAffect->isSubmitted() && $formEditAffect->isValid()) {
            $this->get('teacher.repositories')->editRepoAffect($repository);
        }

        return $this->render('ProfesseurBundle:Repositories:teacher-repositories-affect.html.twig', array(
            'formEditAffect' => $formEditAffect->createView(),
            'repository' => $repository,
        ));
    }

    /**
     * @Route("/repositories/test", name="teacher-repositories-test-modal")
     */
    public function teacherRepositoriesTestModalAction(Request $request) {
        $repository = $this->get('teacher.repositories')->getRepo($request->get('id'));
        $teacher = $this->get('security.token_storage')->getToken()->getUser();
        $submission = new Submission();
        $formRepoSubmission = $this->get('form.factory')->createNamedBuilder("repo_submission_form".$repository->getId(), SubmitCodeType::class, $submission)->getForm();
        $formRepoSubmission->handleRequest($request);

        if($formRepoSubmission->isSubmitted()) {
            $this->get('teacher.repositories')->submitCode($submission, $repository, $teacher);
            $response = $this->forward('ProfesseurBundle:TeacherTests:index');
            return $response;
        }

        return $this->render('ProfesseurBundle:Repositories:Modal/Test/teacher-repositories-test-modal-body.html.twig', array(
            'formRepoSubmission' => $formRepoSubmission->createView(),
            'repository' => $repository,
        ));
    }

    /**
     * @Route("/repositories/edit", name="teacher-repositories-edit-modal")
     */
    public function teacherRepositoriesEditModalAction(Request $request) {
        $repository = $this->get('teacher.repositories')->getRepo($request->get('id'));
        $teacher = $this->get('security.token_storage')->getToken()->getUser();

        $formEditRepo = $this->createForm(EditRepositoryType::class, $repository);
        $formEditRepo->handleRequest($request);
        if ($formEditRepo->isSubmitted() && $formEditRepo->isValid()) {
            $this->get('teacher.repositories')->editRepository($repository);
            return $this->render('ProfesseurBundle:Repositories:Modal/Edit/teacher-repositories-edit-modal-body.html.twig', array(
                'repository' => $repository,
                'formEditRepo' => $formEditRepo->createView(),
            ));
        }

        return $this->render('ProfesseurBundle:Repositories:Modal/Edit/teacher-repositories-edit-modal-body.html.twig', array(
            'repository' => $repository,
            'formEditRepo' => $formEditRepo->createView(),
        ));
    }

    /**
     * @Route("/repositories/downloads/{id}", name="teacher-repositories-all-download")
     */
    public function teacherRepositoriesAllDownloadAction($id)
    {
        $repository = $this->get('teacher.repositories')->getRepo($id);
        $groups = $repository->getUserGroups();

        $tempName = sys_get_temp_dir() . "/" . uniqid(mt_rand());
        mkdir($tempName);
        $countGroups = 0;
        foreach($groups as $key => $group) {
            $glabel = str_replace(" ", "", $group->getLibelle());
            mkdir($tempName . "/" . $glabel);
            $lastSubmissions = $this->get('teacher.repositories')->getAllLastSubmissionOfGroup($repository, $group);
            foreach($lastSubmissions as $key => $sub)  {
                $file = fopen($tempName . "/" . $glabel . "/" . str_replace(" ", "", $sub->getUser()->getLastName() . "-" . $sub->getUser()->getFirstName() . "-"  . $sub->getRepository()->getLibelle() . ".zip"), "w");
                fclose($file);
                copy($sub->getAbsolutePath(), $tempName . "/" . $glabel . "/" . str_replace(" ", "", $sub->getUser()->getLastName() . "-" . $sub->getUser()->getFirstName() . "-"  . $sub->getRepository()->getLibelle() . ".zip"));
            }
            $countGroups++;
        }

        $filename = "allsubmissions-" . str_replace(" ", "", $repository->getLibelle());

        // Initialize archive object
        $zip = new \ZipArchive();
        $endzip = sys_get_temp_dir() . "/" . uniqid(mt_rand()) . ".zip";
        $zip->open($endzip, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

        /** @var SplFileInfo[] $files */
        $files = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator($tempName),
            \RecursiveIteratorIterator::LEAVES_ONLY
        );
        if(sizeof($files) == 1) {
            $zip->addFromString('tmp', '');
        }
        foreach ($files as $name => $file)
        {
            // Skip directories (they would be added automatically)
            if (!$file->isDir())
            {
                // Get real and relative path for current file
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($endzip) - 4);
                // Add current file to archive
                $zip->addFile($filePath, $relativePath);
            }
        }

        $zip->close();

        $response = new BinaryFileResponse($endzip);
        $response->trustXSendfileTypeHeader();
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE,
            $filename . ".zip",
            iconv('UTF-8', 'ASCII//TRANSLIT', $filename)
        );

        return $response;
    }

}