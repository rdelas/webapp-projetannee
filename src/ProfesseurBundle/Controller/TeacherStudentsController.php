<?php
/**
 * Created by PhpStorm.
 * User: remi
 * Date: 18/02/2016
 * Time: 19:58
 */

namespace ProfesseurBundle\Controller;

use ProfesseurBundle\Form\Student\AddSudentType;
use ProfesseurBundle\Form\Student\AddGroupType;
use ProfesseurBundle\Form\Student\EditGroupeType;
use ProfesseurBundle\Form\Student\ImportListStudentType;
use ProfesseurBundle\Form\Student\ListStudentsGroupPaginationType;
use ProfesseurBundle\Form\Student\ProfEditStudentType;
use SharedBundle\Entity\UserGroup;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\User;
use UserBundle\Form\Groupe\GroupeMailType;

class TeacherStudentsController extends Controller
{


    /**
     * @Route("/students", name="teacher-students")
     */

    public function etudiantProfesseurAction(Request $request)
    {

        $currUser = $this->get('security.token_storage')->getToken()->getUser();
        $listGroup = $this->getListeGroupes($currUser);
        $flashbag = null;
        $tagclass = "success";
        $group = new UserGroup();
        $formAddGroup = $this->createForm(AddGroupType::class, $group);
        $formAddGroup->handleRequest($request);
        if ($formAddGroup->isSubmitted() && $formAddGroup->isValid()) {
            $this->get('teacher.students')->createGroup($currUser, $group);
            $formAddGroup = $this->createForm(AddGroupType::class, new UserGroup());
            $listGroup = $this->getListeGroupes($currUser);
            $flashbag = "Le groupe " . $group->getLibelle() . ' a été ajouté avec succès';
        }
        //Formulaire d'ajout
        $userForm = new User();
        $formAddEtudiant = $this->createForm(AddSudentType::class, $userForm, ['listgroups' => $listGroup]);
        $formAddEtudiant->handleRequest($request);
        $etudiantFormEdit = $this->get('user.manager')->findUserByCanonicalEmail($formAddEtudiant->getData()->getEmail(), "ROLE_ETU");
        if ($etudiantFormEdit == "ALLREADYTAKENEMAIL") {

            return new Response("<div class='alert alert-danger''>Cet email est déja utilisé pour un compte administrateur ou professeur </div>");
        }
        if ($etudiantFormEdit) {
            $etudiantEdit = $this->createForm(AddSudentType::class, $etudiantFormEdit, ['listgroups' => $listGroup]);
            $etudiantEdit->handleRequest($request);
            if ($etudiantEdit->isSubmitted() && $etudiantEdit->isValid()) {
                $etudiant = $etudiantEdit->getData();


                $this->get('teacher.students')->saveEtudiant($etudiant);
                $this->get("mail.controller")->sendSimpleMail($etudiant, "Notification de mise à jour de profil", "Vous recevez cet email car un professeur (" . $currUser->getFullname() . ") a mis à jour votre profil");

                $this->get('teacher.students')->putUserInGroups($formAddEtudiant->get('userGroups')->getData(), array($etudiant));

                return new Response("<div class='alert alert-warning''>Un étudiant existait avec cet email , cependant il a été ajouté aux groupes et ses informations ont étés mises à jour</div>");
            }

        } else {
            if ($formAddEtudiant->isSubmitted() && $formAddEtudiant->isValid()) {

                $this->get('user.manager')->registerUser($userForm, array("ROLE_ETU"), true);
                $this->get('teacher.students')->putUserInGroups($formAddEtudiant->get('userGroups')->getData(), array($userForm));
                return new Response("<div class='alert alert-success'>L'étudiant " . $userForm->getFullname() . " a été ajouté</div>");
            }

        }

        $formImportListStudent = $this->createForm(ImportListStudentType::class, $currUser, ['listgroups' => $listGroup]);
        $formImportListStudent->handleRequest($request);
        if ($formImportListStudent->isSubmitted() && $formImportListStudent->isValid()) {
            $response = $this->get('teacher.students')->importStudentList($formImportListStudent->get('textlisttuser')->getData(), $formImportListStudent->get('userGroups')->getData());
            $formImportListStudent = $this->createForm(ImportListStudentType::class, $currUser, ['listgroups' => $listGroup]);


            $total = $response["students"];
            $added = $response["added"];
            $created = $response["created"];
            $rejected = $response["rejected"];

            if($rejected == 0){
                $flashbag = "La liste des étudiants a été importée avec succès";
                $tagclass = "success";
            } else if($added != 0 && $rejected != 0) {
                $flashbag =  "La liste des étudiants a été importée avec des erreurs";
                $tagclass = "warning";
            } else {
                $flashbag =  "La liste des étudiants n'a pas été importée";
                $tagclass = "danger";
            }

            $flashbag .=    "<br/>$total étudiants donnés : <br/>".
                "<ul><li>$added étudiants ajoutés aux groupes</li>".
                "<li>$created étudiants créés</li>".
                "<li>$rejected étudiants rejetés</li></ul>";


        }

        $listGroupEdit = array();
        foreach ($listGroup as $groupe) {
            $groupeEdit = $this->get('form.factory')->createNamedBuilder("groupe_edit" . $groupe->getId(), EditGroupeType::class, $groupe)->getForm();
            $groupeListStudent = $this->get('form.factory')->createNamedBuilder("groupe_list_user" . $groupe->getId(), ListStudentsGroupPaginationType::class, $groupe)->getForm();
            $groupeEditForm = $groupeEdit->createView();
            $groupeEditForm->vars['groupeListStudent'] = $groupeListStudent->createView();
            array_push($listGroupEdit, $groupeEditForm);
        }
        //var_dump(($listGroup,$request)[1]);

        $formMail = $this->createForm(GroupeMailType ::class, null, ['listgroups' => $listGroup]);
        return $this->renderPage($formAddEtudiant, $listGroupEdit, $formAddGroup, $formImportListStudent, $formMail, $flashbag, $tagclass);

    }

    private function getListeGroupes($currUser)
    {
        return $this->get('teacher.students')->findAllGroupesOfTeacher($currUser);
    }

    private function renderPage($formAddEtudiant, $listGroupEdit, $formAddGroup, $formImportListStudent, $formMail, $flashbag = null, $tagclass = null)
    {
        return $this->render('ProfesseurBundle:Students:teacher-students.html.twig', array(
            'listGroupEdit' => $listGroupEdit
        , 'formAddEtudiant' => $formAddEtudiant->createView()

        , 'formAddGroup' => $formAddGroup->createView()

        , 'formImportListStudent' => $formImportListStudent->createView(),
            'formMail' => $formMail->createView(),

            'flashbag' => $flashbag,
            'tagclass' => $tagclass


        ));
    }

    /**
     * @Route("/students/edit-student-group", name="teacher-edit-students")
     * @return Response
     */
    public function editEtudiant(Request $request)
    {

        $currUser = $this->get('security.token_storage')->getToken()->getUser();
        $formOfRequest = $this->createForm(ProfEditStudentType::class, new User());
        $formOfRequest->handleRequest($request);
        $idEdit = $formOfRequest->get('id')->getData();
        $etudiantFormEdit = $this->get('teacher.students')->findUserbyId($idEdit);
        if ($etudiantFormEdit) {
            $etudiantEdit = $this->createForm(ProfEditStudentType::class, $etudiantFormEdit);
            $etudiantEdit->handleRequest($request);
            if ($etudiantEdit->isSubmitted() && $etudiantEdit->isValid()) {
                $etudiant = $etudiantEdit->getData();
                if ($etudiantEdit->get('Retirer')->getData()) {
                    $this->get('teacher.students')->removeEtuFromGroup($etudiantEdit->get('groupe')->getData(), $etudiant);
                    $this->get("mail.controller")->sendSimpleMail($etudiant, "Notification de retrait de groupe",
                        "Vous recevez cet email car un professeur (" . $currUser->getFullname() . ") vous a retiré du groupe. Cela signifie que vous ne pouvez plus soummettre des codes pour les projets associés ", null,
                        "Groupe : " . $etudiantEdit->get('groupe')->getData()->getLibelle());

                } else {
                    $this->get('teacher.students')->saveEtudiant($etudiant);
                    $this->get("mail.controller")->sendSimpleMail($etudiant, "Notification de mise à jour de profil", "Vous recevez cet email car un professeur (" . $currUser->getFullname() . ") a mis à jour votre profil");


                    return $this->render('ProfesseurBundle:Students:row-prof-edit-student-table.html.twig', array(
                            'etu' => $etudiantEdit->createView(),
                            'trclass'=>'success'
                        )
                    );
                }
            }
        }
        return new Response();
    }

    /**
     * @Route("/students/techer-edit-student-group", name="teacher-edit-students-group")
     * @return Response
     */
    public
    function editStudentGroupe(Request $request)
    {
        $currUser = $this->get('security.token_storage')->getToken()->getUser();
        $listGroup = $this->getListeGroupes($currUser);
        foreach ($listGroup as $groupe) {
            $groupeEdit = $this->get('form.factory')->createNamedBuilder("groupe_edit" . $groupe->getId(), EditGroupeType::class, $groupe)->getForm();
            $groupeEdit->handleRequest($request);
            if ($groupeEdit->isSubmitted() && $groupeEdit->isValid()) {

                $groupeListStudent = $this->get('form.factory')->createNamedBuilder("groupe_list_user" . $groupe->getId(), ListStudentsGroupPaginationType::class, $groupe)->getForm();
                $groupeEditForm = $groupeEdit->createView();
                $groupeEditForm->vars['groupeListStudent'] = $groupeListStudent->createView();
                // var_dump($groupeEdit);
                $this->get('teacher.students')->saveGroupe($groupe);

                if ($groupeEdit->getData()->isEnabled()) {
                    return $this->render('ProfesseurBundle:Students:teacher-student-group-tbody.html.twig', array(
                        'listGroupEdit' => array($groupeEditForm)
                    ));
                } else {
                    return new Response();
                }
            }
        }

        return new Response();
    }

    /**
     * @Route("/students/list-student-group", name="teacher-students-group")
     * @return Response
     */
    public
    function listStudentGroupe(Request $request)
    {

        $currUser = $this->get('security.token_storage')->getToken()->getUser();
        $listGroup = $this->getListeGroupes($currUser);

        foreach ($listGroup as $groupe) {
            $groupeListStudent = $this->get('form.factory')->createNamedBuilder("groupe_list_user" . $groupe->getId(), ListStudentsGroupPaginationType::class, $groupe)->getForm();
            $groupeListStudent->handleRequest($request);
            if ($groupeListStudent->isSubmitted() && $groupeListStudent->isValid()) {
                $nbResultPage = $groupeListStudent->get('nbResultPage')->getData();
                $pageCourante = $groupeListStudent->get('pageCourante')->getData();


                if ($nbResultPage <= 0) {
                    return new Response();
                }

                if ($groupeListStudent->get('actifsCb')->getData() != $groupeListStudent->get('inactifsCb')->getData()) {
                    $users = $this->get('user.manager')->findActiveInactiveEtuInGroup($groupe, $groupeListStudent->get('actifsCb')->getData());
                } else {
                    $users = $groupe->getUsers()->getValues();
                }
                $nbTotalInGroup = count($users);

                $nbPages = ceil($nbTotalInGroup / $nbResultPage);
                if ($pageCourante >= $nbPages) {
                    $pageCourante = $nbPages - 1;
                }

                $etudiants = array_slice($users, $pageCourante * $nbResultPage, $nbResultPage);
                foreach ($etudiants as $key => $etudiant) {
                    $etudiantEdit = $this->createForm(ProfEditStudentType::class, $etudiant, ['groupe' => $groupe]);
                    $etudiants[$key] = $etudiantEdit->createView();
                }


                return $this->render('ProfesseurBundle:Students:teacher-students-pagination.html.twig', array(
                    'groupeListStudent' => $groupeListStudent->createView(),
                    'etudiants' => $etudiants,
                    'nbResultPage' => $nbResultPage,
                    'pageCourante' => $pageCourante,
                    'nbPages' => $nbPages
                ));
            }
        }

        return new Response();
    }

    /**
     * @Route("/students/list-students-all", name="teacher-students-all")
     * @return Response
     */
    private
    function listAllStudents()
    {
        $repository = $this->getDoctrine()->getManager()->getRepository('UserBundle:User');
        $listEtudiant = $repository->findAllEtu();
        return $this->render('ProfesseurBundle:Students:teacher-students.html.twig', array(
            'etudiants' => $listEtudiant,
        ));
    }
}