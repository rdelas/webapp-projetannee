<?php
/**
 * Created by PhpStorm.
 * User: remi
 * Date: 24/05/2016
 * Time: 23:20
 */

namespace ProfesseurBundle\Controller;

use ProfesseurBundle\Form\Submissions\AddGrantedSubmissionType;
use SharedBundle\Entity\GrantedSubmission;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class TeacherSubmissionsController extends Controller
{
    /**
    * @Route("/submissions", name="teacher-submissions")
    */
    public function teacherSubmissionsAction(Request $request)
    {
        $page = ($request->get('page') != null) ? $request->get('page') : 1;
        $element = ($request->get('elements') != null) ? $request->get('elements') : 10;
        $filter = ($request->get('filter') != null) ? $request->get('filter') : null;

        $teacher = $this->get('security.token_storage')->getToken()->getUser();
        $repositories = $this->get('teacher.submissions')->findAllRepositoriesOfTeacher($teacher);
        $groups = $this->get('teacher.submissions')->findAllGroupOfTeacher($teacher);
        $submissions = $this->get('teacher.submissions')->findSubmissionsPagination($teacher, $page, $element, $filter);

        $teacherStudents = $this->get('teacher.submissions')->findAllStudentsOfTeacher($teacher);
        $teacherRepositories = $this->get('teacher.submissions')->findAllRepositoriesOfTeacher($teacher);

        $sub = new GrantedSubmission();
        $addGrantedSubmissionForm = $this->createForm(AddGrantedSubmissionType::class, $sub, ['users' => $teacherStudents, 'repositories' => $teacherRepositories]);

        $addGrantedSubmissionForm->handleRequest($request);

        if ($addGrantedSubmissionForm->isSubmitted() && $addGrantedSubmissionForm->isValid()) {
            $stu = $sub->getUser();
            $repo = $sub->getRepository();
            $ssub = $this->getDoctrine()->getRepository(GrantedSubmission::class)->findOneBy(array('user' => $stu, 'repository' => $repo));
            if($ssub) {
                $ssub->setNb($ssub->getNb() + $sub->getNb());
            }
            else {
                $this->getDoctrine()->getManager()->persist($sub);

            }

            $this->getDoctrine()->getManager()->flush();
        }

        //Reception pagination
        if ($request->get('paginate') != null) {
            return $this->render('ProfesseurBundle:Submissions:teacher-submissions-table.html.twig', array(
                'page' => $page,
                'elements' => $element,
                'totalResults' => $submissions["totalResults"],
                'totalPage' => $submissions["totalPage"],
                'beginResult' => $submissions["beginResult"],
                'endResult' => $submissions["endResult"],
                'submissions' => $submissions["submissions"],
                'statut' => $submissions["submission"],
                'teacherStudents' => $teacherStudents,
                'addGrantedSubmissionForm' => $addGrantedSubmissionForm->createView()
            ));
        }

        return $this->render('ProfesseurBundle:Submissions:teacher-submissions.html.twig', array(
            'repositories' => $repositories,
            'totalResults' => $submissions["totalResults"],
            'totalPage' => $submissions["totalPage"],
            'submissions' => $submissions["submissions"],
            'beginResult' => $submissions["beginResult"],
            'endResult' => $submissions["endResult"],
            'page' => $page,
            'elements' => $element,
            'statut' => $submissions["submission"],
            'userGroups' => $groups,
            'teacherStudents' => $teacherStudents,
            'addGrantedSubmissionForm' => $addGrantedSubmissionForm->createView()
        ));
    }

    /**
     * @Route("/submissions/downloads/{id}", name="teacher-submissions-download")
     */
    public function teacherSubmissionsDownloadAction($id)
    {
        $submission = $this->get('teacher.submissions')->getSubmission($id);
        $pathToFile = $submission->getAbsolutePath();
        $filename = str_replace(" ", "", $submission->getUser()->getLastName() . "-" . $submission->getUser()->getFirstName() . "-"  . $submission->getRepository()->getLibelle() . ".zip");

        $response = new BinaryFileResponse($pathToFile);
        $response->trustXSendfileTypeHeader();
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE,
            $filename,
            iconv('UTF-8', 'ASCII//TRANSLIT', $filename)
        );

        return $response;
    }

    /**
     * @Route("/submissions/test/{id}", name="teacher-submissions-test")
     */
    public function teacherSubmissionsTestAction($id)
    {
        $submission = $this->get('teacher.submissions')->getSubmission($id);
        $teacher = $this->get('security.token_storage')->getToken()->getUser();
        $this->get('teacher.submissions')->submitCode($submission, $submission->getRepository(), $teacher);
        $response = $this->redirectToRoute('teacher-tests');
        return $response;
    }
}