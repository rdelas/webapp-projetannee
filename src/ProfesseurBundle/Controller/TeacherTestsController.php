<?php

namespace ProfesseurBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class TeacherTestsController extends Controller
{
    /**
     * @Route("/tests", name="teacher-tests")
     */
    public function indexAction(Request $request)
    {

        $page = ($request->get('page') !=  null) ? $request->get('page') : 1;
        $element =  ($request->get('elements') !=  null) ? $request->get('elements') : 10;
        $filter = ($request->get('filter') != null) ? $request->get('filter') : null;
        $teacher = $this->get('security.token_storage')->getToken()->getUser();

        $submissions = $this->get('student.results')->findSubmissionsPagination($teacher, $page, $element, $filter);

        //Reception pagination
        if($request->get('paginate') != null ) {
            return $this->render('ProfesseurBundle:Tests:teacher-tests-table.html.twig', array(
                'totalResults' => $submissions["totalResults"],
                'submissions' => $submissions["submissions"],
                'totalPage' => $submissions["totalPage"],
                'beginResult' => $submissions["beginResult"],
                'endResult' => $submissions["endResult"],
                'runResults' => $submissions["runResults"],
                'statut' => $submissions["submission"],
                'page' => $page,
                'elements' => $element,
            ));
        }

        return $this->render('ProfesseurBundle:Tests:teacher-tests.html.twig', array(
            'totalResults' => $submissions["totalResults"],
            'submissions' => $submissions["submissions"],
            'totalPage' => $submissions["totalPage"],
            'beginResult' => $submissions["beginResult"],
            'endResult' => $submissions["endResult"],
            'runResults' => $submissions["runResults"],
            'statut' => $submissions["submission"],
            'page' => $page,
            'elements' => $element,
        ));
    }

    /**
     * @Route("/tests/build", name="teacher-tests-build-modal")
     */
    public function teacherTestsBuildModalAction(Request $request) {
        $submission = $this->get('teacher.tests')->getSubmission($request->get('id'));
        return $this->render('ProfesseurBundle:Tests:Modal/teacher-tests-build-modal-body.html.twig', array(
            'sub' => $submission,
        ));
    }

    /**
     * @Route("/tests/run", name="teacher-tests-run-modal")
     */
    public function teacherTestsRunModalAction(Request $request) {
        $submission = $this->get('teacher.tests')->getSubmission($request->get('id'));
        return $this->render('ProfesseurBundle:Tests:Modal/teacher-tests-run-modal-body.html.twig', array(
            'sub' => $submission,
        ));
    }
}
