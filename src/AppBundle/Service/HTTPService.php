<?php
/**
 * Created by PhpStorm.
 * User: rdelas
 * Date: 08/03/16
 * Time: 14:58
 */

namespace AppBundle\Service;


use Doctrine\ORM\EntityManager;
use SharedBundle\Entity\Repository;
use SharedBundle\Entity\RunResult;
use SharedBundle\Entity\Submission;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Router;

class HTTPService
{

    protected $em;
    protected $router;
    protected $submissionRepository;
    protected $configRepository;
    protected $runResultRepository;
    protected $testCasesRepository;
    protected $repoRepository;

    /**
     * HTTPService constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em, Router $router) {
        $this->em = $em;
        $this->router = $router;
        $this->submissionRepository = $em->getRepository('SharedBundle:Submission');
        $this->configRepository = $em->getRepository('SharedBundle:Config');
        $this->runResultRepository = $em->getRepository('SharedBundle:RunResult');
        $this->testCasesRepository = $em->getRepository('SharedBundle:TestCase');
    }

    /**
     * @return array
     */
    public function getJudgeStatus() {
        $data = [
            'status' => false,
            'config' => $this->configRepository->getPresentConfig(),
            'info' => []
        ];

        if($data['config'] != null) {
            $ch = curl_init($this->constructURI() . '/CoreJudge/');
            curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, 3);
            curl_setopt($ch,CURLOPT_HEADER,true);
//            curl_setopt($ch,CURLOPT_NOBODY,true);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
            curl_exec($ch);
            $error = curl_errno($ch);
            $info = curl_getinfo($ch);
            curl_close($ch);
            $data['status'] =!$error && $info['http_code'] == 200;
            $data['info'] = $info;
        }
        return $data;
    }

    /**
     * @return mixed
     */
    public function statusJudge() {
        if(!$this->configRepository->getPresentConfig()) {
            return ['status' => 'exited'];
        }
        $ch = curl_init($this->constructURI() . '/manage/status');
        return $this->execCurl($ch);
    }

    /**
     * @return mixed
     */
    public function startJudge() {
        if(!$this->configRepository->getPresentConfig()) {
            return ['status' => 'exited'];
        }
        $ch = curl_init($this->constructURI() . '/manage/start');
        return $this->execCurl($ch);
    }

    /**
     * @return mixed
     */
    public function startBuildJudge() {
        if(!$this->configRepository->getPresentConfig()) {
            return ['status' => 'exited'];
        }
        $ch = curl_init($this->constructURI() . '/manage/buildstart');
        return $this->execCurl($ch);
    }

    /**
     * @return mixed
     */
    public function restartJudge() {
        if(!$this->configRepository->getPresentConfig()) {
            return ['status' => 'exited'];
        }
        $ch = curl_init($this->constructURI() . '/manage/restart');
        return $this->execCurl($ch);
    }

    /**
     * @return mixed
     */
    public function restartBuildJudge() {
        if(!$this->configRepository->getPresentConfig()) {
            return ['status' => 'exited'];
        }
        $ch = curl_init($this->constructURI() . '/manage/buildrestart');
        return $this->execCurl($ch);
    }

    /**
     * @return mixed
     */
    public function stopJudge() {
        if(!$this->configRepository->getPresentConfig()) {
            return ['status' => 'exited'];
        }
        $ch = curl_init($this->constructURI() . '/manage/stop');
        return $this->execCurl($ch);
    }

    /**
     * @param Submission $submission
     * @param Repository $repository
     * @return mixed
     */
    public function runStudentTest(Submission $submission, Repository $repository) {
        $status = $this->statusJudge();
        if(isset($status->{'status'}) && $status->{'status'} !== "running") {
            return false;
        }
        $url = $this->constructURI();
        $filePath = $submission->getAbsolutePath();
        $fileName = $submission->getPathToCode();
        $temp = tempnam(sys_get_temp_dir(), uniqid(mt_rand(), true));
        copy($filePath, $temp);
        $ref = substr($fileName,0,strlen($fileName) - 4);
        $testcaseIds = array();
        $a = new \PharData($temp);
        $testcases = $this->testCasesRepository->foundTestCase($repository);
        foreach($testcases as $key => $testcase) {
            $a->addFromString($testcase->getId() . "/INPUT.txt",  str_replace("\r\n","\n",$testcase->getInput()));
            $a->addFromString($testcase->getId() . "/OUTPUT.txt",  str_replace("\r\n","\n",$testcase->getOutput()));
            array_push($testcaseIds, $testcase->getId());
        }
        $response =  json_decode($this->sendCurlRequest($url, $ref, $temp, $repository, $testcaseIds, $submission->getDateCommit()));
        if(isset($response->{'message'})) {
            $submission->setLog($response->{'message'});
        }
        if(isset($response->{'code'})) {
            $submission->setResult($response->{'code'});
        }
        $this->em->merge($submission);
        $this->em->flush();
        return true;
    }

    /**
     * @param Submission $submission
     * @param Repository $repository
     * @return mixed
     */
    public function runTeacherTest(Submission $submission, Repository $repository) {
        $status = $this->statusJudge();
        if(isset($status->{'status'}) && $status->{'status'} !== "running") {
            return false;
        }
        $url = $this->constructURI();
        $filePath = $submission->getAbsolutePath();
        $fileName = $submission->getPathToCode();
        $temp = tempnam(sys_get_temp_dir(), uniqid(mt_rand(), true));
        copy($filePath, $temp);
        $ref = substr($fileName,0,strlen($fileName) - 4);
        $testcaseIds = array();
        $a = new \PharData($temp);
        $testcases = $this->testCasesRepository->foundTestCaseWithFinal($repository);
        foreach($testcases as $key => $testcase) {
            $a->addFromString($testcase->getId() . "/INPUT.txt",  str_replace("\r\n","\n",$testcase->getInput()));
            $a->addFromString($testcase->getId() . "/OUTPUT.txt",  str_replace("\r\n","\n",$testcase->getOutput()));
            array_push($testcaseIds, $testcase->getId());
        }
        $response =  json_decode($this->sendCurlRequest($url, $ref, $temp, $repository, $testcaseIds, $submission->getDateCommit()));
        if(isset($response->{'message'})) {
            $submission->setLog($response->{'message'});
        }
        if(isset($response->{'code'})) {
            $submission->setResult($response->{'code'});
        }
        $this->em->merge($submission);
        $this->em->flush();
        return true;
    }

    /**
     * @param $uri
     * @param $ref
     * @param $filePath
     * @param Repository $repository
     * @param $testCaseIds
     * @param \DateTime $dateCommit
     * @return mixed
     */
    private function sendCurlRequest($uri, $ref, $filePath, Repository $repository, $testCaseIds, \DateTime $dateCommit) {
        $file = curl_file_create($filePath, 'application/zip', $ref . "-" . $dateCommit->getTimestamp() . '.zip');
        $post = array(
            'ref' => $ref . "-" . $dateCommit->getTimestamp(),
            'language' => $repository->getLanguage()->getLibelle(),
            'timeout' => $repository->getTimeout(),
            'callback' => 'http://' . $_SERVER["HTTP_HOST"] . $this->router->generate('judge-callback'),
            'file' => $file,
            'testCaseIds' => json_encode($testCaseIds)
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $uri . '/CoreJudge/submitJob');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch,CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    /**
     * @param Request $request
     */
    public function registerResult($request) {
        $submission = $this->submissionRepository->getSubmissionByRef($request->{'ref'});
        $submission->setBuildLog($request->{'buildResult'});
        $submission->setBuildResult($request->{'buildVerdict'});
        $this->em->merge($submission);

        foreach($request->{'runResults'} as $key => $run) {
            $runResult = new RunResult();
            $runResult->setSubmission($submission);
            $runResult->setResult($request->{'results'}->{$key});
            $runResult->setLog($run);
            $runResult->setTestCase($this->testCasesRepository->find($key));
            $this->em->persist($runResult);
        }

        $this->em->flush();
    }

    /**
     * @return string
     */
    private function constructURI() {
        $uri = $this->configRepository->getPresentConfig();
        $result = $uri->getAddr() . ":" . $uri->getPort();
        if($uri->getUri()) {
            $result = $result . '/' .$uri->getUri();
        }
        return $result;
    }

    /**
     * @param $ch
     * @return mixed
     */
    private function execCurl($ch) {
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        $result = curl_exec($ch);
        $error = curl_errno($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);

        return !$error && $info['http_code'] == 200 ? json_decode($result) : ['status' => 'exited'];
    }
}
