<?php

namespace AppBundle\Controller;

use AppBundle\Service\HTTPService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="index")
     */
    public function indexAction()
    {
        $user = $this->getUser();
        if($user) {
           if($user->hasRole("ROLE_PROF")) {
            	return $this->redirectToRoute('teacher-students');
            } else if($user->hasRole("ROLE_ADMIN")) {
            	return $this->redirectToRoute('admin-teacher');
            } else if($user->hasRole("ROLE_ETU")) {
            	return $this->redirectToRoute('student-repositories');
            }
        } else {
        	return $this->render('AppBundle:Default:index.html.twig');
        }
    }

    /**
     * @Route("/judgeCallback", name="judge-callback")
     */
    public function judgeCallbackAction(Request $request) {
        $this->get('rest.manager')->registerResult(json_decode($request->getContent()));
        return new Response();
    }
}
