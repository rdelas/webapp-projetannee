# Mettre a jours les dépendences :
php composer.phar install
(Update depedencies + assetic install and dump + cache clear)

#Install existing bundle (from packagist)
php composer.phar require bundleNamespace
php compposer.phar update
php composer.phar install

# Créer la bdd :
php bin/console doctrine:database:create

# Mettre à jour les tables
php bin/console doctrine:schema:update --force

# Inserer les jeux de données :
php bin/console doctrine:fixtures:load

# Lors d'un changement de ressource (css / js / images )
php bin/console assets:install
php bin/console assetic:dump

# Clear du cache :
php bin/console cache:clear

# Configuration
Désactiver le module intl de php (Symfony a le sien)

# /!\ Chaque pull doit être précédé d'une exécution de composer (en admin de préférence) si vous voulez eviter les problèmes !
Et oui ça peut être long ...

#/!\ Avec phpstorm, il faut utiliser la console de l'IDE pour utiliser composer (PHPstorm pose des locks que lui seul peut enlever ;) ). La console est disponible dans Tools/Run Command...

# Informations sur le layout
Les blocs main_container doivent forcément commencer par <div class="page-content"></div>

php bin/console d:s:u -f

php bin/console d:f:l