/**
 * Created by remi on 21/05/2016.
 */
$(document).ready(function() {

    //Log de compilation
    $('body').on('show.bs.modal', '#build-results-student-modal', function (e) {
        $('#build-results-student-modal-body').html("");
        $('#build-results-student-modal-spinner').show();
        var url = $('#build-results-student-modal-path').val();
        var data = "&id=" + $(e.relatedTarget).attr('data-id');
        $.post(url, data, function (response) {
            $('#build-results-student-modal-spinner').hide();
            $('#build-results-student-modal-body').html(response);
            initUi();
        });
    });

    //Log des runs
    $('body').on('show.bs.modal', '#run-results-student-modal', function (e) {
        $('#run-results-student-modal-body').html("");
        $('#run-results-student-modal-spinner').show();
        var url = $('#run-results-student-modal-path').val();
        var data = "&id=" + $(e.relatedTarget).attr('data-id');
        $.post(url, data, function (response) {
            $('#run-results-student-modal-spinner').hide();
            $('#run-results-student-modal-body').html(response);
            initUi();
        });
    });
});
