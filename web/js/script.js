$(document).ready(function () {

    //////////////////////////////////////////////////////
    //Gestion des panels
    $('body').on('click', '.collapse-block', function (e) {
        if ($(this).attr("data-step") == '0') {
            $(this).attr("class", "pull-right glyphicon glyphicon-chevron-up collapse-block");
            $(this).attr("data-step", "1")
        } else {
            $(this).attr("class", "pull-right glyphicon glyphicon-chevron-down collapse-block");
            $(this).attr("data-step", "0")
        }
    });

    //Pagination

    //Changement de page
    $('body').on('click', '.pagination-page li a, .refresh-block', function (e) {
        e.preventDefault();
        var url = $(this).attr("href");
        $(".pagination-table").html("");
        $(".pagination-spinner").show();
        var data = "&elements=" + $(".pagination-elements").val() + "&page=" + $(this).text() + "&paginate=true";

        $.post(url, data, function (response) {
            $(".pagination-spinner").hide();
            $(".pagination-content").html(response);
            initUi();
        });
    });

    //Changement nb result
    $('body').on('change', '.pagination-elements', function () {
        var url = $('.pagination-page li.active a').attr("href");
        var data = "&elements=" + $(this).val() + "&page=1&paginate=true";
        $(".pagination-table").html("");
        $(".pagination-spinner").show();
        $.post(url, data, function (response) {
            $(".pagination-spinner").hide();
            $(".pagination-content").html(response);
            initUi();
        });
    });

    //Changement de page impose testcase
    $('body').on('click', '.pagination-impose li a', function (e) {
        e.preventDefault();
        var url = $(this).attr("href");
        $(".pagination-impose-table").html("");
        $(".pagination-impose-spinner").show();
        var data = "page=" + $(this).text() + "&paginate=true&id=" + $("#teacher-testcase-repositories-id").val();
        $.post(url, data, function (response) {
            $(".pagination-impose-spinner").hide();
            $(".pagination-impose-content").html(response);
            initUi();
        });
    });

    var routeChangePassword = $('#routeChangePassword').val();
    var passwordModal = $(".bs-password-modal-lg").find(".modal-body");

    $('body').on('click', '[data-target=".bs-password-modal-lg"]', function (e) {
        passwordModal.html("");
        passwordModal.prepend($("<div>", {class: 'text-center'}).prepend($(".spinnerwrap").first().clone().show()));
        $.ajax({
            type: "POST",
            url: routeChangePassword
            , success: function (response) {
                passwordModal.html($(response));

            }
        });
    });

    $("body").on('submit', '.user-change-password', function (e) {
        e.preventDefault();

        passwordModal.prepend($("<div>", {class: 'text-center'}).prepend($(".spinnerwrap").first().clone().show()));
        $.ajax({
            type: "POST",
            url: $(this).attr("action"),
            data: $(this).serialize(),

            success: function (response) {
                passwordModal.html($(response));
            }
        });
    });


    $("body").on("click", '.addStudentInMail', function () {
        tr = $(this).closest('tr');

        if ($("select.addStudentInMailSelect option[value='" + tr.find("input.email").val() + "']").size() == 0) {

            $("select.addStudentInMailSelect").prepend('<option selected value="' + tr.find("input.email").val()
                + '">' + tr.find('input.lastname').val() + '  ' + tr.find('input.firstname').val()
                + '(' + tr.find("input.email").val() + ')</option>').trigger("chosen:updated")
        }
        $(".hidden.addStudentInMailContainer").removeClass("hidden");

        if ($("#collapseMailStudents").is(":hidden")) {
            $("html, body").animate({scrollTop: 0}, "slow");
            $("#collapseMailStudents").collapse("show");
        }
    });


    /*
     $("body").on("change", '.addStudentInMailSelect', function () {
     $(this).find("option:not(:selected)").remove();
     $(this).trigger("chosen:updated");
     });*/

    $("body").on("submit", "form.mail-groupe", function (e) {
        e.preventDefault();
        var container = $(this).closest(".panel-body");
        $(this).hide();

        container.prepend($("<div>", {class: 'text-center'}).prepend($(".spinnerwrap").first().clone().show()));
        var emails = "";
        $(this).find("select.addStudentInMailSelect option").each(function () {
            emails += this.value + ';';
        });
        $(this).find("textarea.addStudentInMailTextarea").val(emails);
        $.ajax({
            type: "POST",
            url: $(this).attr("action"),
            data: $(this).serialize(),

            success: function (response) {
                container.html($(response));
                initUi();
            }
        });
    })
////////////////////////////////////////////////////////////////
    $('#waiting-loader').Loader4Js({
        state: 'hidden',
        text: 'Patientez un instant'
    });

    initUi();

});
$.datepicker.setDefaults($.datepicker.regional["fr"]);
//$.datepicker.setDefaults({dateFormat: "dd-mm-yy"});
function initUi() {
    $('select').chosen({width: "95%", disable_search_threshold: 5});
    ///
    $(".chosen-drop").mouseover(function () {
            $(this).addClass("disableHide");
        })
        .mouseout(function () {
            $(this).removeClass("disableHide");
        });

    $('[data-toggle="tooltip"]').tooltip();

    if ($(".dateForJuiAddBegin")[0]) {
        $(".dateForJuiAddBegin").datepicker({ minDate: "0"});
    }

    if ($(".dateForJuiAddEnd")[0]) {
        $(".dateForJuiAddEnd").datepicker({ minDate: "+1"});
    }

    if ($(".dateForJuiEditBegin")[0]) {
        $(".dateForJuiEditBegin").datepicker();
    }

    if ($(".dateForJuiEditEnd")[0]) {
        $(".dateForJuiEditEnd").datepicker({ minDate: "+1"});
    }

    $(".bagmodal").modal('show').removeClass("bagmodal");
}

