/**
 * Created by remi on 17/02/2016.
 */
$(document).ready(function () {
    //Ajout d'un repo
    $('body').on('submit', '.teacher-repositories-add', function (e) {
        e.preventDefault();
        var data = $(this).serialize();
        var action = $(this).attr('action');
        var method = $(this).attr('method');
        $(':input', $(this)).not(':button, :submit, :reset, :hidden').val('');
        $("#teacher-repositories-tbody").html("");
        $("#tableRepositoriesTeacherSpinner").show();
        $.ajax({
                type: method,
                url: action,
                data: data
            })
            .done(function (response) {

                $("#teacher-repositories-add-form-container").html(response);
                var data = "&elements=" + $(".pagination-elements").val() + "&page=" + $(".pagination li.active a").text() + "&paginate=true";
                $.post(action, data, function (response) {
                    $("#tableRepositoriesTeacherSpinner").hide();
                    $("#teacher-repositories-collapse-table").html(response);
                    initUi();
                });
            });
    });

    //Chargement des modals

    //Administration
    $('body').on('show.bs.modal', '#administration-repositories-teacher-modal', function (e) {
        $('#administration-repositories-teacher-modal-body').html("");
        $('#administration-repositories-teacher-modal-spinner').show();
        var url = $('#administration-repositories-teacher-modal-path').val();
        var data = "&id=" + $(e.relatedTarget).attr('data-id');
        $.post(url, data, function (response) {
            $('#administration-repositories-teacher-modal-spinner').hide();
            $('#administration-repositories-teacher-modal-body').html(response);
            initUi();
        });
    });

    //Test
    $('body').on('show.bs.modal', '#test-repositories-teacher-modal', function (e) {
        $('#test-repositories-teacher-modal-body').html("");
        $('#test-repositories-teacher-modal-spinner').show();
        var url = $('#test-repositories-teacher-modal-path').val();
        var data = "&id=" + $(e.relatedTarget).attr('data-id');
        $.post(url, data, function (response) {
            $('#test-repositories-teacher-modal-spinner').hide();
            $('#test-repositories-teacher-modal-body').html(response);
            initUi();
        });
    });
    
    //Edition
    $('body').on('show.bs.modal', '#edit-repositories-teacher-modal', function (e) {
        $('#edit-repositories-teacher-modal-body').html("");
        $('#edit-repositories-teacher-modal-spinner').show();
        var url = $('#edit-repositories-teacher-modal-path').val();
        var data = "&id=" + $(e.relatedTarget).attr('data-id');
        $.post(url, data, function (response) {
            $('#edit-repositories-teacher-modal-spinner').hide();
            $('#edit-repositories-teacher-modal-body').html(response);
            initUi();
        });
    });

    //Edition d'un repo
    $('body').on('submit', '.teacher-repositories-edit', function (e) {
        e.preventDefault();
        var data = $(this).serialize() + "&id=" + $('#teacher-repositories-edit-id').val();
        var action = $(this).attr('action');
        var method = $(this).attr('method');
        $("#teacher-repositories-tbody").html("");
        $("#tableRepositoriesTeacherSpinner").show();
        $.ajax({
                type: method,
                url: action,
                data: data
            })
            .done(function () {
                var data = "&elements=" + $(".pagination-elements").val() + "&page=" + $(".pagination li.active a").text() + "&paginate=true";
                $.post($("#teacher-repositories-edit-callback").val(), data, function (response) {
                    $("#tableRepositoriesTeacherSpinner").hide();
                    $("#teacher-repositories-collapse-table").html(response);
                    initUi();
                });
            });
    });


    //Ajout d'un testcase
    $('body').on('submit', '.teacher-repositories-testcase-add', function (e) {
        console.log('ajax');
        e.preventDefault();
        var data = $(this).serialize() + "&id=" + $('#teacher-repositories-testcase-id').val();
        var action = $(this).attr('action');
        var method = $(this).attr('method');
        $("#editTestCaseRepositoriesTeacherSpinner").show();
        $("#teacher-repositories-testcase-edit-tab-content").html("");
        $(':input',$(this)) .not(':button, :submit, :reset, :hidden').val('');
        $.ajax({
            type: method,
            url: action,
            data: data
        })
        .done(function() {
            var data = "&page=" + $(".teacher-repositories-testcase-pagination .pagination li.active a").text() + "&paginate=true&id=" + $('#teacher-repositories-testcase-id').val();
            $.post(action, data, function(response) {
                $("#editTestCaseRepositoriesTeacherSpinner").hide();
                $("#teacher-repositories-testcase-edit-tab-content").html(response);
                initUi();
            });
        });
    });

    //Edition d'un test case
    $('body').on('submit', '.teacher-repositories-testcase-edit-form', function(e) {
        e.preventDefault();
        var index = $('.teacher-repositories-testcase-edit-form').index($(this));
        var data = $(this).serialize() + "&id=" + $('#teacher-repositories-testcase-id').val() + "&page=" + $(".pagination-impose li.active a").text();
        $('.teacher-repositories-testcase-edit-form-content:eq('+index +')').html("<p>Chargement</p>");
        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            data: data
        })
        .done(function(data) {
            $('.teacher-repositories-testcase-edit-form-content:eq('+index +')').html(data);
            initUi();
        });
    });

    //Edition des affectations
    $('body').on('submit', '.teacher-repositories-affect-group', function(e) {
        e.preventDefault();
        $('#affect-container').html("");
        $("#affect-container-teacher-spinner").show();
        var data = $(this).serialize() + "&id=" + $('#teacher-repositories-testcase-id').val();
        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            data: data
        })
        .done(function(response) {
            $("#affect-container-teacher-spinner").hide();
            $('#affect-container').html(response);

            initUi();
        });
    });

    $('body').on('click', '#add_repository_limited', function(e){
        if($(this).prop('checked')){
            $("#add_repository_maxCommitPerPerson").removeAttr( "readonly");
        } else {
            $("#add_repository_maxCommitPerPerson").prop("readonly", true);
        }
    });

    $('body').on('click', '#edit_repository_limited', function(e){
        if($(this).prop('checked')){
            $("#edit_repository_maxCommitPerPerson").removeAttr( "readonly");
        } else {
            $("#edit_repository_maxCommitPerPerson").prop("readonly", true);
        }
    });

    $('body').on("change", ".dateForJuiAddBegin", function(e){
        var mindate = $(this).val();
        mindate = mindate.split("/");

        var date1 = new Date();
        var date2 = new Date(mindate[2], mindate[1]-1, mindate[0], 0, 0, 0);
        var diffDays = date2.getDate() - date1.getDate() +1;

        $(".dateForJuiAddEnd").datepicker("option", "minDate", diffDays);

    });

    $('body').on("change", ".dateForJuiEditBegin", function(e){
        var mindate = $(this).val();
        mindate = mindate.split("/");

        var date1 = new Date();
        var date2 = new Date(mindate[2], mindate[1]-1, mindate[0], 0, 0, 0);
        var diffDays = date2.getDate() - date1.getDate() +1;

        $(".dateForJuiEditEnd").datepicker("option", "minDate", diffDays);

    });

});