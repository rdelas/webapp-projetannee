$(document).ready(function () {

    //Log de compilation
    $('body').on('show.bs.modal', '#build-tests-teacher-modal', function (e) {
        $('#build-tests-teacher-modal-body').html("");
        $('#build-tests-teacher-modal-spinner').show();
        var url = $('#build-tests-teacher-modal-path').val();
        var data = "&id=" + $(e.relatedTarget).attr('data-id');
        $.post(url, data, function (response) {
            $('#build-tests-teacher-modal-spinner').hide();
            $('#build-tests-teacher-modal-body').html(response);
            initUi();
        });
    });

    //Log des runs
    $('body').on('show.bs.modal', '#run-tests-teacher-modal', function (e) {
        $('#run-tests-teacher-modal-body').html("");
        $('#run-tests-teacher-modal-spinner').show();
        var url = $('#run-tests-teacher-modal-path').val();
        var data = "&id=" + $(e.relatedTarget).attr('data-id');
        $.post(url, data, function (response) {
            $('#run-tests-teacher-modal-spinner').hide();
            $('#run-tests-teacher-modal-body').html(response);
            initUi();
        });
    });

});