//Controller des requetes ajax pour la navigation entre interface de chaque profil

$(document).ready(function() {
    $('body').on('click' , '.nav-link' , function(e) {
        e.preventDefault();
        var url = $(this).attr('href');

        $(".page-content").html('');

        $('#waiting-loader').Loader4Js().show();

        $('.nav-link').each(function() {
            $(this).parent().attr("class", "");
        });
        $(".page-title").html($(this).attr("data-page-title"));
        $(".breadcrumb-statut").html($(this).attr("data-breadcrumb-statut"));
        $(this).parent().attr("class", "active");

        $.get(url, function(data) {
            $('#waiting-loader').Loader4Js().hide();
            $(".page-content").replaceWith(data);
            initUi();
        });
    });
});
