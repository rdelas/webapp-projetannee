$(document).ready(function() {
    $('.collapse-sidebar').click(function() {
        var state = $(this).attr('data-step');
        if(state == "0") {
            $('.navbar-static-side').css("width", "50px");
            $('.collapse-sidebar').html('&#8680;');
            $('#page-wrapper').css("padding-left", "50px");
            $('.page-title-breadcrumb').css("padding-left", "60px");
            $(this).attr('data-step', '1');
            $('.menu-title').hide();
            $(this).attr('data-toggle', 'tooltip');
            $('[data-toggle="tooltip"]').tooltip('enable');
        } else {
            $('.navbar-static-side').css("width", "250px");
            $('.collapse-sidebar').html('&#8678;');
            $('#page-wrapper').css("padding-left", "250px");
            $('.page-title-breadcrumb').css("padding-left", "260px");
            $(this).attr('data-step', '0');
            $('.menu-title').delay(400).fadeIn();
            $('[data-toggle="tooltip"]').tooltip('disable');
        }
    });
});
