$('body').on('show.bs.collapse.in', '.panelGroupe', function (e) {
    $(this).find(".prof-student-list-student-group-pagination").submit();
}).on('submit', '.prof-student-list-student-group-pagination', function (e) {
    $(this).find(".refreshPaginationStudentInGroup").addClass("spin");
    e.preventDefault();
    var container = $(this).parent();
    $.ajax({
        type: "POST",
        url: $(this).attr("action"),
        data: $(this).serialize(),
        success: function (response) {
            $(container).html(response)
            $(container).find('tr').each(function () {
                var tr = $(this);
                tr.find("td.etuData span.edit.email").prepend(tr.find(".form-control.email").removeClass("hidden"));
                tr.find("td.etuData span.edit.firstname").prepend(tr.find(".form-control.firstname").removeClass("hidden"));
                tr.find("td.etuData span.edit.lastname").prepend(tr.find(".form-control.lastname").removeClass("hidden"));
                tr.find("td.etuData span.edit.enregistrer").prepend(tr.find(".btn.btn-info.enregistrer").removeClass("hidden"));

            })

            initUi();
        }
    });
}).on(
    'click', '.editEtuFromGroup,.cancelEditForStudent', function () {
        $(this).closest("tr").find("td.etuData span").toggleClass("hidden");
    }
).on('click', '.refreshPaginationStudentInGroup', function () {
    $(this).closest("form").submit();
}).on('change', '.selectNbResultPaginationStudentInGroup, .checkbox-pagination-isactif', function () {

        $(this).closest("form").submit();
    })
    .on('click', '.paginationReceiverProfStudent .pagination li', function () {
        var form = $(this).closest("form");
        form.find('.hiddenPageCourantePaginationStudentInGroup').val($(this).attr("data"));
        form.submit();
    }).on('change', ".retirerEtuFromGroup", function (e) {
        if ($(this).prop('checked')) {
            if (confirm("Retirer l'étudiant du groupe?")) {
                $(this).closest("form").submit();
            }
            else {
                $(this)[0].checked = false;
            }
        }
    })
    .on('submit', '.prof-edit-student-in-group', function (e) {
        e.preventDefault();
        var submitedForm = this;

        $.ajax({
            type: "POST",
            url: $(this).attr("action"),
            data: $(this).serialize(),

            success: function (response) {
                if ($(submitedForm).find(".retirerEtuFromGroup")[0].checked) {
                    $(submitedForm).closest("tr").replaceWith(response);
                }
                else {
                    var classe = "." + submitedForm.className.replace(/ /g, ".");
                    $(classe).closest("tr").replaceWith(response);
                    trs = $(classe).closest("tr");
                    trs.each(function () {
                        var tr = $(this);
                        tr.find("td.etuData span.edit.email").prepend(tr.find(".form-control.email").removeClass("hidden"));
                        tr.find("td.etuData span.edit.firstname").prepend(tr.find(".form-control.firstname").removeClass("hidden"));
                        tr.find("td.etuData span.edit.lastname").prepend(tr.find(".form-control.lastname").removeClass("hidden"));
                        tr.find("td.etuData span.edit.enregistrer").prepend(tr.find(".btn.btn-info.enregistrer").removeClass("hidden"));
                    })
                }
            }
        });
    })
    .on('submit', '.prof-student-add-student', function (e) {
        e.preventDefault();
        var spinner = $(this).parent().children(".spinnerwrap");
        var success = $(this).parent().children(".alert");
        spinner.css('display', 'block');
        success.css('display', 'none');
        $.ajax({
            type: "POST",
            url: $(this).attr("action"),
            data: $(this).serialize(),

            success: function (response) {
                spinner.css('display', 'none');
                success.replaceWith($(response));
                $(".prof-student-list-student-group-pagination").submit();
                $('.prof-student-add-student [type="text"],.prof-student-add-student [type="email"]').val("")
                $("#add_sudent_userGroups option:selected").prop("selected", false);
                $("#add_sudent_userGroups").trigger("chosen:updated");

                //$(this).parent().html(response);
            }
        });
    }).on('submit', '.prof-student-add-group,.prof-student-import-list-student', function (e) {
    e.preventDefault();
    var spinner = $(this).parent().children(".spinnerwrap");
    $(this).hide();
    spinner.css('display', 'block');
    $.ajax({
        type: "POST",
        url: $(this).attr("action"),
        data: $(this).serialize(),

        success: function (response) {
            spinner.css('display', 'none');
            $(".page-content").after(response).remove();
            initUi();
        }
    });
}).on('submit', '.prof-student-edit-group', function (e) {
    e.preventDefault();
    var collapse = $(this).parent().collapse("hide");
    var parent = collapse.parent().animate({
        left: '+=100vw'
    }, 500).css('position', 'relative');
    $.ajax({
        type: "POST",
        url: $(this).attr("action"),
        data: $(this).serialize(),

        success: function (response) {

            var itemResponse = $(response);
            parent.replaceWith(itemResponse);
            itemResponse.find(".collapse").collapse("show");
        }
    });
}).on('change', '.checkboxProfDeleteGroupStudents', function (e) {

    if ($(this).prop('checked')) {
        if (confirm("Voulez vous vraiment supprimer ce groupe?")) {
            $(this).closest("form").submit();
        }
        else {
            e.preventDefault();
            $(this)[0].checked = false;
        }
    }
});