$('body').on('show.bs.collapse.in', '.panelGroupe', function (e) {
    $(this).find(".admin-student-list-student-group-pagination").submit();
}).on('submit', '.admin-student-list-student-group-pagination', function (e) {

    $(this).find(".refreshPaginationStudentInGroup").addClass("spin");
    e.preventDefault();
    var container = $(this).parent();
    $.ajax({
        type: "POST",
        url: $(this).attr("action"),
        data: $(this).serialize(),
        success: function (response) {
            $(container).html(response)
            $(container).find('tr').each(function () {
                var tr = $(this);

                tr.find("td.etuData span.edit.username").prepend(tr.find(".form-control.username").removeClass("hidden"));
                tr.find("td.etuData span.edit.email").prepend(tr.find(".form-control.email").removeClass("hidden"));
                tr.find("td.etuData span.edit.firstname").prepend(tr.find(".form-control.firstname").removeClass("hidden"));
                tr.find("td.etuData span.edit.lastname").prepend(tr.find(".form-control.lastname").removeClass("hidden"));
                tr.find("td.etuData span.edit.enregistrer").prepend(tr.find(".btn.btn-info.enregistrer").removeClass("hidden"));
                tr.find("td.etuData span.edit.enabled").prepend(tr.find(".form-control.enabled").removeClass("hidden"));
            })

            initUi();
        }
    });
}).on('change', '.selectNbResultPaginationStudentInGroup,.selectProprietaireGroupe', function () {

        $(this).closest("form").submit();
    })
    .on('click', '.paginationReceiverAdminStudent .pagination li,#adminStudentcollapseListAllStudents .pagination li', function () {
        var form = $(this).closest("form");
        form.find('.hiddenPageCourantePaginationStudentInGroup').val($(this).attr("data"));
        form.submit();
    })
    .on('submit', '.admin-edit-student-in-group', function (e) {
        e.preventDefault();
        var submitedForm = this;

        $.ajax({
            type: "POST",
            url: $(this).attr("action"),
            data: $(this).serialize(),

            success: function (response) {
                if ($(submitedForm).find(".retirerEtuFromGroup")[0].checked) {
                    $(submitedForm).closest("tr").replaceWith(response);
                }
                else {
                    var classe = "." + submitedForm.className.replace(/ /g, ".");
                    $(classe).closest("tr").replaceWith(response);
                    trs = $(classe).closest("tr");
                    trs.each(function () {
                        var tr = $(this);

                        tr.find("td.etuData span.edit.username").prepend(tr.find(".form-control.username").removeClass("hidden"));
                        tr.find("td.etuData span.edit.email").prepend(tr.find(".form-control.email").removeClass("hidden"));
                        tr.find("td.etuData span.edit.firstname").prepend(tr.find(".form-control.firstname").removeClass("hidden"));
                        tr.find("td.etuData span.edit.lastname").prepend(tr.find(".form-control.lastname").removeClass("hidden"));
                        tr.find("td.etuData span.edit.enregistrer").prepend(tr.find(".btn.btn-info.enregistrer").removeClass("hidden"));
                        tr.find("td.etuData span.edit.enabled").prepend(tr.find(".form-control.enabled").removeClass("hidden"));

                    })
                }
            }
        });
    })
    .on('submit', '.admin-student-add-student', function (e) {
        e.preventDefault();
        var spinner = $(this).parent().children(".spinnerwrap");
        var success = $(this).parent().children(".alert");
        spinner.css('display', 'block');
        success.css('display', 'none');
        $.ajax({
            type: "POST",
            url: $(this).attr("action"),
            data: $(this).serialize(),

            success: function (response) {
                spinner.css('display', 'none');
                success.replaceWith($(response));
                $(".admin-student-list-student-group-pagination").submit();
                $('.admin-student-add-student [type="text"],.admin-student-add-student [type="email"]').val("")
                $("#add_sudent_userGroups option:selected").prop("selected", false);
                $("#add_sudent_userGroups").trigger("chosen:updated");

                //$(this).parent().html(response);
            }
        });
    }).on('submit', '.admin-student-add-group,.admin-student-import-list-student', function (e) {
    e.preventDefault();
    var spinner = $(this).parent().children(".spinnerwrap");
    $(this).hide();
    spinner.css('display', 'block');
    $.ajax({
        type: "POST",
        url: $(this).attr("action"),
        data: $(this).serialize(),

        success: function (response) {
            spinner.css('display', 'none');
            $(".page-content").after(response).remove();
            initUi();
        }
    });
}).on('submit', '.admin-student-edit-group', function (e) {
    e.preventDefault();
    var collapse = $(this).parent().collapse("hide");
    var parent = collapse.parent().animate({
        left: '+=100vw'
    }, 500).css('position', 'relative');
    $.ajax({
        type: "POST",
        url: $(this).attr("action"),
        data: $(this).serialize(),

        success: function (response) {
            var itemResponse = $(response);

            parent.replaceWith(itemResponse);


            itemResponse.find(".collapse").collapse("show");
        }
    });
}).on('change', '.checkboxAdminDeleteGroupStudents', function (e) {
    if ($(this).prop('checked')) {
        if (confirm("Voulez vous vraiment désactiver ce groupe?")) {
            $(this).closest("form").submit();
        }
        else {
            $(this)[0].checked = false;
        }
    }
    else {
        if (confirm("Voulez vous réactiver ce groupe?")) {
            $(this).closest("form").submit();
        }
        else {
            $(this)[0].checked = true;
        }
    }
}).on('click', '.tablistAdminListStudentsGroup li', function (e) {
    var container = $($(this).attr("control")).empty();
    $.ajax({

        type: "POST",
        url: 'students/get-student-group',
        data: {enabled: $(this).attr("data")},
        success: function (response) {
            container.html(response);
            initUi();
        }
    })
})
;