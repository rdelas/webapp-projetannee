$(document).ready(function () {

    //Ajout d'un prof
    $('body').on('submit', '.addProfAdmin', function (e) {
        e.preventDefault();
        var data = $(this).serialize() + "&elements=" + $(".pagination-elements").val() + "&page=" + $(".pagination li.active a").text();
        var action = $(this).attr('action');
        var success = $(this).parent().find(".alert");
        $(':input', $(this)).not(':button, :submit, :reset, :hidden').val('');
        $("#admin-teacher-tbody").html("");
        $("#tableProfAdminSpinner").show();
        $.ajax({
                type: $(this).attr('method'),
                url: action,
                data: data
            })
            .done(function (response) {
                var data = "&elements=" + $(".pagination-elements").val() + "&page=" + $(".pagination li.active a").text() + "&paginate=true";

                success.replaceWith($(response));
                $.post(action, data, function (response) {
                    $("#tableProfAdminSpinner").hide();
                    $("#collapse-teacher").html(response);
                    initUi();;
                });

            });
    });

    //edition d'un prof

    //activation
    $('body').on('click', '.edit-teacher', function () {
        $('.edit-teacher-form-real-input-submit').text("");
        var index = $('.edit-teacher').index($(this));
        $('.edit-teacher-form-info-username:eq(' + index + ')').hide();
        $(".edit-teacher-form-info-email:eq(" + index + ")").hide();
        $(".edit-teacher-form-info-firstname:eq(" + index + ")").hide();
        $(".edit-teacher-form-info-lastname:eq(" + index + ")").hide();
        $(".edit-teacher-form-info-enabled:eq(" + index + ")").hide();
        $(this).hide();

        $(".edit-teacher-form-input-username:eq(" + index + ")").show().append($('.edit-teacher-form-real-input-username:eq(' + index + ')'));
        $(".edit-teacher-form-input-email:eq(" + index + ")").show().append($('.edit-teacher-form-real-input-email:eq(' + index + ')'));
        $(".edit-teacher-form-input-firstname:eq(" + index + ")").show().append($('.edit-teacher-form-real-input-firstname:eq(' + index + ')'));
        $(".edit-teacher-form-input-lastname:eq(" + index + ")").show().append($('.edit-teacher-form-real-input-lastname:eq(' + index + ')'));
        $(".edit-teacher-form-input-enabled:eq(" + index + ")").show().append($('.edit-teacher-form-real-input-enabled:eq(' + index + ')'));
        $(".edit-teacher-validate:eq(" + index + ")").show().append($('.edit-teacher-form-real-input-submit:eq(' + index + ')'));


        $(".edit-teacher-cancel:eq(" + index + ")").show();
    });

    //validation
    $('body').on('submit', '.editTeacherForm', function (e) {
        e.preventDefault();
        var index = $('.editTeacherForm').index($(this));
        var data = $(this).serialize() + "&elements=" + $(".pagination-elements").val() + "&page=" + $(".pagination li.active a").text();
        $('.teacher-table-line:eq(' + index + ')').html("<td colspan='10' class='text-center'>Chargement</td>");
        $.ajax({
                type: $(this).attr('method'),
                url: $(this).attr('action'),
                data: data
            })
            .done(function (data) {
                $('.teacher-table-line:eq(' + index + ')').replaceWith(data);
                initUi();
            });
    });

    //annulation
    $('body').on('click', '.edit-teacher-cancel', function () {
        var index = $('.edit-teacher-cancel').index($(this));

        $(".edit-teacher-form-input-username:eq(" + index + ")").hide();
        $(".edit-teacher-form-input-email:eq(" + index + ")").hide();
        $(".edit-teacher-form-input-firstname:eq(" + index + ")").hide();
        $(".edit-teacher-form-input-lastname:eq(" + index + ")").hide();
        $(".edit-teacher-form-input-enabled:eq(" + index + ")").hide();
        $(".edit-teacher-validate:eq(" + index + ")").hide();
        $(this).hide();

        $('.edit-teacher-form-info-username:eq(' + index + ')').show();
        $(".edit-teacher-form-info-email:eq(" + index + ")").show();
        $(".edit-teacher-form-info-firstname:eq(" + index + ")").show();
        $(".edit-teacher-form-info-lastname:eq(" + index + ")").show();
        $(".edit-teacher-form-info-enabled:eq(" + index + ")").show();
        $(".edit-teacher:eq(" + index + ")").show();
    });


    //Request edit password modal
    $('body').on('show.bs.modal', '#edit-teacher-password-modal', function (e) {
        $('#edit-teacher-password-modal-body').html("");
        $('#edit-teacher-password-modal-spinner').show();
        var url = $('#edit-teacher-password-modal-request-body').val();
        var data = "&id=" + $(e.relatedTarget).attr('data-id');
        $.post(url, data, function (response) {
            $('#edit-teacher-password-modal-spinner').hide();
            $('#edit-teacher-password-modal-body').html(response);
            initUi();
        });
    });

    //Changez le mot de passe d'un prof
    $('body').on('submit', '.edit-teacher-password-form', function (e) {
        e.preventDefault();
        $('#edit-teacher-password-modal-body').html("");
        $('#edit-teacher-password-modal-spinner').show();
        var data = $(this).serialize() + "&id=" + $(this).attr('data-id');
        $.ajax({
                type: $(this).attr('method'),
                url: $(this).attr('action'),
                data: data
            })
            .done(function (response) {
                $('#edit-teacher-password-modal-spinner').hide();
                $('#edit-teacher-password-modal-body').html(response);
                initUi();
            });
    });
});