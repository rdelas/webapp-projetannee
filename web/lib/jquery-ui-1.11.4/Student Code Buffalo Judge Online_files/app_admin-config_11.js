$(document).ready(function () {

    //Ajout d'une config
    $('body').on('submit', '.admin-config-add', function (e) {
        e.preventDefault();
        var data = $(this).serialize();
        var action = $(this).attr('action');
        var method = $(this).attr('method');
        $(':input', $(this)).not(':button, :submit, :reset, :hidden').val('');
        $("#admin-config-tbody").html("");
        $("#tableConfigAdminSpinner").show();
        $.ajax({
                type: method,
                url: action,
                data: data
            })
            .done(function () {
                var data = "&elements=" + $(".pagination-elements").val() + "&page=" + $(".pagination li.active a").text() + "&paginate=true";
                $.post(action, data, function (response) {
                    $("#tableConfigAdminSpinner").hide();
                    $("#admin-config-collapse-table").html(response);
                    initUi();
                });
                $("#admin-config-connexion-test").html("");
                $("#admin-config-connexion-test-spinner").show();
                $.get($("#admin-config-connexion-test-path").val(), function (response) {
                    $("#admin-config-connexion-test-spinner").hide();
                    $("#admin-config-connexion-test").html(response);
                    initUi();
                });
            });
    });

    //edition d'un prof

    //activation
    $('body').on('click', '.admin-edit-config', function () {
        $('.admin-edit-config-form-real-submit').text("");
        var index = $('.admin-edit-config').index($(this));
        $('.admin-edit-config-form-label:eq(' + index + ')').hide();
        $(".admin-edit-config-form-addr:eq(" + index + ")").hide();
        $(".admin-edit-config-form-port:eq(" + index + ")").hide();
        $(".admin-edit-config-form-uri:eq(" + index + ")").hide();
        $(".admin-edit-config-form-status:eq(" + index + ")").hide();
        $(this).hide();

        $(".admin-edit-config-form-input-label:eq(" + index + ")").show().append($('.admin-edit-config-form-real-label:eq(' + index + ')'));
        $(".admin-edit-config-form-input-addr:eq(" + index + ")").show().append($('.admin-edit-config-form-real-addr:eq(' + index + ')'));
        $(".admin-edit-config-form-input-port:eq(" + index + ")").show().append($('.admin-edit-config-form-real-port:eq(' + index + ')'));
        $(".admin-edit-config-form-input-uri:eq(" + index + ")").show().append($('.admin-edit-config-form-real-uri:eq(' + index + ')'));
        $(".admin-edit-config-form-input-status:eq(" + index + ")").show().append($('.admin-edit-config-form-real-status:eq(' + index + ')'));
        $(".admin-edit-config-form-input-submit:eq(" + index + ")").show().append($('.admin-edit-config-form-real-submit:eq(' + index + ')'));

        $(".admin-edit-config-cancel:eq(" + index + ")").show();
    });

    //validation
    $('body').on('submit', '.AdminEditConfigForm', function (e) {
        e.preventDefault();
        var index = $('.AdminEditConfigForm').index($(this));
        var action = $(this).attr('action');
        var method = $(this).attr('method');
        var data = $(this).serialize() + "&elements=" + $(".pagination-elements").val() + "&page=" + $(".pagination li.active a").text();
        $('.admin-config-table-line:eq(' + index + ')').html("<td colspan='10' class='text-center'>Chargement</td>");
        $.ajax({
                type: method,
                url: action,
                data: data
            })
            .done(function () {
                var data = "&elements=" + $(".pagination-elements").val() + "&page=" + $(".pagination li.active a").text() + "&paginate=true";
                $.post(action, data, function (response) {
                    $("#admin-config-collapse-table").html(response);
                });
                $("#admin-config-connexion-test").html("");
                $("#admin-config-connexion-test-spinner").show();
                $.get($("#admin-config-connexion-test-path").val(), function (response) {
                    $("#admin-config-connexion-test-spinner").hide();
                    $("#admin-config-connexion-test").html(response);
                    initUi();
                });
            });
    });

    //annulation
    $('body').on('click', '.admin-edit-config-cancel', function () {
        var index = $('.admin-edit-config-cancel').index($(this));

        $(".admin-edit-config-form-input-label:eq(" + index + ")").hide();
        $(".admin-edit-config-form-input-addr:eq(" + index + ")").hide();
        $(".admin-edit-config-form-input-port:eq(" + index + ")").hide();
        $(".admin-edit-config-form-input-uri:eq(" + index + ")").hide();
        $(".admin-edit-config-form-input-status:eq(" + index + ")").hide();
        $(".admin-edit-config-form-input-submit:eq(" + index + ")").hide();
        $(this).hide();

        $('.admin-edit-config-form-label:eq(' + index + ')').show();
        $(".admin-edit-config-form-addr:eq(" + index + ")").show();
        $(".admin-edit-config-form-port:eq(" + index + ")").show();
        $(".admin-edit-config-form-uri:eq(" + index + ")").show();
        $(".admin-edit-config-form-status:eq(" + index + ")").show();
        $(".admin-edit-config:eq(" + index + ")").show();
    });

    //Premier test de connexion

    //Reload test de connexion
    $('body').on('click', '#admin-config-connexion-test-refresh', function (e) {
        e.preventDefault();
        loadConnection();
    });

    $(document).ajaxComplete( function (event, xhr, settings) {
        if(settings.url.endsWith("/admin/config")){
            loadConnection();
        }
    });

    function loadConnection() {
        $("#admin-config-connexion-test").html("");
        $("#admin-config-connexion-test-spinner").show();
        $.get($("#admin-config-connexion-test-path").val(), function (response) {
            $("#admin-config-connexion-test-spinner").hide();
            $("#admin-config-connexion-test").html(response);
            initUi();
        });
    }

    //action button
    $('body').on('click', '.admin-config-server-action', function (e) {
        e.preventDefault();
        $("#admin-config-connexion-test-action-content").html("");
        $("#admin-config-connexion-test-action-spinner").show();
        $.get($(this).attr('data-path'), function (response) {
            $("#admin-config-connexion-test").html("");
            $("#admin-config-connexion-test-spinner").show();
            $.get($("#admin-config-connexion-test-path").val(), function (response2) {
                $("#admin-config-connexion-test-spinner").hide();
                $("#admin-config-connexion-test").html(response2);
                $("#admin-config-connexion-test-action-spinner").hide();
                $("#admin-config-connexion-test-action-content").html($(response).delay(2000).fadeOut(1000, "linear"));

            });
        });
    });
});