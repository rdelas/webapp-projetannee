$(document).ready(function() {

    //Modal de soumission
    $('body').on('show.bs.modal', '#submissions-repositories-student-modal', function (e) {
        $('#submissions-repositories-student-modal-body').html("");
        $('#submissions-repositories-student-modal-spinner').show();
        var url = $('#submissions-repositories-student-modal-path').val();
        var data = "&id=" + $(e.relatedTarget).attr('data-id');
        $.post(url, data, function (response) {
            $('#submissions-repositories-student-modal-spinner').hide();
            $('#submissions-repositories-student-modal-body').html(response);
        });
    });
});